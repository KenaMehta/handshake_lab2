import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/applications.css";
import { getApplications } from "./../../actions/studentApplicationAction/studentApplicationAction";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class StudentApplications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pending_filter: false,
      reviewed_filter: false,
      declined_filter: false,
      page: "1",
      limit: "2"
    };
  }

  componentWillMount() {
    this.props.getApplications({
      pending_filter: this.state.pending_filter,
      reviewed_filter: this.state.reviewed_filter,
      declined_filter: this.state.declined_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  }
  callGetPage = () => {
    this.props.getApplications({
      pending_filter: this.state.pending_filter,
      reviewed_filter: this.state.reviewed_filter,
      declined_filter: this.state.declined_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  };
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getApplications({
      pending_filter: this.state.pending_filter,
      reviewed_filter: this.state.reviewed_filter,
      declined_filter: this.state.declined_filter,
      limit: this.state.limit,
      page: pageNumber
    });
  };
  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Your Applications</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="card_style d-flex mt-5"
                style={{ width: "auto", marginBottom: "20px", height: "auto" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title mb-3" align="center">
                    Filters
                  </h4>
                  <div>
                    <div className="style__divider___1j_Fp mb-2" />
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: false,
                              pending_filter: true,
                              declined_filter: false
                            },
                            () => this.callGetPage()
                          )}
                      />Pending
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: true,
                              pending_filter: false,
                              declined_filter: false
                            },
                            () => this.callGetPage()
                          )}
                      />Reviewed
                    </label>
                  </div>

                  <div>
                    <label>
                      <input
                        id="status"
                        name="status"
                        className="mr-2"
                        type="radio"
                        value="status"
                        onClick={() =>
                          this.setState(
                            {
                              reviewed_filter: false,
                              pending_filter: false,
                              declined_filter: true
                            },
                            () => this.callGetPage()
                          )}
                      />Declined
                    </label>
                  </div>
                  <div>
                    <input
                      className="mr-2"
                      type="button"
                      value="Clear"
                      onClick={() =>
                        this.setState(
                          {
                            reviewed_filter: false,
                            pending_filter: false,
                            declined_filter: false
                          },
                          () => {
                            var tick = document.getElementsByName("status");
                            for (var i = 0; i < tick.length; i++)
                              tick[i].checked = false;
                            this.callGetPage();
                          }
                        )}
                    />
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.props.jobListArr ? (
                <div>
                  <div>
                    <Card className="card_style mt-2">
                      {this.props.jobListArr.map(job => (
                        <Card
                          bg="light"
                          className="card_style m-3 p-2"
                          style={{
                            width: "auto",
                            marginBottom: "20px",
                            height: "auto"
                          }}
                        >
                          <div
                            style={{
                              fontWeight: "600",
                              fontSize: "18px"
                            }}
                          >
                            {job.JID.JOB_TITLE}
                          </div>
                          <div
                            style={{
                              fontWeight: "500",
                              fontSize: "14px"
                            }}
                          >
                            {job.JID.C_NAME}
                          </div>
                          <div
                            className="d-flex"
                            style={{
                              fontSize: "14px",
                              color: "rgba(0,0,0,.56)"
                            }}
                          >
                            <div
                              style={{
                                top: "2px",
                                position: "relative",
                                opacity: "0.7"
                              }}
                            >
                              <ion-icon name="information" />
                            </div>
                            status: {job.STATUS}
                          </div>
                          <div
                            className="d-flex"
                            style={{
                              fontSize: "14px",
                              color: "rgba(0,0,0,.56)"
                            }}
                          >
                            <div
                              style={{
                                top: "2px",
                                position: "relative",
                                opacity: "0.7"
                              }}
                            >
                              <ion-icon name="checkmark" />
                            </div>
                            Applied {job.CreatedAt.split("T")[0]} - Applications
                            closes on {job.JID.APP_DEADLINE.split("T")[0]}
                          </div>
                        </Card>
                      ))}
                      <div className="align-self-center">
                        <Pagination
                          activePage={this.state.page}
                          itemsCountPerPage={this.state.limit}
                          totalItemsCount={this.props.totalDocuments}
                          pageRangeDisplayed={5}
                          onChange={this.handlePageChange}
                        />
                      </div>
                    </Card>
                  </div>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

//get props from redux store
const mapStateToProps = state => {
  console.log(
    "inside application student update mapStateToProps " +
      JSON.stringify(state.studentApplicationReducer)
  );
  return {
    jobListArr: state.studentApplicationReducer.jobListArr || [],
    totalDocuments: state.studentApplicationReducer.total,
    totalPages: state.studentApplicationReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    //get applications
    getApplications: payload => dispatch(getApplications(payload))
  };
};

//export StudentApplications Component
export default connect(mapStateToProps, mapDispatchToProps)(
  StudentApplications
);
