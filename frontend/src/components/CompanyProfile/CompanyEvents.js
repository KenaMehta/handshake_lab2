import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import {
  getEvents,
  addEvent,
  refreshStatus
} from "./../../actions/companyEventAction/companyEventAction";
import Pagination from "react-js-pagination";

class CompanyEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event_filter: "none",
      eventArr: [],
      eventRegArr: [],
      modalBox: "HideBox",
      application_status: "",
      E_NAME: "",
      E_DESC: "",
      LOCATION: "",
      TIME: "",
      DATE: "",
      ELIGIBILITY: "",
      selectedEvent: "",
      redirect: false,
      page: "1",
      limit: "2"
    };
  }
  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    let data = {
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: pageNumber
    };
    this.props.getEvents(data);
  };
  addEvent = e => {
    e.preventDefault();
    e.target.reset();
    console.log("in addEvent");
    let data = {
      E_NAME: this.state.E_NAME,
      E_DESC: this.state.E_DESC,
      TIME: this.state.TIME,
      DATE: this.state.DATE,
      LOCATION: this.state.LOCATION,
      ELIGIBILITY: this.state.ELIGIBILITY,
      page: this.state.page,
      limit: this.state.limit
    };
    this.props.addEvent(data);
  };

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/company/event/students/" + this.state.selectedEvent;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };
  componentWillMount() {
    let data = {
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: this.state.page
    };
    this.props.getEvents(data);
  }
  callGetPage = () => {
    let data = {
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: this.state.page
    };
    this.props.getEvents(data);
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Events</h5>
                </Col>
                <div className="d-flex m-2 p-2 justify-content-between">
                  <div className="d-flex">
                    <div
                      className="m-2"
                      style={{ left: "30px", top: "7px", position: "relative" }}
                    >
                      <ion-icon name="search-outline" />
                    </div>
                    <input
                      style={{ width: "auto" }}
                      className="mt-2 pl-4"
                      onChange={e => {
                        this.setState({ event_filter: e.target.value }, () => {
                          if (this.state.event_filter) {
                            this.callGetPage();
                          } else {
                            this.setState({ event_filter: "none" }, () => {
                              this.callGetPage();
                            });
                          }
                        });
                      }}
                      id="display_name"
                      type="text"
                      placeholder="Filter on Event"
                    />
                  </div>

                  <div>
                    <button
                      onClick={this.modalUpdate}
                      type="button"
                      className="btn btn-outline-danger style__base___hEhR9 m-2"
                    >
                      <span>Add Event</span>
                    </button>
                  </div>
                </div>
                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.props.eventArr.map(events => (
                      <div
                        onClick={e => {
                          console.log(events._id + " in div onclick");
                          localStorage.setItem("Selected_Event", events._id);
                          this.setState({ selectedEvent: events._id }, () =>
                            this.setRedirect()
                          );
                        }}
                        className="e mb-3 p-2"
                      >
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "18px"
                          }}
                        >
                          {events.E_NAME}
                        </div>
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "14px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          {events.TIME} PST|{events.DATE ? (
                            events.DATE.split("T")[0]
                          ) : (
                            ""
                          )}
                        </div>

                        <div
                          style={{
                            fontSize: "14px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          {events.LOCATION}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="align-self-center">
                  <Pagination
                    activePage={this.state.page}
                    itemsCountPerPage={this.state.limit}
                    totalItemsCount={this.props.totalDocuments}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Card>
              <Card className={this.state.modalBox + " modal"}>
                <div className="modal-content col-5">
                  <Container>
                    <span
                      className="close"
                      onClick={() => {
                        this.modalUpdate();
                        this.props.refreshStatus({ application_status: "" });
                      }}
                    >
                      &times;
                    </span>
                    <form onSubmit={this.addEvent}>
                      <div className="form-group">
                        <label style={{ fontWeight: "bold" }}>Event Name</label>
                        <input
                          name="event_name"
                          onChange={e => {
                            this.setState({ E_NAME: e.target.value });
                          }}
                          type="text"
                          placeholder="Enter Event Name"
                          className="form-control"
                        />

                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>Time</label>
                            <input
                              name="time"
                              onChange={e =>
                                this.setState({ TIME: e.target.value })}
                              type="time"
                              placeholder="Enter time"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>Date</label>
                            <input
                              name="event_date"
                              onChange={e =>
                                this.setState({ DATE: e.target.value })}
                              type="date"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Location
                            </label>
                            <input
                              name="location"
                              onChange={e =>
                                this.setState({ LOCATION: e.target.value })}
                              type="text"
                              placeholder="Enter City"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Eligibility
                            </label>
                            <input
                              name="eligibility"
                              onChange={e =>
                                this.setState({ ELIGIBILITY: e.target.value })}
                              type="text"
                              placeholder="eg. All/CE/SE/CS"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label style={{ fontWeight: "bold" }}>
                            Event Description
                          </label>
                          <textarea
                            name="event_desc"
                            onChange={e =>
                              this.setState({ E_DESC: e.target.value })}
                            type="number"
                            placeholder="Enter Event Desc"
                            className="form-control"
                          />
                        </div>

                        <div />
                        <div className="m-3" align="center">
                          <input type="submit" value="Submit" />

                          <span style={{ fontWeight: "bold", color: "red" }}>
                            {this.props.application_status}
                          </span>
                        </div>
                      </div>
                    </form>
                  </Container>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside company profile mapStateToProps " +
      JSON.stringify(state.companyEventReducer)
  );
  return {
    eventArr: state.companyEventReducer.eventArr || [],
    application_status: state.companyEventReducer.application_status || "",
    totalDocuments: state.companyEventReducer.total,
    totalPages: state.companyEventReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for propic");
  return {
    //company Event
    getEvents: payload => dispatch(getEvents(payload)),
    addEvent: payload => dispatch(addEvent(payload)),
    refreshStatus: payload => dispatch(refreshStatus(payload))
  };
};

//export CompanyProfile Component
export default connect(mapStateToProps, mapDispatchToProps)(CompanyEvents);
