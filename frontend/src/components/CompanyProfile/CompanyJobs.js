import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class CompanyJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      job_filter: "none",
      location_filter: "none",
      company_filter: "none",
      jobArr: [],
      modalBox: "HideBox",
      application_status: "",
      JOB_TITLE: "",
      APP_DEADLINE: "",
      CITY: "",
      STATE: "",
      COUNTRY: "",
      SALARY: "",
      JOB_DESC: "",
      JOB_CATEGORY: "",
      POST_FLAG: "",
      redirect: false,
      page: "1",
      limit: "2"
    };
  }
  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: pageNumber
    });
  };
  addJob = e => {
    e.preventDefault();
    e.target.reset();
    console.log("in addJob");
    var data = {
      C_NAME: localStorage.getItem("company_name"),
      JOB_TITLE: this.state.JOB_TITLE,
      APP_DEADLINE: this.state.APP_DEADLINE,
      CITY: this.state.CITY,
      STATE: this.state.STATE,
      COUNTRY: this.state.COUNTRY,
      SALARY: this.state.SALARY,
      JOB_DESC: this.state.JOB_DESC,
      JOB_CATEGORY: this.state.JOB_CATEGORY,
      POST_FLAG: "Y",
      page: this.state.page,
      limit: this.state.limit
    };
    this.props.addJob(data);
  };

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  renderRedirect = () => {
    const redirectPath = "/company/job/students/" + this.state.selectedJob;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };

  callGetPage = () => {
    console.log(this.state.category_filter + "filter");
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  };
  componentWillMount() {
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  }

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}

        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Jobs</h5>
                </Col>
                <div className="d-flex">
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="briefcase-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState({ job_filter: e.target.value }, () => {
                        if (this.state.job_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ job_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on job title"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="location-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4 mr-4"
                    onChange={e => {
                      this.setState({ location_filter: e.target.value }, () => {
                        if (this.state.location_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ location_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on city"
                  />
                </div>
                <div className="d-flex m-2 p-2 justify-content-between">
                  <div>
                    <button
                      style={{ borderRadius: "20px" }}
                      type="button"
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("on-campus button");
                        this.setState({ category_filter: "on-campus" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      On-Campus
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("full-time button");
                        this.setState({ category_filter: "full-time" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      Full Time
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("parttime button");
                        this.setState({ category_filter: "part-time" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      Part Time
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("internship button");
                        this.setState({ category_filter: "internship" }, () => {
                          console.log(this.state.category_filter);
                          this.callGetPage();
                        });
                      }}
                    >
                      Internship
                    </button>
                  </div>
                  <div>
                    <button
                      onClick={this.modalUpdate}
                      type="button"
                      className="btn btn-outline-danger style__base___hEhR9 m-2"
                    >
                      <span>Add Job</span>
                    </button>
                  </div>
                </div>
                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.props.jobArr.map(job => (
                      <div>
                        <div
                          className="e mb-3"
                          onClick={e => {
                            console.log(job._id + " in div onclick");
                            localStorage.setItem("Selected_Job", job._id);
                            this.setState({ selectedJob: job._id }, () =>
                              this.setRedirect()
                            );
                          }}
                        >
                          <div className="d-flex">
                            <div className="p-2 col-2">
                              {job.CID ? job.CID.PROFILE_PIC != null ? (
                                <img
                                  style={{
                                    border: "1px solid #ddd",
                                    borderRadius: "4px",
                                    padding: "5px",
                                    width: "60px"
                                  }}
                                  src={
                                    `${configPath.base}/images/` +
                                    job.CID.PROFILE_PIC
                                  }
                                />
                              ) : (
                                <div className="mt-3 ml-3">
                                  <ion-icon
                                    size="large"
                                    name="person-outline"
                                  />
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div>
                              <div
                                style={{
                                  fontWeight: "bold",
                                  fontSize: "18px"
                                }}
                              >
                                {job.JOB_TITLE}
                              </div>
                              <div
                                style={{
                                  fontSize: "16px"
                                }}
                              >
                                {job.C_NAME} - {job.CITY}, {job.STATE}
                              </div>
                              <div
                                style={{
                                  fontSize: "14px",
                                  color: "rgba(0,0,0,.56)"
                                }}
                              >
                                {job.JOB_CATEGORY} job
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="align-self-center">
                  <Pagination
                    activePage={this.state.page}
                    itemsCountPerPage={this.state.limit}
                    totalItemsCount={this.props.totalDocuments}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Card>
              <Card className={this.state.modalBox + " modal"}>
                <div className="modal-content col-5">
                  <Container>
                    <span
                      className="close"
                      onClick={() => {
                        this.modalUpdate();
                        this.props.refreshStatus({ application_status: "" });
                      }}
                    >
                      &times;
                    </span>
                    <form onSubmit={this.addJob}>
                      <div className="form-group">
                        <label style={{ fontWeight: "bold" }}>Job Title</label>
                        <input
                          name="job_title"
                          onChange={e => {
                            this.setState({ JOB_TITLE: e.target.value });
                          }}
                          type="text"
                          placeholder="Enter Job Title"
                          className="form-control"
                        />

                        <div className="mb-3" style={{ fontWeight: "bold" }}>
                          Job Category
                        </div>
                        <select
                          className="form-control"
                          value={this.state.JOB_CATEGORY}
                          id="category"
                          onChange={e => {
                            this.setState({ JOB_CATEGORY: e.target.value });
                          }}
                          required
                        >
                          <option value="full-time">Full Time</option>
                          <option value="part-time">Part Time</option>
                          <option value="on-campus">On Campus</option>
                          <option value="internship">Internship</option>
                        </select>

                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>Salary</label>
                            <input
                              name="Salary"
                              onChange={e =>
                                this.setState({ SALARY: e.target.value })}
                              type="number"
                              placeholder="Enter Salary"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Application Deadline
                            </label>
                            <input
                              name="app_deadline"
                              onChange={e =>
                                this.setState({ APP_DEADLINE: e.target.value })}
                              type="date"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="d-flex justify-content-between">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>City</label>
                            <input
                              name="city"
                              onChange={e =>
                                this.setState({ CITY: e.target.value })}
                              type="text"
                              placeholder="Enter City"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>State</label>
                            <input
                              name="state"
                              onChange={e =>
                                this.setState({ STATE: e.target.value })}
                              type="text"
                              placeholder="Enter State"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <label style={{ fontWeight: "bold" }}>
                              Country
                            </label>
                            <input
                              name="country"
                              onChange={e =>
                                this.setState({ COUNTRY: e.target.value })}
                              type="text"
                              placeholder="Enter Country"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label style={{ fontWeight: "bold" }}>
                            Job Description
                          </label>
                          <textarea
                            name="job_desc"
                            onChange={e =>
                              this.setState({ JOB_DESC: e.target.value })}
                            type="number"
                            placeholder="Enter Job Desc"
                            className="form-control"
                          />
                        </div>

                        <div />
                        <div className="m-3" align="center">
                          <input type="submit" value="Submit" />

                          <span style={{ fontWeight: "bold", color: "red" }}>
                            {this.props.application_status}
                          </span>
                        </div>
                      </div>
                    </form>
                  </Container>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default CompanyJobs;
