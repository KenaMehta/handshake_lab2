import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import { getEventStudents } from "./../../actions/companyStudentActions/companyStudentActions";

class CompanyEventsStudents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      job_filter: "none",
      location_filter: "none",
      company_filter: "none",
      redirect: false
    };
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/student/selected_student/" + this.state.selectedSID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };

  // getPage = () => {
  //   console.log(localStorage.getItem("Selected_Event"));
  //   axios
  //     .get(
  //       configPath.api_host +
  //         `/company/event/students/${localStorage.getItem("Selected_Event")}`
  //     )
  //     .then(response => {
  //       if (response.status === 200) {
  //         console.log(response.data);
  //         this.setState({ studentArr: response.data });
  //       }
  //     })
  //     .catch(err => console.log(err + " : Error in getting Profile details"));
  // };
  // callGetPage = () => {
  //   console.log(this.state.category_filter + "filter");
  //   this.getPage();
  // };

  componentWillMount() {
    console.log("In componentWillMount");
    this.props.getEventStudents();
  }

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Event/Students</h5>
                </Col>

                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.props.studentArr ? (
                      <div>
                        <div>
                          {this.props.studentArr.map(student => (
                            <Card
                              bg="light"
                              className="e card_style m-3 p-2"
                              style={{
                                width: "auto",
                                marginBottom: "20px",
                                height: "auto"
                              }}
                            >
                              <div className="d-flex">
                                <div className="p-2 col-2">
                                  {student.SID.PHOTO != null ? (
                                    <img
                                      style={{
                                        border: "1px solid #ddd",
                                        borderRadius: "4px",
                                        padding: "5px",
                                        width: "70px"
                                      }}
                                      src={
                                        `${configPath.base}/images/` +
                                        student.SID.PHOTO
                                      }
                                    />
                                  ) : (
                                      <div className="mt-3 ml-3">
                                        <ion-icon
                                          size="large"
                                          name="person-outline"
                                        />
                                      </div>
                                    )}
                                </div>
                                <div
                                  onClick={e => {
                                    console.log(
                                      student.SID._id + " in div onclick"
                                    );
                                    localStorage.setItem(
                                      "Selected_SID",
                                      student.SID._id
                                    );
                                    this.setState(
                                      { selectedSID: student.SID._id },
                                      () => this.setRedirect()
                                    );
                                  }}
                                >
                                  <div
                                    style={{
                                      fontWeight: "600",
                                      fontSize: "18px"
                                    }}
                                  >
                                    {student.SID.NAME}
                                  </div>
                                  {student.SID.EDUCATION.length != 0 ? (
                                    student.SID.EDUCATION.map(
                                      edu =>
                                        student.SID.COLLEGE_NAME ==
                                          edu.COLLEGE_NAME ? (
                                            <div>
                                              <div
                                                style={{
                                                  fontWeight: "500",
                                                  fontSize: "14px"
                                                }}
                                              >
                                                {edu.COLLEGE_NAME}
                                              </div>
                                              <div
                                                style={{
                                                  fontWeight: "400",
                                                  fontSize: "13px"
                                                }}
                                              >
                                                {edu.DEGREE}, Year of Passing:{" "}
                                                {edu.YEAR_OF_PASSING} |{" "}
                                                {edu.MAJOR}
                                              </div>
                                            </div>
                                          ) : (
                                            ""
                                          )
                                    )
                                  ) : (
                                      <div
                                        style={{
                                          fontWeight: "500",
                                          fontSize: "14px"
                                        }}
                                      >
                                        {student.SID.COLLEGE_NAME}
                                      </div>
                                    )}
                                </div>
                              </div>
                            </Card>
                          ))}
                        </div>
                        <div />
                      </div>
                    ) : (
                        <div />
                      )}
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside companyStudentReducer mapStateToProps " +
    JSON.stringify(state.companyStudentReducer)
  );
  return {
    studentArr: state.companyStudentReducer.studentArr || []
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for companyEventStudent");
  return {
    //company event students
    getEventStudents: payload => dispatch(getEventStudents(payload))
  };
};

//export CompanyEventsStudents Component
export default connect(mapStateToProps, mapDispatchToProps)(
  CompanyEventsStudents
);
