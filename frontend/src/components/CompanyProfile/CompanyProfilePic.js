import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class CompanyProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      location: "",
      description: "",
      phone: "",
      profileUpdateForm: "HideForm",
      profileObj: {},
      picture: "",
      profilePicture: "null"
    };
  }

  updateProfilePic = () => {
    console.log(this.state);
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };

  companyDetailsUpdate = () => {
    console.log("inside details update");
    console.log(
      configPath.api_host + "/company/profile/" + localStorage.getItem("SID")
    );
    const dataAdd = {
      NAME: this.state.name,
      LOCATION: this.state.location,
      DESCRIPTION: this.state.description,
      PHONE: this.state.phone
    };
    this.props.updateProfile(dataAdd);
    this.updateProfilePic();
  };
  updatePic = e => {
    e.preventDefault();
    let picdata = new FormData();
    picdata.append("myimage", this.state.picture);
    picdata.append("name", "profile");

    console.log("mounting in picture------------");
    console.log(this.state.picture);
    this.props.uploadProfilePic(picdata);
  };
  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
          align="center"
        >
          {console.log(this.props.profilePicture)}
          {!this.props.profilePicture.includes("null") ? (
            <img
              style={{
                border: "1px solid #ddd",
                borderRadius: "4px",
                padding: "5px",
                width: "150px"
              }}
              src={this.props.profilePicture}
            />
          ) : (
            <form onSubmit={this.updatePic}>
              <button className="style__edit-photo___B-_os">
                <div>
                  <ion-icon
                    size="large"
                    name="camera"
                    style={{ color: "#1569e0" }}
                  />
                </div>

                <div>
                  {" "}
                  <input
                    style={{ color: "#1569e0", fontSize: "13px" }}
                    type="file"
                    name="file"
                    onChange={e => {
                      console.log(e.target.files[0]);
                      this.setState({ picture: e.target.files[0] });
                    }}
                  />
                </div>
              </button>
              <input
                style={{ fontSize: "10px" }}
                type="submit"
                className="btn btn-primary mt-3"
                value="Edit Pic"
              />
            </form>
          )}

          <Card.Body>
            <Card.Title>{this.props.profileObj.NAME}</Card.Title>

            <Card.Text>{this.props.profileObj.LOCATION}</Card.Text>
            <Card.Text>{this.props.profileObj.PHONE}</Card.Text>
            <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
              {this.props.profileObj.DESCRIPTION}
            </Card.Text>
            <Button
              className="btn-danger"
              onClick={this.updateProfilePic}
              variant="primary"
            >
              Update
            </Button>
          </Card.Body>
        </Card>
        <Card
          bg="light"
          style={{ width: "18rem" }}
          className={this.state.profileUpdateForm + " card_style edu-form"}
        >
          <Card.Body>
            <form>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Name: </label>
                <input
                  name="name"
                  onChange={e => {
                    this.setState({ name: e.target.value });
                  }}
                  type="text"
                  placeholder="Edit Company Name"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Location</label>
                <input
                  name="city"
                  onChange={e => this.setState({ location: e.target.value })}
                  type="text"
                  placeholder="Enter City"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Phone</label>
                <input
                  name="state"
                  onChange={e => this.setState({ phone: e.target.value })}
                  type="number"
                  placeholder="Enter 10 digit number"
                  className="form-control"
                />
              </div>

              <div className="form-group">
                <label style={{ fontWeight: "bold" }}>Description</label>
                <textarea
                  name="country"
                  onChange={e => this.setState({ description: e.target.value })}
                  type="text"
                  placeholder="Enter Description"
                  className="form-control"
                />
              </div>
            </form>
            <Button
              className="btn-danger"
              onClick={this.companyDetailsUpdate}
              variant="primary"
            >
              {" "}
              Update
            </Button>

            <Button
              className="btn-secondary ml-3"
              onClick={() => {
                this.setState({ profileUpdateForm: "HideForm" });
              }}
              variant="primary"
            >
              {" "}
              Cancel
            </Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default CompanyProfilePic;
