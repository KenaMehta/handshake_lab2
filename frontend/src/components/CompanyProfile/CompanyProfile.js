import React, { Component } from "react";
import CompanyProfilePic from "./CompanyProfilePic";
import CompanyJobs from "./CompanyJobs";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  getProfile,
  updateProfile,
  uploadProfilePic,
  getJobs,
  addJob,
  refreshStatus
} from "./../../actions/companyProfileActions/profileAction";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
class CompanyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    this.props.getProfile();
  }

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Profile</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={4}>
              <CompanyProfilePic
                profileObj={this.props.profileObj}
                profilePicture={this.props.profilePicture}
                getProfile={this.props.getProfile}
                updateProfile={this.props.updateProfile}
                uploadProfilePic={this.props.uploadProfilePic}
              />
            </Col>
            <Col md={8}>
              <CompanyJobs
                application_status={this.props.application_status}
                jobArr={this.props.jobArr}
                getJobs={this.props.getJobs}
                addJob={this.props.addJob}
                refreshStatus={this.props.refreshStatus}
                totalDocuments={this.props.totalDocuments}
                totalPages={this.props.totalPages}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside company profile mapStateToProps " +
      JSON.stringify(state.companyProfileReducer)
  );
  return {
    profileObj: state.companyProfileReducer.profileObj || {},
    profilePicture: state.companyProfileReducer.profilePicture || "null",
    jobArr: state.companyProfileReducer.jobArr || [],
    application_status: state.companyProfileReducer.application_status || "",
    totalDocuments: state.companyProfileReducer.total,
    totalPages: state.companyProfileReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for propic");
  return {
    //company Profile
    getProfile: payload => dispatch(getProfile(payload)),
    updateProfile: payload => dispatch(updateProfile(payload)),
    uploadProfilePic: payload => dispatch(uploadProfilePic(payload)),
    //company Job
    getJobs: payload => dispatch(getJobs(payload)),
    addJob: payload => dispatch(addJob(payload)),
    refreshStatus: payload => dispatch(refreshStatus(payload))
  };
};

//export CompanyProfile Component
export default connect(mapStateToProps, mapDispatchToProps)(CompanyProfile);
