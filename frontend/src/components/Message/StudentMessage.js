import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import {
  getMessages,
  getSelectedMessage,
  sendMessage
} from "./../../actions/studentMessageAction/studentMessageAction";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class StudentMessage extends Component {
  constructor(props) {
    super(props);
    this.state = { showTextBox: false };
  }

  componentWillMount() {
    this.props.getMessages({ cid: localStorage.getItem("SID") });
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Messages</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={4}>
              <Card
                bg="light"
                className="card_style d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title" align="center">
                    List
                  </h4>
                  <div className="style__divider___1j_Fp mb-3" />
                  <div>
                    {this.props.UserMessagesArr.map(message => (
                      <div
                        onClick={e => {
                          this.props.getSelectedMessage({ id: message.ID });
                          this.setState({ showTextBox: true, id: message.ID });
                        }}
                        className="e mb-3"
                      >
                        <div className="d-flex">
                          <div className="p-2 col-3">
                            {message.PHOTO != "null" ? (
                              <img
                                style={{
                                  border: "1px solid #ddd",
                                  borderRadius: "4px",
                                  padding: "5px",
                                  width: "50px"
                                }}
                                src={
                                  `${configPath.base}/images/` + message.PHOTO
                                }
                              />
                            ) : (
                              <div className="mt-3 ml-3">
                                <ion-icon size="large" name="person-outline" />
                              </div>
                            )}
                          </div>
                          <div className="p-2 col-6">
                            <div
                              style={{
                                fontWeight: "bold",
                                fontSize: "14px"
                              }}
                            >
                              {message.NAME}
                            </div>
                            <div
                              style={{
                                fontSize: "12px"
                              }}
                            >
                              {message.INFO}
                            </div>
                            <div
                              style={{
                                fontSize: "12px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {message.LAST_MESSAGE.substring(0, 15)}...
                            </div>
                          </div>
                          <div
                            style={{
                              fontSize: "12px",
                              color: "rgba(0,0,0,.56)"
                            }}
                          >
                            {message.LAST_DATE ? (
                              message.LAST_DATE.split("T")[0]
                            ) : (
                              ""
                            )}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.props.MessageThreadArr ? (
                <div>
                  <Card
                    bg="light"
                    className="card_style d-flex m-3 p-2"
                    style={{
                      width: "auto",
                      marginBottom: "20px",
                      height: "500px"
                    }}
                  >
                    <div className="d-flex">
                      <div className="p-2 col-2">
                        {Object.keys(this.props.UserData).length != 0 ? this
                          .props.UserData.PHOTO != "null" ? (
                          <img
                            style={{
                              border: "1px solid #ddd",
                              borderRadius: "4px",
                              padding: "5px",
                              width: "70px"
                            }}
                            src={
                              `${configPath.base}/images/` +
                              this.props.UserData.PHOTO
                            }
                          />
                        ) : (
                          <div className="mt-3 ml-3">
                            <ion-icon size="large" name="person-outline" />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div>
                        <div style={{ fontSize: "24px", fontWeight: "500" }}>
                          {this.props.UserData.NAME}
                        </div>
                        <div
                          className="e_c"
                          style={{ fontSize: "16px", fontWeight: "400" }}
                        >
                          {this.props.UserData.INFO}
                        </div>
                      </div>
                    </div>
                    <div className="style__divider___1j_Fp mb-3" />
                    <div id="scroll_id" className="style__jobs___3seWY p-2">
                      <div className="p-2 style__jobs___3seWY">
                        {this.props.MessageThreadArr.map(message => (
                          <div>
                            {message.From == localStorage.getItem("SID") ? (
                              <div className="right_message p-2 m-2">
                                {message.Body}
                              </div>
                            ) : (
                              <div className="left_message p-2 m-2">
                                {message.Body}
                              </div>
                            )}
                          </div>
                        ))}
                      </div>
                    </div>

                    {this.state.showTextBox ? (
                      <div>
                        <div className="style__divider___1j_Fp leave_bottom mb-3" />
                        <div className="d-flex">
                          <div className="send_message col-10 m-2">
                            <textarea
                              id="message_body"
                              name="message_body"
                              onChange={e =>
                                this.setState({ messageBody: e.target.value })}
                              type="text"
                              placeholder="Enter Text Here"
                              className="form-control"
                            />
                          </div>
                          <div className="m-2 send_button">
                            <button
                              class="button1 mt-4"
                              onClick={() => {
                                this.props.sendMessage({
                                  id: this.state.id,
                                  BODY: this.state.messageBody
                                });
                                document.getElementById("message_body").value =
                                  "";
                              }}
                            >
                              Send
                            </button>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </Card>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log(
    "inside job student update mapStateToProps " +
      JSON.stringify(state.studentMessageReducer)
  );
  return {
    UserMessagesArr: state.studentMessageReducer.UserMessagesArr || [],
    MessageThreadArr: state.studentMessageReducer.MessageThreadArr || [],
    UserData: state.studentMessageReducer.UserData || {}
  };
};

const mapDispatchToProps = dispatch => {
  return {
    //get messages
    getMessages: payload => dispatch(getMessages(payload)),
    //get selected messagee
    getSelectedMessage: payload => dispatch(getSelectedMessage(payload)),
    //send message
    sendMessage: payload => dispatch(sendMessage(payload))
  };
};

//export CompanyMessage Component
export default connect(mapStateToProps, mapDispatchToProps)(StudentMessage);
