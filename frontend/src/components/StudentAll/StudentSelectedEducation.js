import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";

class StudentSelectedEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      deleteFlag: false,
      deleteSchool: "",
      educationArr: [],
      school: "",
      degree: "",
      major: "",
      yop: "",
      gpa: ""
    };
  }

  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>Education</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            {this.props.educationArr.map(education => (
              <div style={{ marginTop: "20px" }}>
                <div style={{ fontSize: "18px" }}>{education.COLLEGE_NAME}</div>
                <div
                  style={{
                    fontSize: "16px",
                    lineHeight: "24px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {education.DEGREE}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  Year of Passing : {education.YEAR_OF_PASSING}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    Major in
                  </span>{" "}
                  {education.MAJOR}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    Cumulative GPA:
                  </span>{" "}
                  {education.CURRENT_GPA}
                </div>
              </div>
            ))}
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedEducation;
