import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import {
  getStudentProfilePic,
  updateStudentProfileName
} from "./../../actions/profileActions/profilepicAction";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentSelectedProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: localStorage.getItem("SID"),
      profileUpdateForm: "HideForm",
      updateTextValue: this.props.name,
      tempTextUpdate: "",
      profileObj: {},
      picture: "",
      profilePicture: "null"
    };
  }
  modalUpdate = () => {
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };

  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
        >
          <div
            align="right"
            className="position_message"
            onClick={this.modalUpdate}
          >
            <ion-icon size="large" name="mail" />
          </div>
          <img
            style={{
              border: "1px solid #ddd",
              borderRadius: "4px",
              padding: "5px",
              width: "150px"
            }}
            src={this.props.profilePicture}
          />

          <Card.Body>
            <Card.Title>{this.props.profileObj.name}</Card.Title>

            <Card.Text>{this.props.profileObj.school}</Card.Text>
            {this.props.profileObj.Education ? this.props.profileObj.Education
              .length > 0 ? (
              <div>
                <Card.Text>
                  {this.props.profileObj.Education[0].MAJOR}
                </Card.Text>
                <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
                  {this.props.profileObj.Education[0].DEGREE} • GPA:{" "}
                  {this.props.profileObj.Education[0].CURRENT_GPA}
                </Card.Text>
              </div>
            ) : (
              <div />
            ) : (
              ""
            )}
          </Card.Body>
        </Card>
        <Card className={this.state.modalBox + " modal"}>
          <div className="modal-content col-5">
            <Container>
              <span
                className="close"
                onClick={() => {
                  this.modalUpdate();
                  if (localStorage.getItem("category") == "company")
                    this.props.refreshStatusCompany({ message_status: "" });
                  this.props.refreshStatusStudent({ message_status: "" });
                }}
              >
                &times;
              </span>
              <div className="d-flex">
                <div className="send_message col-10 m-2">
                  <textarea
                    id="message_body"
                    name="message_body"
                    onChange={e =>
                      this.setState({ messageBody: e.target.value })}
                    type="text"
                    placeholder="Enter Text Here"
                    className="form-control"
                  />
                </div>
                <div className="m-2 send_button">
                  <button
                    class="button1 mt-4"
                    onClick={() => {
                      if (localStorage.getItem("category") == "company") {
                        this.props.sendMessageCompany({
                          sid: localStorage.getItem("Selected_SID"),
                          BODY: this.state.messageBody
                        });
                      } else {
                        this.props.sendMessageStudent({
                          id: localStorage.getItem("Selected_SID"),
                          BODY: this.state.messageBody
                        });
                      }
                      document.getElementById("message_body").value = "";
                    }}
                  >
                    Send
                  </button>
                </div>
              </div>
              <div className="m-3" align="center">
                <span style={{ fontWeight: "bold", color: "red" }}>
                  {localStorage.getItem("category") == "company" ? (
                    this.props.company_status
                  ) : (
                    this.props.student_status
                  )}
                </span>
              </div>
            </Container>
          </div>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedProfilePic;
