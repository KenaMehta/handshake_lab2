import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";
class StudentSelectedSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: localStorage.getItem("SID"),
      profileUpdateForm: "HideForm",
      deleteSkill: "",
      deleteFlag: false,
      skillArr: [],
      skill: ""
    };
  }

  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
        >
          <Card.Body>
            <Card.Title>Your Skills</Card.Title>
            <div className="style__divider___1j_Fp mb-2" />
            <div className=" d-flex">
              {this.props.skillArr.map(skill => (
                <div className="student-skills">
                  <span className="style__tag___JUqHD" title="C++" data-hook="tag">
                    <span className="style__content___2INbm">
                      <span className="style__children___1bmK9">{skill.SKILL}</span>
                    </span>
                  </span>
                </div>
              ))}
            </div>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedSkills;
