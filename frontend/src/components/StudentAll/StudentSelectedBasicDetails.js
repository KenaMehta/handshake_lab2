import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";

class StudentSelectedBasicDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      detailObj: {},
      dob: "",
      city: "",
      state: "",
      country: "",
      phone: "",
      email: ""
    };
  }

  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          align="left"
          style={{ width: "18rem" }}
        >
          <Card.Body>
            <Card.Title>Basic Details</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            <div style={{ marginTop: "20px" }}>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>DOB:</b>{" "}
                {this.props.detailObj.dob ? (
                  this.props.detailObj.dob.split("T")[0]
                ) : (
                  ""
                )}
              </div>
              <div
                style={{
                  fontSize: "16px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>CITY:</b> {this.props.detailObj.city}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>STATE:</b> {this.props.detailObj.state}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  fontWeight: "400",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>COUNTRY:</b> {this.props.detailObj.country}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>PHONE:</b> {this.props.detailObj.phone}
              </div>
              <div
                style={{
                  fontSize: "15px",
                  lineHeight: "24px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                <b>EMAIL:</b> {this.props.detailObj.email}
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedBasicDetails;
