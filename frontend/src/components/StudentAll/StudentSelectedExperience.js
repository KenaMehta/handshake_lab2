import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";

class StudentSelectedExperience extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      deleteExperience: "",
      experienceArr: [],
      deleteFlag: false,
      company_name: "",
      title: "",
      location: "",
      start_dt: "",
      end_dt: "",
      work_desc: ""
    };
  }

  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>Work and Volunteer Experience</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            {this.props.experienceArr.map(experience => (
              <div style={{ marginTop: "20px" }}>
                <div style={{ fontSize: "18px" }}>
                  {experience.COMPANY_NAME}
                </div>
                <div
                  style={{
                    fontSize: "16px",
                    lineHeight: "24px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.TITLE}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.START_DT ? (
                    experience.START_DT.split("T")[0]
                  ) : (
                    ""
                  )}{" "}
                  to {" "}
                  {experience.END_DT ? (
                    experience.END_DT.split("T")[0]
                  ) : (
                    "Present"
                  )}{" "}
                  | {experience.LOCATION}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  {experience.WORK_DESC}
                </div>
              </div>
            ))}
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedExperience;
