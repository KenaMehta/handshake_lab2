import React, { Component } from "react";
import StudentSelectedProfilePic from "./StudentSelectedProfilePic";
import StudentSelectedSkills from "./StudentSelectedSkills";
import StudentSelectedJourney from "./StudentSelectedJourney";
import StudentSelectedEducation from "./StudentSelectedEducation";
import StudentSelectedExperience from "./StudentSelectedExperience";
import StudentSelectedBasicDetails from "./StudentSelectedBasicDetails";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { getStudentProfilePic } from "./../../actions/profileActions/profilepicAction";
import { getStudentJourney } from "./../../actions/profileActions/journeyAction";
import {
  sendMessage as sendMessageCompany,
  refreshStatus as refreshStatusCompany
} from "./../../actions/companyMessageAction/companyMessageAction";
import {
  sendMessage as sendMessageStudent,
  refreshStatus as refreshStatusStudent
} from "./../../actions/studentMessageAction/studentMessageAction";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
class StudentSelectedProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    this.props.getStudentProfilePic(localStorage.getItem("Selected_SID"));
  }
  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Profile</h2>
          </Col>
        </Row>
        <Container align="center">
          <Row>
            <Col md={4}>
              <StudentSelectedProfilePic
                profileObj={this.props.profileObj}
                profilePicture={this.props.profilePicture}
                company_status={this.props.company_status}
                student_status={this.props.student_status}
                sendMessageCompany={this.props.sendMessageCompany}
                sendMessageStudent={this.props.sendMessageStudent}
                refreshStatusCompany={this.props.refreshStatusCompany}
                refreshStatusStudent={this.props.refreshStatusStudent}
              />
              <StudentSelectedSkills
                skillArr={this.props.profileObj.Skill || []}
              />
              <StudentSelectedBasicDetails detailObj={this.props.profileObj} />
            </Col>
            <Col md={8}>
              <StudentSelectedJourney
                journeyText={this.props.journeyText}
                getStudentJourney={this.props.getStudentJourney}
              />
              <StudentSelectedEducation
                educationArr={this.props.profileObj.Education || []}
              />
              <StudentSelectedExperience
                experienceArr={this.props.profileObj.Experience || []}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside profile pic update mapStateToProps " +
      JSON.stringify(state.studentProfileReducer)
  );
  return {
    profileObj: state.studentProfileReducer.profileObj || "",
    profilePicture: state.studentProfileReducer.profilePicture || "",
    journeyText: state.studentProfileReducer.journeyText || "",
    company_status: state.companyMessageReducer.message_status || "",
    student_status: state.studentMessageReducer.message_status || ""
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for propic");
  return {
    //Student Profile Pic
    getStudentProfilePic: payload => dispatch(getStudentProfilePic(payload)),
    //Student Journey
    getStudentJourney: payload => dispatch(getStudentJourney(payload)),
    //refreshStatus
    refreshStatusCompany: payload => dispatch(refreshStatusCompany(payload)),
    refreshStatusStudent: payload => dispatch(refreshStatusStudent(payload)),
    //send message
    sendMessageCompany: payload => dispatch(sendMessageCompany(payload)),
    sendMessageStudent: payload => dispatch(sendMessageStudent(payload))
  };
};

//export StudentSelectedProfile Component
export default connect(mapStateToProps, mapDispatchToProps)(
  StudentSelectedProfile
);
