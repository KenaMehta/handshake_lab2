import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/all.css";
import { getSelectedStudent } from "./../../actions/studentAllActions/studentAllAction";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class StudentAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      student_filter: "none",
      major_filter: "none",
      skill_filter: "none",
      studentArr: [],
      redirect: false,
      profilePicture: "",
      page: "1",
      limit: "2"
    };
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/student/selected_student/" + this.state.selectedSID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };
  componentWillMount() {
    this.props.getSelectedStudent({
      student_filter: this.state.student_filter,
      major_filter: this.state.major_filter,
      skill_filter: this.state.skill_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  }
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getSelectedStudent({
      student_filter: this.state.student_filter,
      major_filter: this.state.major_filter,
      skill_filter: this.state.skill_filter,
      limit: this.state.limit,
      page: pageNumber
    });
  };

  callGetPage = () => {
    console.log(this.state.studentArr + "   studentArr");
    this.props.getSelectedStudent({
      student_filter: this.state.student_filter,
      major_filter: this.state.major_filter,
      skill_filter: this.state.skill_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Row className="head_line p-2" align="center">
          <Col md={12}>
            <h4 style={{ opacity: 0.7 }}>Explore Students</h4>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-5 d-flex"
                style={{ width: "auto", marginBottom: "20px" }}
              >
                <div className="p-2">
                  <h4 className="card-title p-2" align="center">
                    Filter Students
                  </h4>
                  <div className="style__divider___1j_Fp mb-3" />
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="people" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState(
                          { student_filter: e.target.value },
                          () => {
                            if (this.state.student_filter) {
                              this.callGetPage();
                            } else {
                              this.setState({ student_filter: "none" }, () => {
                                this.callGetPage();
                              });
                            }
                          }
                        );
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on student/college"
                    />
                  </div>
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="school" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState({ major_filter: e.target.value }, () => {
                          if (this.state.major_filter) {
                            this.callGetPage();
                          } else {
                            this.setState({ major_filter: "none" }, () => {
                              this.callGetPage();
                            });
                          }
                        });
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on Major"
                    />
                  </div>
                  <div
                    className="d-flex"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{
                        left: "30px",
                        position: "relative",
                        opcaity: "0.5"
                      }}
                    >
                      <ion-icon name="aperture" />
                    </div>
                    <input
                      className="mb-4 pl-4"
                      onChange={e => {
                        this.setState({ skill_filter: e.target.value }, () => {
                          if (this.state.skill_filter) {
                            this.callGetPage();
                          } else {
                            this.setState({ skill_filter: "none" }, () => {
                              this.callGetPage();
                            });
                          }
                        });
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on skills"
                    />
                  </div>
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.props.studentArr ? (
                <div>
                  <div>
                    <Card className="e card_style m-3 p-2">
                      {this.props.studentArr.map(student => (
                        <Card
                          bg="light"
                          className="e card_style m-3 p-2"
                          onClick={e => {
                            console.log(student.SID + " in div onclick");
                            localStorage.setItem("Selected_SID", student.SID);
                            this.setState({ selectedSID: student.SID }, () =>
                              this.setRedirect()
                            );
                          }}
                          style={{
                            width: "auto",
                            marginBottom: "20px",
                            height: "auto"
                          }}
                        >
                          <div className="d-flex">
                            {console.log(student)}
                            <div className="p-2 col-2">
                              {student.profilePic != null ? (
                                <img
                                  style={{
                                    border: "1px solid #ddd",
                                    borderRadius: "4px",
                                    padding: "5px",
                                    width: "70px"
                                  }}
                                  src={
                                    `${configPath.base}/images/` +
                                    student.profilePic
                                  }
                                />
                              ) : (
                                <div className="mt-3 ml-3">
                                  <ion-icon
                                    size="large"
                                    name="person-outline"
                                  />
                                </div>
                              )}
                            </div>
                            <div className="p-2">
                              <div
                                style={{
                                  fontWeight: "600",
                                  fontSize: "18px"
                                }}
                              >
                                {student.name}
                              </div>
                              <div
                                style={{
                                  fontWeight: "500",
                                  fontSize: "14px"
                                }}
                              >
                                {student.school}
                              </div>
                              <div
                                style={{
                                  fontWeight: "400",
                                  fontSize: "13px"
                                }}
                              >
                                {student.DEGREE}, Year of Passing:{" "}
                                {student.YEAR_OF_PASSING} | {student.MAJOR}
                              </div>
                              <div
                                style={{
                                  fontWeight: "400",
                                  fontSize: "13px"
                                }}
                              >
                                {student.SkillList}
                              </div>
                            </div>
                          </div>
                        </Card>
                      ))}
                      <div className="align-self-center">
                        <Pagination
                          activePage={this.state.page}
                          itemsCountPerPage={this.state.limit}
                          totalItemsCount={this.props.totalDocuments}
                          pageRangeDisplayed={5}
                          onChange={this.handlePageChange}
                        />
                      </div>
                    </Card>
                  </div>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside all student update mapStateToProps " +
      JSON.stringify(state.studentAllReducer)
  );
  return {
    studentArr: state.studentAllReducer.studentArr || [],
    totalDocuments: state.studentAllReducer.total,
    totalPages: state.studentAllReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    //Selected Student
    getSelectedStudent: payload => dispatch(getSelectedStudent(payload))
  };
};

//export StudentAll Component
export default connect(mapStateToProps, mapDispatchToProps)(StudentAll);
