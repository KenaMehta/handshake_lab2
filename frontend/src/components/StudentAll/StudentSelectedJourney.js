import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import { getStudentJourney } from "./../../actions/profileActions/journeyAction";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentSelectedJourney extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      journeyText: "",
      tempjourney: ""
    };
  }

  componentWillMount() {
    this.props.getStudentJourney(localStorage.getItem("Selected_SID"));
  }

  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>My Journey</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            <Card.Text style={{ marginTop: "20px", marginBottom: "20px" }}>
              {this.props.journeyText}
            </Card.Text>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentSelectedJourney;
