import React, { Component } from "react";
import StudentProfilePic from "./StudentProfilePic";
import StudentSkills from "./StudentSkills";
import StudentJourney from "./StudentJourney";
import StudentEducation from "./StudentEducation";
import StudentExperience from "./StudentExperience";
import StudentBasicDetails from "./StudentBasicDetails";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import {
  getStudentProfilePic,
  updateStudentProfileName,
  updateStudentProfilePic
} from "./../../actions/profileActions/profilepicAction";
import {
  getStudentJourney,
  updateStudentJourney
} from "./../../actions/profileActions/journeyAction";
import {
  updateStudentEducation,
  deleteStudentEducation
} from "./../../actions/profileActions/educationAction";
import {
  updateStudentExperience,
  deleteStudentExperience
} from "./../../actions/profileActions/experienceAction";
import {
  updateStudentSkill,
  deleteStudentSkill
} from "./../../actions/profileActions/skillAction";
import { updateStudentBasicDetail } from "./../../actions/profileActions/basicDetailAction";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
class StudentProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    this.props.getStudentProfilePic(localStorage.getItem("SID"));
  }
  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Your Profile</h2>
          </Col>
        </Row>
        <Container align="center">
          <Row>
            <Col md={4}>
              <StudentProfilePic
                profileObj={this.props.profileObj}
                profilePicture={this.props.profilePicture}
                updateStudentProfileName={this.props.updateStudentProfileName}
                updateStudentProfilePic={this.props.updateStudentProfilePic}
              />
              <StudentSkills
                skillArr={this.props.profileObj.Skill || []}
                updateStudentSkill={this.props.updateStudentSkill}
                deleteStudentSkill={this.props.deleteStudentSkill}
              />
              <StudentBasicDetails
                detailObj={this.props.profileObj}
                updateStudentBasicDetail={this.props.updateStudentBasicDetail}
              />
            </Col>
            <Col md={8}>
              <StudentJourney
                journeyText={this.props.journeyText}
                getStudentJourney={this.props.getStudentJourney}
                updateStudentJourney={this.props.updateStudentJourney}
              />
              <StudentEducation
                educationArr={this.props.profileObj.Education || []}
                updateStudentEducation={this.props.updateStudentEducation}
                deleteStudentEducation={this.props.deleteStudentEducation}
              />
              <StudentExperience
                experienceArr={this.props.profileObj.Experience || []}
                updateStudentExperience={this.props.updateStudentExperience}
                deleteStudentExperience={this.props.deleteStudentExperience}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside profile pic update mapStateToProps " +
      JSON.stringify(state.studentProfileReducer)
  );
  return {
    profileObj: state.studentProfileReducer.profileObj || "",
    profilePicture: state.studentProfileReducer.profilePicture || "null",
    journeyText: state.studentProfileReducer.journeyText || ""
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for propic");
  return {
    //Student Profile Pic
    getStudentProfilePic: payload => dispatch(getStudentProfilePic(payload)),
    updateStudentProfileName: payload =>
      dispatch(updateStudentProfileName(payload)),
    updateStudentProfilePic: payload =>
      dispatch(updateStudentProfilePic(payload)),
    //Student Journey
    getStudentJourney: payload => dispatch(getStudentJourney(payload)),
    updateStudentJourney: payload => dispatch(updateStudentJourney(payload)),
    //Student Education
    updateStudentEducation: payload =>
      dispatch(updateStudentEducation(payload)),
    deleteStudentEducation: payload =>
      dispatch(deleteStudentEducation(payload)),
    //Student Experience
    updateStudentExperience: payload =>
      dispatch(updateStudentExperience(payload)),
    deleteStudentExperience: payload =>
      dispatch(deleteStudentExperience(payload)),
    //Student Skills
    updateStudentSkill: payload => dispatch(updateStudentSkill(payload)),
    deleteStudentSkill: payload => dispatch(deleteStudentSkill(payload)),
    //Student Basic Detail
    updateStudentBasicDetail: payload =>
      dispatch(updateStudentBasicDetail(payload))
  };
};

//export StudentProfilePic Component
export default connect(mapStateToProps, mapDispatchToProps)(StudentProfile);
