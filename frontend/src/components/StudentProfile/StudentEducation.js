import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";

class StudentEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      deleteFlag: false,
      deleteSchool: "",
      educationArr: [],
      school: "",
      degree: "",
      major: "",
      yop: "",
      gpa: ""
    };
  }
  educationUpdate = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };
  educationDelete = () => {
    console.log("code for delete");
    console.log(this.state.deleteSchool);
    let data = { deleteSchool: this.state.deleteSchool };
    this.props.deleteStudentEducation(data);
    this.setState({ deleteFlag: false });
    this.educationUpdate();
  };
  educationAdd = () => {
    console.log("inside education add");
    const dataAdd = {
      COLLEGE_NAME: this.state.school,
      DEGREE: this.state.degree,
      MAJOR: this.state.major,
      YEAR_OF_PASSING: this.state.yop,
      CURRENT_GPA: this.state.gpa
    };
    this.props.updateStudentEducation(dataAdd);
    this.educationUpdate();
  };

  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>Education</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            {this.props.educationArr.map(education => (
              <div style={{ marginTop: "20px" }}>
                <div style={{ fontSize: "18px" }}>{education.COLLEGE_NAME}</div>
                <div
                  style={{
                    fontSize: "16px",
                    lineHeight: "24px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {education.DEGREE}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  Year of Passing : {education.YEAR_OF_PASSING}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    Major in
                  </span>{" "}
                  {education.MAJOR}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  <span
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    Cumulative GPA:
                  </span>{" "}
                  {education.CURRENT_GPA}
                </div>
              </div>
            ))}
            <Button
              className="btn-danger"
              style={{ marginTop: "20px", marginBottom: "10px" }}
              onClick={e => {
                this.educationUpdate();
                this.setState({ deleteFlag: false });
              }}
              variant="primary"
            >
              Add/Update
            </Button>
            <Button
              className="btn-danger"
              style={{
                marginLeft: "20px",
                marginTop: "20px",
                marginBottom: "10px"
              }}
              onClick={e => {
                this.educationUpdate();
                this.setState({ deleteFlag: true });
              }}
              variant="primary"
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
        {this.state.deleteFlag ? (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Delete Education</Card.Title>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>School Name</label>
                    <input
                      name="school"
                      onChange={e => {
                        this.setState({ deleteSchool: e.target.value });
                      }}
                      type="text"
                      placeholder="Enter school you want to delete"
                      className="form-control"
                    />
                  </div>
                </form>
              </div>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={
                  this.state.deleteSchool !== "" ? (
                    this.educationDelete
                  ) : (
                    this.educationUpdate
                  )
                }
                variant="primary"
              >
                Delete
              </Button>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={this.educationUpdate}
                variant="primary"
              >
                Cancel
              </Button>
            </Card.Body>
          </Card>
        ) : (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Add/Update Education</Card.Title>
              <div
                style={{
                  fontSize: "13px",
                  lineHeight: "20px",
                  color: "rgba(0,0,0,.8)"
                }}
              >
                Note: To update, mention the school name you want to update.
              </div>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>School Name</label>
                    <input
                      name="school"
                      onChange={e => {
                        this.setState({ school: e.target.value });
                      }}
                      type="text"
                      placeholder="Enter new school or name you want to update"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>
                      Education Level
                    </label>
                    <input
                      name="degree"
                      onChange={e => this.setState({ degree: e.target.value })}
                      type="text"
                      placeholder="eg. Masters/Bachelors/etc."
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Major</label>
                    <input
                      name="major"
                      onChange={e => this.setState({ major: e.target.value })}
                      type="text"
                      placeholder="eg. SE/CE/EE/etc"
                      className="form-control"
                    />
                  </div>

                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>
                      Year of passing
                    </label>
                    <input
                      name="year"
                      onChange={e => this.setState({ yop: e.target.value })}
                      type="text"
                      placeholder="eg. 2022"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>GPA</label>
                    <input
                      name="gpa"
                      onChange={e => this.setState({ gpa: e.target.value })}
                      type="text"
                      placeholder="Out of 4"
                      className="form-control"
                    />
                  </div>
                  <div />
                </form>
                <Button
                  className="btn-danger"
                  onClick={
                    this.state.school !== "" ? (
                      this.educationAdd
                    ) : (
                      this.educationUpdate
                    )
                  }
                  variant="primary"
                >
                  {" "}
                  Add
                </Button>

                <Button
                  className="btn-secondary ml-3"
                  onClick={() => {
                    this.setState({ profileUpdateForm: "HideForm" });
                  }}
                  variant="primary"
                >
                  {" "}
                  Cancel
                </Button>
              </div>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

export default StudentEducation;
