import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import {
  getStudentProfilePic,
  updateStudentProfileName
} from "./../../actions/profileActions/profilepicAction";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class StudentProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: localStorage.getItem("SID"),
      profileUpdateForm: "HideForm",
      updateTextValue: this.props.name,
      tempTextUpdate: "",
      profileObj: {},
      picture: "",
      profilePicture: "null"
    };
  }

  updateProfilePic = () => {
    console.log(this.state);
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };

  handleUpdate = () => {
    console.log("start coding handle update pro pic");
    const data = { updateTextValue: this.state.tempTextUpdate };
    this.props.updateStudentProfileName(data);
    this.updateProfilePic();
  };

  updatePic = e => {
    e.preventDefault();

    let picdata = new FormData();
    picdata.append("myimage", this.state.picture);
    picdata.append("name", "profile");
    this.props.updateStudentProfilePic(picdata);
  };

  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
        >
          {console.log(this.props.profilePicture.split("/")[4])}
          {!this.props.profilePicture.includes("null") ? (
            <img
              style={{
                border: "1px solid #ddd",
                borderRadius: "4px",
                padding: "5px",
                width: "150px"
              }}
              src={this.props.profilePicture}
            />
          ) : (
            <form onSubmit={this.updatePic}>
              <button className="style__edit-photo___B-_os">
                <div>
                  <ion-icon
                    size="large"
                    name="camera"
                    style={{ color: "#1569e0" }}
                  />
                </div>

                <div>
                  {" "}
                  <input
                    style={{ color: "#1569e0", fontSize: "13px" }}
                    type="file"
                    name="file"
                    onChange={e => {
                      console.log(e.target.files[0]);
                      this.setState({ picture: e.target.files[0] });
                    }}
                  />
                </div>
              </button>
              <input
                style={{ fontSize: "10px" }}
                type="submit"
                className="btn btn-primary mt-3"
                value="Edit Pic"
              />
            </form>
          )}
          <Card.Body>
            <Card.Title>{this.props.profileObj.name}</Card.Title>

            <Card.Text>{this.props.profileObj.school}</Card.Text>
            {this.props.profileObj.Education ? this.props.profileObj.Education
              .length > 0 ? (
              <div>
                <Card.Text>
                  {this.props.profileObj.Education[0].MAJOR}
                </Card.Text>
                <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
                  {this.props.profileObj.Education[0].DEGREE} • GPA:{" "}
                  {this.props.profileObj.Education[0].CURRENT_GPA}
                </Card.Text>
              </div>
            ) : (
              <div />
            ) : (
              ""
            )}
            <Button
              className="btn-danger"
              onClick={this.updateProfilePic}
              variant="primary"
            >
              Update
            </Button>
          </Card.Body>
        </Card>
        <Card
          bg="light"
          style={{ width: "18rem" }}
          className={this.state.profileUpdateForm + " card_style edu-form"}
        >
          <Card.Body>
            <Form>
              <Form.Group controlId="formName">
                <Card.Title>Update Display Name</Card.Title>
                <input
                  onChange={e => {
                    this.setState({ tempTextUpdate: e.target.value });
                  }}
                  id="display_name"
                  style={{ margin: "20px" }}
                  type="text"
                  placeholder="Enter here"
                />
              </Form.Group>
              <Button
                className="btn-danger"
                onClick={
                  this.state.tempTextUpdate !== "" ? (
                    this.handleUpdate
                  ) : (
                    this.updateProfilePic
                  )
                }
                variant="primary"
              >
                Update
              </Button>
              <Button
                className="btn-secondary ml-3"
                onClick={() => {
                  this.setState({ profileUpdateForm: "HideForm" });
                }}
                variant="primary"
              >
                {" "}
                Cancel
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default StudentProfilePic;
