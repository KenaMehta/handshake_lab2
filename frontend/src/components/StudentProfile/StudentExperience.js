import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import axios from "axios";
import configPath from "./../../configApp";
import { connect } from "react-redux";
import { Redirect } from "react-router";

class StudentExperience extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileUpdateForm: "HideForm",
      deleteExperience: "",
      experienceArr: [],
      deleteFlag: false,
      company_name: "",
      title: "",
      location: "",
      start_dt: "",
      end_dt: "",
      work_desc: ""
    };
  }
  experienceUpdate = () => {
    if (this.state.profileUpdateForm == "DisplayForm")
      this.setState({ profileUpdateForm: "HideForm" });
    else this.setState({ profileUpdateForm: "DisplayForm" });
  };
  experienceDelete = () => {
    console.log("code for delete");
    console.log(this.state.deleteExperience);
    let data = { deleteExperience: this.state.deleteExperience };
    this.props.deleteStudentExperience(data);
    this.setState({ deleteFlag: false });
    this.experienceUpdate();
  };
  experienceAdd = () => {
    console.log("inside experience add");
    const dataAdd = {
      COMPANY_NAME: this.state.company_name,
      TITLE: this.state.title,
      LOCATION: this.state.location,
      START_DT: this.state.start_date,
      END_DT: this.state.end_date,
      WORK_DESC: this.state.work_desc
    };
    this.props.updateStudentExperience(dataAdd);
    this.experienceUpdate();
  };
  render() {
    return (
      <div>
        <Card className="card_style" border="danger" bg="light" align="left">
          <Card.Body>
            <Card.Title>Work and Volunteer Experience</Card.Title>
            <div className="style__divider___1j_Fp mb-3" />
            {this.props.experienceArr.map(experience => (
              <div style={{ marginTop: "20px" }}>
                <div style={{ fontSize: "18px" }}>
                  {experience.COMPANY_NAME}
                </div>
                <div
                  style={{
                    fontSize: "16px",
                    lineHeight: "24px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.TITLE}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px",
                    color: "rgba(0,0,0,.8)"
                  }}
                >
                  {experience.START_DT ? (
                    experience.START_DT.split("T")[0]
                  ) : (
                    ""
                  )}{" "}
                  to {" "}
                  {experience.END_DT ? (
                    experience.END_DT.split("T")[0]
                  ) : (
                    "Present"
                  )}{" "}
                  | {experience.LOCATION}
                </div>
                <div
                  style={{
                    fontSize: "13px",
                    lineHeight: "20px"
                  }}
                >
                  {experience.WORK_DESC}
                </div>
              </div>
            ))}
            <Button
              className="btn-danger"
              style={{ marginTop: "20px", marginBottom: "10px" }}
              onClick={e => {
                this.experienceUpdate();
                this.setState({ deleteFlag: false });
              }}
              variant="primary"
            >
              Add/Update
            </Button>
            <Button
              className="btn-danger"
              style={{
                marginLeft: "20px",
                marginTop: "20px",
                marginBottom: "10px"
              }}
              onClick={e => {
                this.experienceUpdate();
                this.setState({ deleteFlag: true });
              }}
              variant="primary"
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
        {this.state.deleteFlag ? (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Delete Experience</Card.Title>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Company name</label>
                    <input
                      name="company"
                      onChange={e => {
                        this.setState({ deleteExperience: e.target.value });
                      }}
                      type="text"
                      placeholder="Enter company you want to delete"
                      className="form-control"
                    />
                  </div>
                </form>
              </div>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={
                  this.state.deleteExperience !== "" ? (
                    this.experienceDelete
                  ) : (
                    this.experienceUpdate
                  )
                }
                variant="primary"
              >
                Delete
              </Button>
              <Button
                className="btn-danger"
                style={{
                  marginLeft: "20px",
                  marginBottom: "10px"
                }}
                onClick={this.experienceUpdate}
                variant="primary"
              >
                Cancel
              </Button>
            </Card.Body>
          </Card>
        ) : (
          <Card
            bg="light"
            className={this.state.profileUpdateForm + " card_style edu-form"}
            align="left"
          >
            <Card.Body>
              <Card.Title>Add Experience</Card.Title>
              <div className="content margin-top" style={{ margin: "20px" }}>
                <form>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Job Title</label>
                    <input
                      name="school"
                      onChange={e => this.setState({ title: e.target.value })}
                      type="text"
                      placeholder="Enter Company Name"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Employer</label>
                    <input
                      name="name"
                      onChange={e =>
                        this.setState({ company_name: e.target.value })}
                      type="text"
                      placeholder="eg. Enter your employer"
                      className="form-control"
                    />
                  </div>
                  <label style={{ fontWeight: "bold" }}>Time Period</label>
                  <div className="form-group d-flex">
                    <div className="form-group">
                      <label>Start Date</label>
                      <input
                        name="start_date"
                        onChange={e =>
                          this.setState({ start_date: e.target.value })}
                        type="date"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group ml-4">
                      <label>End Date</label>
                      <input
                        name="end_date"
                        onChange={e =>
                          this.setState({ end_date: e.target.value })}
                        type="date"
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>Location</label>
                    <input
                      name="gpa"
                      onChange={e =>
                        this.setState({ location: e.target.value })}
                      type="text"
                      placeholder="Enter the city you worked in"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label style={{ fontWeight: "bold" }}>
                      Work Description
                    </label>
                    <textarea
                      name="gpa"
                      onChange={e =>
                        this.setState({ work_desc: e.target.value })}
                      rows="4"
                      type="textarea"
                      placeholder="Describe your work here"
                      className="form-control"
                    />
                  </div>
                  <div />
                </form>
                <Button
                  className="btn-danger"
                  onClick={
                    this.state.school !== "" ? (
                      this.experienceAdd
                    ) : (
                      this.experienceUpdate
                    )
                  }
                  variant="primary"
                >
                  {" "}
                  Add
                </Button>
                <Button
                  className="btn-secondary ml-3"
                  onClick={() => {
                    this.setState({ profileUpdateForm: "HideForm" });
                  }}
                  variant="primary"
                >
                  {" "}
                  Cancel
                </Button>
              </div>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

export default StudentExperience;
