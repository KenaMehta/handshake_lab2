import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";

class CompanySelectedProfilePic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      location: "",
      description: "",
      phone: "",
      profileUpdateForm: "HideForm",
      profileObj: {},
      picture: "",
      profilePicture: "null"
    };
  }

  render() {
    return (
      <div>
        <Card
          className="card_style"
          border="danger"
          bg="light"
          style={{ width: "18rem" }}
          align="center"
        >
          {console.log(this.props.profilePicture)}
          {!this.props.profilePicture.includes("null") ? (
            <img
              style={{
                border: "1px solid #ddd",
                borderRadius: "4px",
                padding: "5px",
                width: "150px"
              }}
              src={this.props.profilePicture}
            />
          ) : (
            <form onSubmit={this.updatePic}>
              <button className="style__edit-photo___B-_os">
                <div>
                  <ion-icon
                    size="large"
                    name="camera"
                    style={{ color: "#1569e0" }}
                  />
                </div>

                <div>
                  {" "}
                  <input
                    style={{ color: "#1569e0", fontSize: "13px" }}
                    type="file"
                    name="file"
                    onChange={e => {
                      console.log(e.target.files[0]);
                      this.setState({ picture: e.target.files[0] });
                    }}
                  />
                </div>
              </button>
            </form>
          )}

          <Card.Body>
            <Card.Title>{this.props.profileObj.NAME}</Card.Title>

            <Card.Text>{this.props.profileObj.LOCATION}</Card.Text>
            <Card.Text>{this.props.profileObj.PHONE}</Card.Text>
            <Card.Text style={{ color: "rgba(0, 0, 0, 0.56)" }}>
              {this.props.profileObj.DESCRIPTION}
            </Card.Text>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default CompanySelectedProfilePic;
