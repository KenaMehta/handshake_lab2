import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class CompanySelectedJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      job_filter: "none",
      location_filter: "none",
      company_filter: "none",
      page: 1,
      limit: 20
    };
  }

  callGetPage = () => {
    console.log(this.state.category_filter + "filter");
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  };
  componentWillMount() {
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  }
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getJobs({
      category_filter: this.state.category_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: pageNumber
    });
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <Col align="center" className="head_line" md={12}>
                  <h5 style={{ opacity: 0.7 }}>Jobs</h5>
                </Col>
                <div className="d-flex">
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="briefcase-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState({ job_filter: e.target.value }, () => {
                        if (this.state.job_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ job_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on job title"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="location-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4 mr-4"
                    onChange={e => {
                      this.setState({ location_filter: e.target.value }, () => {
                        if (this.state.location_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ location_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on city"
                  />
                </div>
                <div className="d-flex m-2 p-2 justify-content-between">
                  <div>
                    <button
                      style={{ borderRadius: "20px" }}
                      type="button"
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("on-campus button");
                        this.setState({ category_filter: "on-campus" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      On-Campus
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("full-time button");
                        this.setState({ category_filter: "full-time" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      Full Time
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("parttime button");
                        this.setState({ category_filter: "part-time" }, () => {
                          console.log("Changing state");
                          this.callGetPage();
                        });
                      }}
                    >
                      Part Time
                    </button>
                    <button
                      style={{ borderRadius: "20px" }}
                      className="btn btn-outline-secondary m-2"
                      onClick={e => {
                        console.log("internship button");
                        this.setState({ category_filter: "internship" }, () => {
                          console.log(this.state.category_filter);
                          this.callGetPage();
                        });
                      }}
                    >
                      Internship
                    </button>
                  </div>
                </div>
                <div className="style__divider___1j_Fp mb-2" />
                <div className="style__jobs___3seWY p-2">
                  <div>
                    {this.props.jobArr.map(job => (
                      <div>
                        <div className="e mb-3">
                          <div className="d-flex">
                            <div className="p-2 col-2">
                              {job.CID ? job.CID.PROFILE_PIC != null ? (
                                <img
                                  style={{
                                    border: "1px solid #ddd",
                                    borderRadius: "4px",
                                    padding: "5px",
                                    width: "60px"
                                  }}
                                  src={
                                    `${configPath.base}/images/` +
                                    job.CID.PROFILE_PIC
                                  }
                                />
                              ) : (
                                <div className="mt-3 ml-3">
                                  <ion-icon
                                    size="large"
                                    name="person-outline"
                                  />
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div>
                              <div
                                style={{
                                  fontWeight: "bold",
                                  fontSize: "18px"
                                }}
                              >
                                {job.JOB_TITLE}
                              </div>
                              <div
                                style={{
                                  fontSize: "16px"
                                }}
                              >
                                {job.C_NAME} - {job.CITY}, {job.STATE}
                              </div>
                              <div
                                style={{
                                  fontSize: "14px",
                                  color: "rgba(0,0,0,.56)"
                                }}
                              >
                                {job.JOB_CATEGORY} job
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div 
               
                className="align-self-center">
                  <Pagination
                  hideFirstLastPages
                    activePage={this.state.page}
                    itemsCountPerPage={this.state.limit}
                    totalItemsCount={this.props.totalDocuments}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default CompanySelectedJobs;
