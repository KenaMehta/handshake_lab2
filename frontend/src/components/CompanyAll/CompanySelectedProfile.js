import React, { Component } from "react";
import CompanySelectedProfilePic from "./CompanySelectedProfilePic";
import CompanySelectedJobs from "./CompanySelectedJobs";
import cookie from "react-cookies";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import {
  getProfile,
  getJobs
} from "./../../actions/companySelectedProfileAction/profileSelectedAction";

class CompanySelectedProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    this.props.getProfile();
  }
  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Profile</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={4}>
              <CompanySelectedProfilePic
                profileObj={this.props.profileObj}
                profilePicture={this.props.profilePicture}
                getProfile={this.props.getProfile}
              />
            </Col>
            <Col md={8}>
              <CompanySelectedJobs
                jobArr={this.props.jobArr}
                getJobs={this.props.getJobs}
                totalDocuments={this.props.totalDocuments}
                totalPages={this.props.totalPages}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "inside company profile mapStateToProps " +
      JSON.stringify(state.companyProfileReducer)
  );
  return {
    profileObj: state.companyProfileReducer.profileObj || {},
    profilePicture: state.companyProfileReducer.profilePicture || "null",
    jobArr: state.companyProfileReducer.jobArr || [],
    totalDocuments: state.companyProfileReducer.total,
    totalPages: state.companyProfileReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  console.log("calling thunk for propic");
  return {
    //company Profile
    getProfile: payload => dispatch(getProfile(payload)),
    //company Job
    getJobs: payload => dispatch(getJobs(payload))
  };
};

//export CompanyProfile Component
export default connect(mapStateToProps, mapDispatchToProps)(
  CompanySelectedProfile
);
