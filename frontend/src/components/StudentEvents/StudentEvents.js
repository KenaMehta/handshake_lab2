import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/events.css";
import { connect } from "react-redux";
//importing actions
import {
  getEvents,
  getSelectedEvent,
  registerEvent,
  getRegisteredEvents
} from "./../../actions/studentEventActions/studentEventAction";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";

class StudentEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event_filter: "none",
      modalBox: "HideBox",
      showRegButton: true,
      page: "1",
      limit: "2"
    };
  }

  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };

  displayEvent = event_id => {
    console.log("in getPage");
    this.props.getSelectedEvent(event_id);
  };

  componentWillMount() {
    this.props.getEvents({
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: this.state.page
    });
    this.props.getRegisteredEvents();
  }
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getEvents({
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: pageNumber
    });
  };
  callGetPage = () => {
    console.log(this.state.event_filter + "filter");
    this.props.getEvents({
      event_filter: this.state.event_filter,
      limit: this.state.limit,
      page: this.state.page
    });
  };

  studentApply = e => {
    console.log("In studentApply");
    var data = {
      SID: localStorage.getItem("SID"),
      E_CODE: e.E_CODE,
      ELIGIBILITY: e.ELIGIBILITY
    };
    this.props.registerEvent(data);
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Events</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-3 d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title p-2" align="center">
                    List of Events
                  </h4>
                  <div className="style__divider___1j_Fp mb-3" />
                  <div
                    className="d-flex mb-3"
                    style={{ left: "-10px", position: "relative" }}
                  >
                    <div
                      className="m-2"
                      style={{ left: "30px", top: "7px", position: "relative" }}
                    >
                      <ion-icon name="search-outline" />
                    </div>
                    <input
                      style={{ width: "auto" }}
                      className="mt-2 pl-4"
                      onChange={e => {
                        this.setState({ event_filter: e.target.value }, () => {
                          if (this.state.event_filter) {
                            this.callGetPage();
                          } else {
                            this.setState({ event_filter: "none" }, () => {
                              this.callGetPage();
                            });
                          }
                        });
                      }}
                      id="display_name"
                      //style={{ margin: "20px" }}
                      type="text"
                      placeholder="Filter on Event"
                    />
                  </div>
                  <div>
                    {this.props.eventArr.map(events => (
                      <div
                        onClick={e => {
                          console.log(events._id + " in div onclick");
                          this.setState({ showRegButton: true });
                          this.setState({ application_status: "Register" });
                          this.displayEvent(events._id);
                        }}
                        className="e mb-3"
                      >
                        <div className="d-flex">
                          <div className="p-2 col-3">
                            {events.CID.PROFILE_PIC != null ? (
                              <img
                                style={{
                                  border: "1px solid #ddd",
                                  borderRadius: "4px",
                                  padding: "5px",
                                  width: "50px"
                                }}
                                src={
                                  `${configPath.base}/images/` +
                                  events.CID.PROFILE_PIC
                                }
                              />
                            ) : (
                              <div className="mt-3 ml-3">
                                <ion-icon size="large" name="person-outline" />
                              </div>
                            )}
                          </div>
                          <div className="p-2">
                            <div
                              style={{
                                fontWeight: "500",
                                fontSize: "13px"
                              }}
                            >
                              {events.E_NAME}
                            </div>

                            <div
                              style={{
                                fontSize: "12px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {events.DATE.split("T")[0]}
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="align-self-center">
                  <Pagination
                    activePage={this.state.page}
                    itemsCountPerPage={this.state.limit}
                    totalItemsCount={this.props.totalDocuments}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Card>
            </Col>
            <Col md={6}>
              {this.props.eventObj ? (
                <div>
                  <Card
                    bg="light"
                    className="mt-3 d-flex p-2"
                    style={{
                      width: "auto",
                      marginBottom: "20px",
                      height: "500px"
                    }}
                  >
                    <div className="style__jobs___3seWY p-2">
                      <div className="d-flex">
                        <div className="p-2 col-2">
                          {this.props.eventObj.CID ? this.props.eventObj.CID
                            .PROFILE_PIC != null ? (
                            <img
                              style={{
                                border: "1px solid #ddd",
                                borderRadius: "4px",
                                padding: "5px",
                                width: "70px"
                              }}
                              src={
                                `${configPath.base}/images/` +
                                this.props.eventObj.CID.PROFILE_PIC
                              }
                            />
                          ) : (
                            <div className="mt-3 ml-3">
                              <ion-icon size="large" name="person-outline" />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="p-2">
                          <div style={{ fontSize: "24px", fontWeight: "500" }}>
                            {this.props.eventObj.E_NAME}
                          </div>

                          <div>
                            <div className="d-flex">
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="calendar-outline"
                              />

                              <div
                                style={{
                                  left: "15px",
                                  marginLeft: "5px",
                                  fontSize: "13px",
                                  color: "rgba(0,0,0,.56)"
                                }}
                              >
                                {this.props.eventObj.DATE ? (
                                  this.props.eventObj.DATE.split("T")[0]
                                ) : (
                                  ""
                                )}, {this.props.eventObj.TIME} PDT
                              </div>
                            </div>
                            <div className="d-flex">
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="eye-outline"
                              />

                              <div
                                style={{
                                  left: "15px",
                                  marginLeft: "5px",
                                  fontSize: "13px",
                                  color: "rgba(0,0,0,.56)"
                                }}
                              >
                                Any Handshake student with a link to this event
                                can view and RSVP
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="style__card___31yrn mt-2 d-flex justify-content-between">
                        <div className="style__large___3qwwG mt-2">
                          RSVP for the Event here{" "}
                        </div>

                        {this.state.showRegButton ? this.props
                          .application_status == "Register" ? (
                          <button
                            onClick={() => {
                              this.studentApply({
                                E_CODE: this.props.eventObj._id,
                                ELIGIBILITY: this.props.eventObj.ELIGIBILITY
                              });
                            }}
                            type="button"
                            className="btn btn-outline-danger style__base___hEhR9"
                          >
                            <div className="d-flex">
                              <div
                                style={{
                                  top: "2px",
                                  position: "relative"
                                }}
                              >
                                <ion-icon name="add-outline" />
                              </div>

                              <span>{this.props.application_status}</span>
                              <span style={{ fontWeight: "bold" }} />
                            </div>
                          </button>
                        ) : (
                          <button
                            onClick={this.studentApply}
                            type="button"
                            className="btn btn-danger style__base___hEhR9"
                          >
                            <div className="d-flex">
                              <div
                                style={{
                                  top: "2px",
                                  position: "relative"
                                }}
                              />

                              <span>{this.props.application_status}</span>
                              <span style={{ fontWeight: "bold" }} />
                            </div>
                          </button>
                        ) : (
                          <div />
                        )}
                      </div>
                      <div>{this.props.eventObj.E_DESC}</div>
                    </div>
                  </Card>
                </div>
              ) : (
                <div />
              )}
            </Col>
            <Col md={3}>
              <Card
                bg="light"
                className="mt-3 ml-3 d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title mb-4 p-3" align="center">
                    Your Registered Events
                  </h4>
                  <div className="style__divider___1j_Fp mb-3" />
                  <div>
                    {this.props.eventRegArr.map(events => (
                      <div
                        onClick={e => {
                          console.log(events._id + " in div onclick");
                          this.displayEvent(events.EID._id);
                          this.setState({ showRegButton: false });
                        }}
                        className="e mb-3"
                      >
                        <div
                          style={{
                            fontWeight: "500",
                            fontSize: "15px"
                          }}
                        >
                          {events.EID ? events.EID.E_NAME : ""}
                        </div>

                        <div
                          style={{
                            fontSize: "13px",
                            color: "rgba(0,0,0,.56)"
                          }}
                        >
                          {events.EID ? events.EID.DATE ? (
                            events.EID.DATE.split("T")[0]
                          ) : (
                            ""
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

//get props from redux store
const mapStateToProps = state => {
  console.log(
    "inside event student update mapStateToProps " +
      JSON.stringify(state.studentEventReducer)
  );
  return {
    eventArr: state.studentEventReducer.eventArr || [],
    eventObj: state.studentEventReducer.eventObj || {},
    eventRegArr: state.studentEventReducer.eventRegArr || [],
    application_status: state.studentEventReducer.application_status || "",
    totalDocuments: state.studentEventReducer.total,
    totalPages: state.studentEventReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    //get events
    getEvents: payload => dispatch(getEvents(payload)),
    //get selected event
    getSelectedEvent: payload => dispatch(getSelectedEvent(payload)),
    //register event
    registerEvent: payload => dispatch(registerEvent(payload)),
    //get registered events
    getRegisteredEvents: () => dispatch(getRegisteredEvents())
  };
};

//export StudentEvents Component
export default connect(mapStateToProps, mapDispatchToProps)(StudentEvents);
