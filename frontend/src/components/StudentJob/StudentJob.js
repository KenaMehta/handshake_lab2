import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Image
} from "react-bootstrap";
import "../styles/components.css";
import "../styles/job.css";
import {
  getJobs,
  getSelectedJob,
  applyJob,
  refreshStatus
} from "./../../actions/studentJobAction/studentJobAction";
import { connect } from "react-redux";
import cookie from "react-cookies";
import { Redirect } from "react-router";
import axios from "axios";
import configPath from "./../../configApp";
import Pagination from "react-js-pagination";
// require("./../../../node_modules/bootstrap-less/bootstrap");
// import './bootstrap/bootstrap.less'

class StudentJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_filter: "none",
      company_filter: "none",
      job_filter: "none",
      location_filter: "none",
      jobArr: [],
      jobObj: {},
      modalBox: "HideBox",
      application_status: "",
      resume: "",
      selectedCompany: "",
      redirect: false,
      page: "1",
      limit: "2",
      sortOn: "",
      sortOrder: ""
    };
  }
  modalUpdate = () => {
    this.setState({ application_status: "" });
    if (this.state.modalBox == "DisplayBox")
      this.setState({ modalBox: "HideBox" });
    else this.setState({ modalBox: "DisplayBox" });
  };
  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getJobs({
      category_filter: this.state.category_filter,
      company_filter: this.state.company_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: pageNumber,
      sortOrder: this.state.sortOrder,
      sortOn: this.state.sortOn
    });
  };
  componentWillMount() {
    this.props.getJobs({
      category_filter: this.state.category_filter,
      company_filter: this.state.company_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page,
      sortOrder: this.state.sortOrder,
      sortOn: this.state.sortOn
    });
  }
  displayJob = job_id => {
    console.log("in getPage");
    this.props.getSelectedJob(job_id);
  };

  callGetPage = () => {
    console.log(this.state.category_filter + "filter");
    this.props.getJobs({
      category_filter: this.state.category_filter,
      company_filter: this.state.company_filter,
      job_filter: this.state.job_filter,
      location_filter: this.state.location_filter,
      limit: this.state.limit,
      page: this.state.page,
      sortOrder: this.state.sortOrder,
      sortOn: this.state.sortOn
    });
  };
  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    const redirectPath = "/company/selected_company/" + this.state.selectedCID;
    if (this.state.redirect) {
      return <Redirect to={redirectPath} />;
    }
  };
  studentApply = e => {
    e.preventDefault();
    //e.target.reset();
    console.log("In studentApply");
    const data = new FormData();
    console.log(this.state.selectedFile);
    data.append("resume", this.state.selectedFile);
    data.append("SID", localStorage.getItem("SID"));
    data.append("JOB_CODE", this.props.jobObj._id);
    this.props.applyJob(data);
  };

  render() {
    if (localStorage.getItem("loginFlag") != "true") {
      return <Redirect to="/login" />;
    }
    return (
      <div>
        {this.renderRedirect()}
        <Row align="center">
          <Col className="head_line" md={12}>
            <h2 style={{ opacity: 0.7 }}>Jobs</h2>
          </Col>
        </Row>
        <Container>
          <Row>
            <Col md={12}>
              <Card
                border="danger"
                bg="light"
                className="card_style d-flex m-3"
                style={{ width: "auto" }}
              >
                <div className="d-flex p-2">
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="podium-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState({ company_filter: e.target.value }, () => {
                        if (this.state.company_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ company_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on company"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="briefcase-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState({ job_filter: e.target.value }, () => {
                        if (this.state.job_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ job_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on job title"
                  />
                  <div
                    className="m-2"
                    style={{ left: "30px", top: "19px", position: "relative" }}
                  >
                    <ion-icon name="location-outline" />
                  </div>
                  <input
                    className="form-control mt-3 pl-4"
                    onChange={e => {
                      this.setState({ location_filter: e.target.value }, () => {
                        if (this.state.location_filter) {
                          this.callGetPage();
                        } else {
                          this.setState({ location_filter: "none" }, () => {
                            this.callGetPage();
                          });
                        }
                      });
                    }}
                    id="display_name"
                    //style={{ margin: "20px" }}
                    type="text"
                    placeholder="Filter on city"
                  />
                </div>
                <div className="d-flex m-3 p-2">
                  <button
                    style={{ borderRadius: "20px" }}
                    type="button"
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("on-campus button");
                      this.setState({ category_filter: "on-campus" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    On-Campus
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("full-time button");
                      this.setState({ category_filter: "full-time" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    Full Time
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("parttime button");
                      this.setState({ category_filter: "part-time" }, () => {
                        console.log("Changing state");
                        this.callGetPage();
                      });
                    }}
                  >
                    Part Time
                  </button>
                  <button
                    style={{ borderRadius: "20px" }}
                    className="btn btn-outline-secondary m-2"
                    onClick={e => {
                      console.log("internship button");
                      this.setState({ category_filter: "internship" }, () => {
                        console.log(this.state.category_filter);
                        this.callGetPage();
                      });
                    }}
                  >
                    Internship
                  </button>
                </div>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Card
                bg="light"
                className="card_style d-flex"
                style={{ width: "auto", marginBottom: "20px", height: "500px" }}
              >
                <div className="style__jobs___3seWY p-2">
                  <h4 className="card-title" align="center">
                    List of Jobs
                  </h4>
                  <div className="style__divider___1j_Fp" />

                  <span
                    style={{
                      fontSize: "13px",
                      color: "rgba(0,0,0,.66)"
                    }}
                  >
                    Sort on :
                  </span>
                  <span
                    onClick={() => {
                      this.setState({ sortOn: "LOCATION" }, () => {
                        this.state.sortOrder == "1"
                          ? this.setState({ sortOrder: "-1" }, () => {
                              this.callGetPage();
                            })
                          : this.setState({ sortOrder: "1" }, () => {
                              this.callGetPage();
                            });
                      });
                    }}
                    className="e ml-1 mr-2"
                    style={{
                      fontSize: "12px",
                      color: "rgba(0,0,0,.66)"
                    }}
                  >
                    Location
                  </span>
                  <span
                    onClick={() => {
                      this.setState({ sortOn: "Created_At" }, () => {
                        this.state.sortOrder == "1"
                          ? this.setState({ sortOrder: "-1" }, () => {
                              this.callGetPage();
                            })
                          : this.setState({ sortOrder: "1" }, () => {
                              this.callGetPage();
                            });
                      });
                    }}
                    className="e mr-2"
                    style={{
                      fontSize: "12px",
                      color: "rgba(0,0,0,.66)"
                    }}
                  >
                    Posting date
                  </span>

                  <span
                    onClick={() => {
                      this.setState({ sortOn: "APP_DEADLINE" }, () => {
                        this.state.sortOrder == "1"
                          ? this.setState({ sortOrder: "-1" }, () => {
                              this.callGetPage();
                            })
                          : this.setState({ sortOrder: "1" }, () => {
                              this.callGetPage();
                            });
                      });
                    }}
                    className="e mr-2"
                    style={{
                      fontSize: "12px",
                      color: "rgba(0,0,0,.66)"
                    }}
                  >
                    Deadline
                  </span>
                  {this.state.sortOrder ? this.state.sortOrder == "1" ? (
                    <span>
                      <ion-icon name="arrow-up-outline" />
                    </span>
                  ) : (
                    <span>
                      <ion-icon name="arrow-down-outline" />
                    </span>
                  ) : (
                    ""
                  )}
                  <div className="style__divider___1j_Fp mb-3" />
                  <div>
                    {this.props.jobArr.map(job => (
                      <div
                        onClick={e => {
                          console.log(job._id + " in div onclick");
                          this.displayJob(job._id);
                        }}
                        className="e mb-3"
                      >
                        <div className="d-flex">
                          <div className="p-2 col-3">
                            {job.CID.PROFILE_PIC != null ? (
                              <img
                                style={{
                                  border: "1px solid #ddd",
                                  borderRadius: "4px",
                                  padding: "5px",
                                  width: "50px"
                                }}
                                src={
                                  `${configPath.base}/images/` +
                                  job.CID.PROFILE_PIC
                                }
                              />
                            ) : (
                              <div className="mt-3 ml-3">
                                <ion-icon size="large" name="person-outline" />
                              </div>
                            )}
                          </div>
                          <div className="p-2">
                            <div
                              style={{
                                fontWeight: "bold",
                                fontSize: "13px"
                              }}
                            >
                              {job.JOB_TITLE}
                            </div>
                            <div
                              style={{
                                fontSize: "12px"
                              }}
                            >
                              {job.C_NAME} - {job.CITY}, {job.STATE}
                            </div>
                            <div
                              style={{
                                fontSize: "12px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {job.JOB_CATEGORY} job
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="align-self-center">
                  <Pagination
                    activePage={this.state.page}
                    itemsCountPerPage={this.state.limit}
                    totalItemsCount={this.props.totalDocuments}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}
                  />
                </div>
              </Card>
            </Col>
            <Col md={8}>
              {this.props.jobObj ? (
                <div>
                  <Card
                    bg="light"
                    className="card_style d-flex m-3 p-2"
                    style={{
                      width: "auto",
                      marginBottom: "20px",
                      height: "500px"
                    }}
                  >
                    <div className="style__jobs___3seWY p-2">
                      <div className="d-flex">
                        <div className="p-2 col-2">
                          {this.props.jobObj.CID ? this.props.jobObj.CID
                            .PROFILE_PIC != null ? (
                            <img
                              style={{
                                border: "1px solid #ddd",
                                borderRadius: "4px",
                                padding: "5px",
                                width: "70px"
                              }}
                              src={
                                `${configPath.base}/images/` +
                                this.props.jobObj.CID.PROFILE_PIC
                              }
                            />
                          ) : (
                            <div className="mt-3 ml-3">
                              <ion-icon size="large" name="person-outline" />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          <div style={{ fontSize: "24px", fontWeight: "500" }}>
                            {this.props.jobObj.JOB_TITLE}
                          </div>
                          <div
                            className="e_c"
                            style={{ fontSize: "16px", fontWeight: "400" }}
                            onClick={e => {
                              console.log(
                                this.props.jobObj.CID._id + " in div onclick"
                              );
                              localStorage.setItem(
                                "Selected_CID",
                                this.props.jobObj.CID._id
                              );
                              localStorage.setItem(
                                "Selected_Company",
                                this.props.jobObj.C_NAME
                              );
                              this.setState(
                                { selectedCID: this.props.jobObj.CID._id },
                                () => this.setRedirect()
                              );
                            }}
                          >
                            {this.props.jobObj.C_NAME}
                          </div>
                          <div className="d-flex">
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="briefcase-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.props.jobObj.JOB_CATEGORY} job •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="location-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.props.jobObj.CITY},{" "}
                              {this.props.jobObj.STATE} •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="cash-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              {this.props.jobObj.SALARY} •{"  "}
                            </div>
                            <div>
                              <ion-icon
                                style={{
                                  opacity: "0.6"
                                }}
                                name="time-outline"
                              />
                            </div>
                            <div
                              style={{
                                left: "15px",
                                fontSize: "14px",
                                color: "rgba(0,0,0,.56)"
                              }}
                            >
                              Posted on{" "}
                              {this.props.jobObj.Created_At ? (
                                this.props.jobObj.Created_At.split("T")[0]
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="style__card___31yrn mt-2 d-flex justify-content-between">
                        <div className="style__large___3qwwG mt-2">
                          Applications close on{" "}
                          {this.props.jobObj.APP_DEADLINE ? (
                            this.props.jobObj.APP_DEADLINE.split("T")[0]
                          ) : (
                            ""
                          )}
                        </div>

                        <button
                          onClick={this.modalUpdate}
                          type="button"
                          className="btn btn-outline-danger style__base___hEhR9"
                        >
                          <span>Apply</span>
                        </button>
                      </div>
                      <div>{this.props.jobObj.JOB_DESC}</div>
                    </div>
                  </Card>
                  <Card className={this.state.modalBox + " modal"}>
                    <div className="modal-content">
                      <Container>
                        <span
                          className="close"
                          onClick={() => {
                            this.modalUpdate();
                            this.props.refreshStatus({
                              application_status: ""
                            });
                          }}
                        >
                          &times;
                        </span>
                        <div>
                          <h2 className="style__heading___29i1Z">
                            <span>Apply to {this.props.jobObj.C_NAME}</span>
                          </h2>
                        </div>
                        <div>
                          <h3 className="style__heading___29i1Z style__medium___m_Ip7">
                            Details from {this.props.jobObj.C_NAME}:
                          </h3>
                        </div>
                        <div className="style__text___2ilXR">
                          Applying for {this.props.jobObj.JOB_TITLE} requires a
                          few documents. Attach them below and get one step
                          closer to your next job!
                        </div>
                        <form className="mt-3" onSubmit={this.studentApply}>
                          <label>Upload your resume below:</label>
                          <div className="d-flex">
                            <div className="m-3">
                              <input
                                type="file"
                                name="resume"
                                id="resume"
                                onChange={event => {
                                  console.log(event.target.files[0]);
                                  this.setState({
                                    selectedFile: event.target.files[0]
                                  });
                                }}
                              />
                            </div>
                            <div className="mt-2">
                              {this.props.resume ? (
                                <a
                                  href={this.props.resume}
                                  download="Resume"
                                  target="_blank"
                                >
                                  Download
                                </a>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                          <div className="m-3">
                            <input type="submit" value="Submit" />

                            <span style={{ fontWeight: "bold", color: "red" }}>
                              {this.props.application_status}
                            </span>
                          </div>
                        </form>
                      </Container>
                    </div>
                  </Card>
                </div>
              ) : (
                <div />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log(
    "inside job student update mapStateToProps " +
      JSON.stringify(state.studentJobReducer)
  );
  return {
    jobArr: state.studentJobReducer.jobArr || [],
    jobObj: state.studentJobReducer.jobObj || {},
    resume: state.studentJobReducer.resume || "",
    application_status: state.studentJobReducer.application_status || "",
    totalDocuments: state.studentJobReducer.total,
    totalPages: state.studentJobReducer.pages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    //get jobs
    getJobs: payload => dispatch(getJobs(payload)),
    //get selected job
    getSelectedJob: payload => dispatch(getSelectedJob(payload)),
    //apply job
    applyJob: payload => dispatch(applyJob(payload)),
    //refresh status
    refreshStatus: payload => dispatch(refreshStatus(payload))
  };
};

//export StudentJobs Component
export default connect(mapStateToProps, mapDispatchToProps)(StudentJobs);
