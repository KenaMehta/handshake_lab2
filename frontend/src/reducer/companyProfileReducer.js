import {
  GETPROFILE,
  UPDATEPROFILE,
  UPLOADPROFILEPIC,
  GETJOBS,
  ADDJOB
} from "../actions/companyProfileActions/actionTypes";

const initialState = {};

const companyProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETPROFILE: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPDATEPROFILE: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPLOADPROFILEPIC: {
      return {
        ...state,
        ...action.payload
      };
    }
    case GETJOBS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case ADDJOB: {
      return {
        ...state,
        ...action.payload
      };
    }
    case "REFRESHSTATUS": {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default companyProfileReducer;
