import {
  GETMESSAGES,
  GETSELECTEDMESSAGE,
  SENDMESSAGE
} from "../actions/companyMessageAction/actionTypes";

const initialState = {};

const companyMessageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETMESSAGES: {
      return {
        ...state,
        ...action.payload
      };
    }
    case GETSELECTEDMESSAGE: {
      return {
        ...state,
        ...action.payload
      };
    }
    case SENDMESSAGE: {
      return {
        ...state,
        ...action.payload
      };
    }
    case "REFRESHSTATUS": {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default companyMessageReducer;
