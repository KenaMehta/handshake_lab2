import { GETAPPLICATIONS } from "../actions/studentApplicationAction/actionTypes";

const initialState = {};

const studentApplicationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETAPPLICATIONS: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default studentApplicationReducer;
