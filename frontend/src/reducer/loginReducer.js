import { LOGIN, LOGOUT } from "../actions/profileActions/actionTypes";

const initialState = {
  loginFlag: false,
  logoutFlag: false
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      console.log(action.payload);
      localStorage.setItem("company_name", action.payload.name);
      return {
        id: action.payload.id,
        idToken: action.payload.idToken,
        res: action.payload.res,
        loginFlag: action.payload.loginFlag,
        category: action.payload.category,
        name: action.payload.name
      };
    }
    case LOGOUT: {
      return {
        loginFlag: false
      };
    }
    default:
      return state;
  }
};

export default loginReducer;
