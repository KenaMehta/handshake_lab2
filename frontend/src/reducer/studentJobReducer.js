import {
  GETJOBS,
  GETSELECTEDJOB,
  APPLYJOB
} from "../actions/studentJobAction/actionTypes";

const initialState = {};

const studentJobReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETJOBS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case GETSELECTEDJOB: {
      return {
        ...state,
        ...action.payload
      };
    }
    case APPLYJOB: {
      return {
        ...state,
        ...action.payload
      };
    }
    case "REFRESHSTATUS": {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default studentJobReducer;
