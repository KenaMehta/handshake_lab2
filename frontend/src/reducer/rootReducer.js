//importing individual reducers
import loginReducer from "./loginReducer";
import registerReducer from "./registerReducer";
import studentProfileReducer from "./studentProfileReducer";
import studentAllReducer from "./studentAllReducer";
import studentJobReducer from "./studentJobReducer";
import studentEventReducer from "./studentEventReducer";
import studentApplicationReducer from "./studentApplicationReducer";
import companyProfileReducer from "./companyProfileReducer";
import companyStudentReducer from "./companyStudentReducer";
import companyEventReducer from "./companyEventReducer";
import companyMessageReducer from "./companyMessageReducer";
import studentMessageReducer from "./studentMessageReducer";
import { combineReducers } from "redux";

//combining all reducers to form single root reducer
const rootReducer = combineReducers({
  loginReducer,
  registerReducer,
  //student reducers
  studentProfileReducer,
  studentAllReducer,
  studentJobReducer,
  studentEventReducer,
  studentApplicationReducer,
  //company reducers
  companyProfileReducer,
  companyStudentReducer,
  companyEventReducer,
  //messages
  companyMessageReducer,
  studentMessageReducer
});

//exporting the root reducer
export default rootReducer;
