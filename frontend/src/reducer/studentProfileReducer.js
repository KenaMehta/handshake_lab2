import {
  GETPROFILEPIC,
  UPDATEPROFILENAME,
  GETSTUDENTJOURNEY,
  UPDATESTUDENTJOURNEY,
  UPDATEPROFILEPIC,
  UPDATESTUDENTEDUCATION,
  DELETESTUDENTEDUCATION,
  UPDATESTUDENTEXPERIENCE,
  DELETESTUDENTEXPERIENCE,
  UPDATESTUDENTSKILL,
  DELETESTUDENTSKILL,
  UPDATESTUDENTBASICDETAIL
} from "../actions/profileActions/actionTypes";

const initialState = {};
  
const studentProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETPROFILEPIC: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPDATEPROFILENAME: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          name: action.payload.updateTextValue
        }
      };
    }
    case UPDATEPROFILEPIC: {
      return {
        ...state,
        ...action.payload
      };
    }
    case GETSTUDENTJOURNEY: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPDATESTUDENTJOURNEY: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPDATESTUDENTEDUCATION: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Education: action.payload
        }
      };
    }
    case DELETESTUDENTEDUCATION: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Education: action.payload
        }
      };
    }
    case UPDATESTUDENTEXPERIENCE: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Experience: action.payload
        }
      };
    }
    case DELETESTUDENTEXPERIENCE: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Experience: action.payload
        }
      };
    }
    case UPDATESTUDENTSKILL: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Skill: action.payload
        }
      };
    }
    case DELETESTUDENTSKILL: {
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          Skill: action.payload
        }
      };
    }
    case UPDATESTUDENTBASICDETAIL: {
      console.log("in red, " + action.payload);
      return {
        ...state,
        profileObj: {
          ...state.profileObj,
          ...action.payload
        }
      };
    }
    default:
      return state;
  }
};

export default studentProfileReducer;
