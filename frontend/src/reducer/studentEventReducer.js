import {
  GETEVENTS,
  GETSELECTEDEVENT,
  GETREGISTEREDEVENTS,
  REGISTEREVENT
} from "../actions/studentEventActions/actionTypes";

const initialState = {};

const studentEventReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETEVENTS: {
      return {
        ...state,
        ...action.payload,
        application_status: "Register"
      };
    }
    case GETSELECTEDEVENT: {
      return {
        ...state,
        ...action.payload,
        application_status: "Register"
      };
    }
    case GETREGISTEREDEVENTS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case REGISTEREVENT: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default studentEventReducer;
