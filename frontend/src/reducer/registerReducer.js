import { REGISTERSTUDENT, REGISTERCOMPANY } from "../actions/profileActions/actionTypes";

const initialState = {
  res: "",
  registerFlag: ""
};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTERSTUDENT: {
      return {
        res: action.payload.res,
        registerFlag: action.payload.registerFlag
      };
    }
    case REGISTERCOMPANY: {
      return {
        res: action.payload.res,
        registerFlag: action.payload.registerFlag
      };
    }
    default:
      return state;
  }
};

export default registerReducer;
