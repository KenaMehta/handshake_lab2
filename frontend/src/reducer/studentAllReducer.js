import { GETSELECTEDSTUDENT } from "../actions/studentAllActions/actionTypes";

const initialState = {};

const studentAllReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETSELECTEDSTUDENT: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default studentAllReducer;
