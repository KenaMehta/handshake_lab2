import { GETEVENTS, ADDEVENT } from "../actions/companyEventAction/actionTypes";

const initialState = {};

const companyEventReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETEVENTS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case ADDEVENT: {
      return {
        ...state,
        ...action.payload
      };
    }
    case "REFRESHSTATUS": {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default companyEventReducer;
