import {
  GETSTUDENTS,
  UPDATESTATUS,
  GETEVENTSTUDENT
} from "../actions/companyStudentActions/actionTypes";

const initialState = {};

const companyStudentReducer = (state = initialState, action) => {
  switch (action.type) {
    case GETSTUDENTS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case UPDATESTATUS: {
      return {
        ...state,
        ...action.payload
      };
    }
    case "REFRESHSTATUS": {
      return {
        ...state,
        ...action.payload
      };
    }
    case GETEVENTSTUDENT: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
};

export default companyStudentReducer;
