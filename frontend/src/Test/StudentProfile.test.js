import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Login from "./../components/Login/Login";

Enzyme.configure({ adapter: new Adapter() });

describe("Login", () => {
  it("should show text", () => {
    const wrapper = shallow(<Login />);
    const text = wrapper.find("div div div div div h2");
    expect(text.text()).toBe("Login");
  });
});
