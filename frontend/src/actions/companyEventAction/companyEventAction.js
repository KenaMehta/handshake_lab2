//importing reduc action types
import { GETEVENTS, ADDEVENT } from "./actionTypes";

import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting events
const getEventsDispatcher = payload => {
  console.log("Inside getEventsDispatcher action, " + JSON.stringify(payload));
  return {
    type: GETEVENTS,
    payload
  };
};

//dispatcher for adding event
const addEventDispatcher = payload => {
  console.log("Inside addEventDispatcher action, " + JSON.stringify(payload));
  return {
    type: ADDEVENT,
    payload
  };
};

//dispatcher for refreshing status
export const refreshStatus = payload => {
  console.log("Inside refreshStatus action");
  return {
    type: "REFRESHSTATUS",
    payload
  };
};

//thunk for getting company events
export const getEvents = payload => {
  return dispatch => {
    console.log("Inside getEvents thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/company/event_list/${localStorage.getItem(
            "SID"
          )}/${payload.event_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getEventsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting event details"));
  };
};

//thunk for adding event
export const addEvent = payload => {
  console.log("Inside addEvent", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/company/event_list/add_event/${localStorage.getItem(
            "SID"
          )}?page=${payload.page}&limit=${payload.limit}`,
        payload,
        config
      )
      .then(response => {
        console.log(JSON.stringify(response.data));
        dispatch(
          addEventDispatcher({
            ...response.data,
            application_status: "Event Successfully Added"
          })
        );
      })
      .catch(error => {
        console.log(error);
        dispatch(
          addEventDispatcher({
            application_status: "Failed to Add Event"
          })
        );
      });
  };
};
