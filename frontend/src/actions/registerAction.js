import { REGISTERSTUDENT, REGISTERCOMPANY } from "./profileActions/actionTypes";
import configPath from "./../configApp";
import axios from "axios";

//Student Register Dispatacher
const registerStudentDispatcher = payload => {
  console.log("Inside registerStudentDispatcher action");
  console.log("payload", payload);
  return {
    type: REGISTERSTUDENT,
    payload
  };
};

//Company Register Dispatacher
const registerCompanyDispatcher = payload => {
  console.log("Inside registerCompanyDispatcher action");
  console.log("payload", payload);
  return {
    type: REGISTERCOMPANY,
    payload
  };
};

//Delayed dispatch to make async call for Student data
export const registerStudent = payload => {
  console.log("Inside registerStudent thunk");

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    //make a post request with the user data
    axios
      .post(configPath.api_host + "/registerStudent", payload)
      .then(response => {
        console.log("Status Code : ", response.status);
        if (response.status === 200) {
          dispatch(
            registerStudentDispatcher({
              res: "Student record inserted",
              registerFlag: true
            })
          );
        }
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          // console.log(error.response.data);
          dispatch(
            registerStudentDispatcher({
              res: "Error caught while inserting record",
              registerFlag: false
            })
          );
        } else {
          dispatch(
            registerStudentDispatcher({
              res: "Network error",
              registerFlag: false
            })
          );
        }
      });
  };
};

//Delayed dispatch to make async call for Company data
export const registerCompany = payload => {
  console.log("Inside registerCompany thunk");

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    //make a post request with the user data
    axios
      .post(configPath.api_host + "/registerCompany", payload)
      .then(response => {
        console.log("Status Code : ", response.status);
        if (response.status === 200) {
          dispatch(
            registerCompanyDispatcher({
              res: "Company record inserted",
              registerFlag: true
            })
          );
        }
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          // console.log(error.response.data);
          dispatch(
            registerCompanyDispatcher({
              res: "Error caught while inserting record",
              registerFlag: false
            })
          );
        } else {
          dispatch(
            registerCompanyDispatcher({
              res: "Network error",
              registerFlag: false
            })
          );
        }
      });
  };
};
