import {
  GETEVENTS,
  GETSELECTEDEVENT,
  GETREGISTEREDEVENTS,
  REGISTEREVENT
} from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting events
const getEventsDispatcher = payload => {
  console.log("Inside getEventsDispatcher action, " + payload);
  return {
    type: GETEVENTS,
    payload
  };
};

//dispatcher for selected event
const getSelectedEventDispatcher = payload => {
  console.log("Inside getSelectedEventDispatcher action, " + payload);
  return {
    type: GETSELECTEDEVENT,
    payload
  };
};

//dispatcher for applying for event
const registerEventDispatcher = payload => {
  console.log("Inside registerEventDispatcher action, " + payload);
  return {
    type: REGISTEREVENT,
    payload
  };
};

//dispatcher for applying for event
const getRegisteredEventsDispatcher = payload => {
  console.log("Inside getRegisteredEventsDispatcher action, " + payload);
  return {
    type: GETREGISTEREDEVENTS,
    payload
  };
};

//thunk get events
export const getEvents = payload => {
  console.log("Inside getEvents thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/event_list/${payload.event_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getEventsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting event details"));
  };
};

//thunk get selected event
export const getSelectedEvent = payload => {
  console.log("Inside getSelectedEvent thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + `/student/event_list`,
        { event_id: payload },
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getSelectedEventDispatcher({ eventObj: response.data }));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk register event
export const registerEvent = payload => {
  console.log("Inside registerEvent thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + `/student/event_list/event_apply`,
        payload,
        config
      )
      .then(response => {
        console.log(JSON.stringify(response.data) + "iiin stu app");
        dispatch(
          registerEventDispatcher({
            eventRegArr: response.data.eventListArr,
            application_status: response.data.application_status
          })
        );
      })
      .catch(error => {
        console.log(error);
        dispatch(
          registerEventDispatcher({
            application_status: error.response.data.application_status
          })
        );
      });
  };
};

//thunk get registered events
export const getRegisteredEvents = () => {
  console.log("Inside getRegisteredEvents thunk");
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + `/student/event_list/regEvents`,
        {
          SID: localStorage.getItem("SID")
        },
        config
      )
      .then(response => {
        console.log(JSON.stringify(response.data) + "reg events");
        dispatch(getRegisteredEventsDispatcher({ eventRegArr: response.data }));
      })
      .catch(error => {
        console.log(error);
      });
  };
};
