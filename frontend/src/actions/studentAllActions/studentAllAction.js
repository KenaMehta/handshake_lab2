import { GETSELECTEDSTUDENT } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for selected student
const getSelectedStudentDispatcher = payload => {
  console.log("Inside getSelectedStudentDispatcher action, " + payload);
  return {
    type: GETSELECTEDSTUDENT,
    payload
  };
};

//thunk get selected student
export const getSelectedStudent = payload => {
  console.log("Inside getSelectedStudent thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/all_students/${payload.student_filter}/${payload.major_filter}/${payload.skill_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          dispatch(getSelectedStudentDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting event details"));
  };
};
