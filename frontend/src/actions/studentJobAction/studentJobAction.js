import { GETJOBS, GETSELECTEDJOB, APPLYJOB } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting jobs
const getJobsDispatcher = payload => {
  console.log("Inside getJobsDispatcher action, " + payload);
  return {
    type: GETJOBS,
    payload
  };
};

//dispatcher for selected job
const getSelectedJobDispatcher = payload => {
  console.log("Inside getSelectedJobDispatcher action, " + payload);
  return {
    type: GETSELECTEDJOB,
    payload
  };
};

//dispatcher for applying for job
const applyJobDispatcher = payload => {
  console.log("Inside applyJobDispatcher action, " + payload);
  return {
    type: APPLYJOB,
    payload
  };
};

//actiion for refreshing status
export const refreshStatus = payload => {
  return {
    type: "REFRESHSTATUS",
    payload
  };
};
//thunk get jobs
export const getJobs = payload => {
  console.log("Inside getJobs thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/job_list/${payload.category_filter}/${payload.company_filter}/${payload.job_filter}/${payload.location_filter}?limit=${payload.limit}&page=${payload.page}&sortOrder=${payload.sortOrder}&sortOn=${payload.sortOn}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getJobsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk get selected job
export const getSelectedJob = payload => {
  console.log("Inside getSelectedJob thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + `/student/job_list`,
        { job_id: payload },
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getSelectedJobDispatcher({ jobObj: response.data }));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk apply  job
export const applyJob = payload => {
  console.log("Inside getSelectedJobs thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + `/student/job_list/job_apply`,
        payload,
        config
      )
      .then(response => {
        console.log(JSON.stringify(response.data) + "in stu app");
        var path = `${configPath.base}/images/` + `${response.data.resume}`;
        dispatch(
          applyJobDispatcher({
            resume: path,
            application_status: response.data.application_status
          })
        );
      })
      .catch(error => {
        console.log(error);
        dispatch(
          applyJobDispatcher({
            application_status: error.response.data.application_status
          })
        );
      });
  };
};
