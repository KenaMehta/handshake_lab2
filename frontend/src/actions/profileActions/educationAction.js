import { UPDATESTUDENTEDUCATION, DELETESTUDENTEDUCATION } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for updating student Education
const updateStudentEducationDispatcher = payload => {
  console.log("Inside updateStudentJourneyDispatcher action, " + payload);
  return {
    type: UPDATESTUDENTEDUCATION,
    payload
  };
};

//dispatcher for deleting student education
const deleteStudentEducationDispatcher = payload => {
  console.log("Inside deleteStudentJourneyDispatcher action, " + payload);
  return {
    type: DELETESTUDENTEDUCATION,
    payload
  };
};

//thunk update student education
export const updateStudentEducation = payload => {
  console.log("Inside updateStudentEducation thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/student/profile/education/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(updateStudentEducationDispatcher(res.data));
        }
      })
      .catch(err => console.log(err + " err in adding/updating"));
  };
};

export const deleteStudentEducation = payload => {
  console.log("Inside updateStudentEducation thunk", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .delete(
        configPath.api_host +
          `/student/profile/education/${localStorage.getItem("SID")}`,
        {
          data: {
            data: payload
          },
          headers: {
            Authorization: `${window.localStorage.getItem("SIDToken")}`
          }
        },
        config
      )
      .then(res => {
        console.log(JSON.stringify(res.data) + "---> obj after deleting");

        if (res.status === 200) {
          console.log(res.data);
          dispatch(deleteStudentEducationDispatcher(res.data));
        } else console.log("Error in deleting");
      })
      .catch(err => console.log("Error in catch: " + err));
  };
};
