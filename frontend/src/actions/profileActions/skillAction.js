import { UPDATESTUDENTSKILL, DELETESTUDENTSKILL } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for updating student skill
const updateStudentSkillDispatcher = payload => {
  console.log("Inside updateStudentSkillDispatcher action, " + payload);
  return {
    type: UPDATESTUDENTSKILL,
    payload
  };
};

//dispatcher for deleting student skill
const deleteStudentSkillDispatcher = payload => {
  console.log("Inside deleteStudentSkillDispatcher action, " + payload);
  return {
    type: DELETESTUDENTSKILL,
    payload
  };
};

//thunk update student skill
export const updateStudentSkill = payload => {
  console.log("Inside updateStudentSkill thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/student/profile/skills/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(updateStudentSkillDispatcher(res.data));
        }
      })
      .catch(err => console.log(err + " err in adding/updating"));
  };
};

//thunk for deleting student's skill
export const deleteStudentSkill = payload => {
  console.log("Inside deleteStudentSkill thunk", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .delete(
        configPath.api_host +
          `/student/profile/skills/${localStorage.getItem("SID")}`,
        {
          data: { data: payload },
          headers: {
            Authorization: `${window.localStorage.getItem("SIDToken")}`
          }
        }
      )
      .then(res => {
        console.log(JSON.stringify(res.data) + "---> obj after deleting");

        if (res.status === 200) {
          console.log(res.data);
          dispatch(deleteStudentSkillDispatcher(res.data));
        } else console.log("Error in deleting");
      })
      .catch(err => console.log("Error in catch: " + err));
  };
};
