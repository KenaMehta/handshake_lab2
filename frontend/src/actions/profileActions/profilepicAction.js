//importing reduc action types
import {
  GETPROFILEPIC,
  UPDATEPROFILENAME,
  UPDATEPROFILEPIC
} from "./actionTypes";

import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting profile pic details
const getStudentProfilePicDispatcher = payload => {
  console.log("Inside getStudentProfilePicDispatcher action");
  console.log("payload", payload);
  return {
    type: GETPROFILEPIC,
    payload
  };
};

//dispatcher for updating profile name
const updateStudentProfileNameDispatcher = payload => {
  console.log("Inside updateStudentProfileNameDispatcher action");
  return {
    type: UPDATEPROFILENAME,
    payload
  };
};

//dispatcher for updating profile pic
const updateStudentProfilePicDispatcher = payload => {
  console.log(
    "Inside updateStudentProfilePicDispatcher action, " +
      JSON.stringify(payload)
  );
  return {
    type: UPDATEPROFILEPIC,
    payload
  };
};

//thunk for getting student profile
export const getStudentProfilePic = payload => {
  return dispatch => {
    console.log("Inside getStudentProfilePic thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(configPath.api_host + `/student/profile/${payload}`, config)
      .then(response => {
        if (response.status === 200) {
          console.log(
            JSON.stringify(response.data) + " : Axios Profile response"
          );
          var path = `${configPath.base}/images/` + `${response.data.PHOTO}`;
          console.log(path);
          dispatch(
            getStudentProfilePicDispatcher({
              profileObj: response.data,
              profilePicture: response.data.PHOTO ? path : "null",
              res: "success"
            })
          );
        }
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          // console.log(error.response.data);
          dispatch(
            getStudentProfilePicDispatcher({
              res: "Error caught while fetching"
            })
          );
        } else {
          dispatch(
            getStudentProfilePicDispatcher({
              res: "Network error"
            })
          );
        }
      });
  };
};

//thunk for updating students name
export const updateStudentProfileName = payload => {
  console.log("Inside updateStudentProfileNamethunk", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .put(
        configPath.api_host + `/student/profile/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(response => {
        dispatch(updateStudentProfileNameDispatcher(payload));
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          console.log(error.response);
        } else {
          console.log("network error");
        }
      });
  };
};

//thunk for updating profile picture
export const updateStudentProfilePic = payload => {
  console.log("Inside updateStudentProfilePicthunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };

    try {
      console.log("In try block");
      axios
        .post(
          configPath.api_host +
            `/student/profile/picture/${localStorage.getItem("SID")}`,
          payload,
          config
        )
        .then(res => {
          console.log(res.data);
          var path = `${configPath.base}/images/` + `${res.data}`;
          console.log(path);
          dispatch(updateStudentProfilePicDispatcher({ profilePicture: path }));
        })
        .catch(err => {
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  };
};
