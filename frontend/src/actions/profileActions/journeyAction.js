import { GETSTUDENTJOURNEY, UPDATESTUDENTJOURNEY } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

const getStudentJourneyDispatcher = payload => {
  console.log("Inside getStudentJourneyDispatcher action, " + payload);
  return {
    type: GETSTUDENTJOURNEY,
    payload
  };
};

const updateStudentJourneyDispatcher = payload => {
  console.log("Inside updateStudentJourneyDispatcher action, " + payload);
  return {
    type: UPDATESTUDENTJOURNEY,
    payload
  };
};

//thunk for getting student Journey
export const getStudentJourney = payload => {
  console.log("Inside getStudentJourney thunk");

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(configPath.api_host + `/student/profile/journey/${payload}`, config)
      .then(response => {
        console.log("Status Code : ", response.status);
        if (response.status === 200) {
          console.log(response.data);
          dispatch(
            getStudentJourneyDispatcher({ journeyText: response.data.journey })
          );
        }
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          // console.log(error.response.data);
          dispatch(
            getStudentJourneyDispatcher({
              res: "Error caught while fetching"
            })
          );
        } else {
          dispatch(
            getStudentJourneyDispatcher({
              res: "Network error"
            })
          );
        }
      });
  };
};

export const updateStudentJourney = payload => {
  console.log("Inside updateStudentJourney thunk", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    //make a post request with the user data
    console.log("in here");
    console.log(localStorage.getItem("SID") + "dgfddfg");
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .put(
        configPath.api_host +
          `/student/profile/journey/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(response => {
        dispatch(
          updateStudentJourneyDispatcher({ journeyText: payload.journey })
        );
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          console.log(error.response);
        } else {
          console.log("network error");
        }
      });
  };
};
