import {
  UPDATESTUDENTEXPERIENCE,
  DELETESTUDENTEXPERIENCE
} from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for updating student experience
const updateStudentExperienceDispatcher = payload => {
  console.log("Inside updateStudentExperienceDispatcher action, " + payload);
  return {
    type: UPDATESTUDENTEXPERIENCE,
    payload
  };
};

//dispatcher for deleting student experience
const deleteStudentExperienceDispatcher = payload => {
  console.log("Inside deleteStudentExperienceDispatcher action, " + payload);
  return {
    type: DELETESTUDENTEXPERIENCE,
    payload
  };
};

//thunk update student experience
export const updateStudentExperience = payload => {
  console.log("Inside updateStudentExperience thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/student/profile/experience/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(updateStudentExperienceDispatcher(res.data));
        }
      })
      .catch(err => console.log(err + " err in adding/updating"));
  };
};

export const deleteStudentExperience = payload => {
  console.log("Inside updateStudentExperience thunk", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .delete(
        configPath.api_host +
          `/student/profile/experience/${localStorage.getItem("SID")}`,
        {
          data: { data: payload },
          headers: {
            Authorization: `${window.localStorage.getItem("SIDToken")}`
          }
        },
        config
      )
      .then(res => {
        console.log(JSON.stringify(res.data) + "---> obj after deleting");

        if (res.status === 200) {
          console.log(res.data);
          dispatch(deleteStudentExperienceDispatcher(res.data));
        } else console.log("Error in deleting");
      })
      .catch(err => console.log("Error in catch: " + err));
  };
};
