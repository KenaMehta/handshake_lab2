//login/logout
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

//register
export const REGISTERSTUDENT = "REGISTERSTUDENT";
export const REGISTERCOMPANY = "REGISTERCOMPANY";

//Profile Pic upload, name update and get for both
export const GETPROFILEPIC = "GETPROFILEPIC";
export const UPDATEPROFILENAME = "UPDATEPROFILENAME";
export const UPDATEPROFILEPIC = "UPDATEPROFILEPIC";

//student journey
export const GETSTUDENTJOURNEY = "GETSTUDENTJOURNEY";
export const UPDATESTUDENTJOURNEY = "UPDATESTUDENTJOURNEY";

//student education
export const UPDATESTUDENTEDUCATION = "UPDATESTUDENTEDUCATION";
export const DELETESTUDENTEDUCATION = "DELETEEDUCATION";

//student experience
export const UPDATESTUDENTEXPERIENCE = "UPDATESTUDENTEXPERIENCE";
export const DELETESTUDENTEXPERIENCE = "DELETESTUDENTEXPERIENCE";

//student skill
export const UPDATESTUDENTSKILL = "UPDATESTUDENTSKILL";
export const DELETESTUDENTSKILL = "DELETESTUDENTSKILL";

//basic details
export const UPDATESTUDENTBASICDETAIL = "UPDATESTUDENTBASICDETAIL";

