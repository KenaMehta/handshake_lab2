import { UPDATESTUDENTBASICDETAIL } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for updating student basic detail
const updateStudentBasicDetailDispatcher = payload => {
  console.log("Inside updateStudentBasicDetailDispatcher action, " + payload);
  return {
    type: UPDATESTUDENTBASICDETAIL,
    payload
  };
};

//thunk update student basic detail
export const updateStudentBasicDetail = payload => {
  console.log("Inside updateStudentBasicDetail thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/student/profile/basic_details/${localStorage.getItem("SID")}`,
        payload,
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(updateStudentBasicDetailDispatcher(res.data));
        }
      })
      .catch(err => console.log(err + " err in adding/updating"));
  };
};
