//importing reduc action types
import { GETPROFILE, GETJOBS } from "./actionTypes";

import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting profile details
const getProfileDispatcher = payload => {
  console.log("Inside getProfileDispatcher action, " + payload);
  return {
    type: GETPROFILE,
    payload
  };
};

//dispatcher for getting jobs
const getJobsDispatcher = payload => {
  console.log("Inside getJobsDispatcher action, " + JSON.stringify(payload));
  return {
    type: GETJOBS,
    payload
  };
};

//thunk for getting profile
export const getProfile = payload => {
  return dispatch => {
    console.log("Inside getProfile thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          "/company/profile/" +
          localStorage.getItem("Selected_CID"),
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(
            JSON.stringify(response.data) + " : Axios Profile response"
          );
          var path =
            `${configPath.base}/images/` + `${response.data.PROFILE_PIC}`;
          dispatch(
            getProfileDispatcher({
              profilePicture: path,
              profileObj: response.data
            })
          );
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk for getting company jobs
export const getJobs = payload => {
  return dispatch => {
    console.log("Inside getJobs thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/job_list/${payload.category_filter}/${localStorage.getItem(
            "Selected_Company"
          )}/${payload.job_filter}/${payload.location_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          dispatch(getJobsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};
