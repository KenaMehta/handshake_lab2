//importing reduc action types
import {
  GETPROFILE,
  UPDATEPROFILE,
  UPLOADPROFILEPIC,
  GETJOBS,
  ADDJOB
} from "./actionTypes";

import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting profile details
const getProfileDispatcher = payload => {
  console.log("Inside getProfileDispatcher action, " + payload);
  return {
    type: GETPROFILE,
    payload
  };
};

//dispatcher for updating profile
const updateProfileDispatcher = payload => {
  console.log("Inside updateProfileDispatcher action, " + payload);
  return {
    type: UPDATEPROFILE,
    payload
  };
};

//dispatcher for updating profile pic
const uploadProfilePicDispatcher = payload => {
  console.log(
    "Inside uploadProfilePicDispatcher action, " + JSON.stringify(payload)
  );
  return {
    type: UPLOADPROFILEPIC,
    payload
  };
};

//dispatcher for getting jobs
const getJobsDispatcher = payload => {
  console.log("Inside getJobsDispatcher action, " + JSON.stringify(payload));
  return {
    type: GETJOBS,
    payload
  };
};

//dispatcher for adding job
const addJobDispatcher = payload => {
  console.log("Inside addJobDispatcher action, " + JSON.stringify(payload));
  return {
    type: ADDJOB,
    payload
  };
};

//dispatcher for adding job
export const refreshStatus = payload => {
  console.log("Inside refreshStatus action");
  return {
    type: "REFRESHSTATUS",
    payload
  };
};

//thunk for getting profile
export const getProfile = payload => {
  return dispatch => {
    console.log("Inside getProfile thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host + "/company/profile/" + localStorage.getItem("SID"),
        config
      )
      .then(response => {
        if (response.status === 200) {
          localStorage.setItem("company_name", response.data.NAME);
          console.log(
            JSON.stringify(response.data) + " : Axios Profile response"
          );
          var path =
            `${configPath.base}/images/` + `${response.data.PROFILE_PIC}`;
          dispatch(
            getProfileDispatcher({
              profilePicture: path,
              profileObj: response.data
            })
          );
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk for updating profile
export const updateProfile = payload => {
  console.log("Inside updateProfile", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host + "/company/profile/" + localStorage.getItem("SID"),
        payload,
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(updateProfileDispatcher({ profileObj: res.data }));
          localStorage.setItem("company_name", res.data.NAME);
        }
      });
  };
};

//thunk for uploading profile picture
export const uploadProfilePic = payload => {
  console.log("Inside uploadProfilePic", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };

    try {
      console.log("In try block");
      axios
        .post(
          configPath.api_host +
            `/company/profile/picture/${localStorage.getItem("SID")}`,
          payload,
          config
        )
        .then(res => {
          console.log(res.data);
          var path = `${configPath.base}/images/` + `${res.data}`;
          console.log(path);
          dispatch(uploadProfilePicDispatcher({ profilePicture: path }));
        })
        .catch(err => {
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  };
};

//thunk for getting company jobs
export const getJobs = payload => {
  return dispatch => {
    console.log("Inside getJobs thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/job_list/${payload.category_filter}/${localStorage.getItem(
            "company_name"
          )}/${payload.job_filter}/${payload.location_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          dispatch(getJobsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk for adding job
export const addJob = payload => {
  console.log("Inside addJob", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          `/company/profile/add_job/${localStorage.getItem(
            "SID"
          )}?page=${payload.page}&limit=${payload.limit}`,
        payload,
        config
      )
      .then(response => {
        console.log(JSON.stringify(response.data));
        dispatch(
          addJobDispatcher({
            ...response.data,
            application_status: "Job Successfully Added"
          })
        );
      })
      .catch(error => {
        console.log(error);
        dispatch(
          addJobDispatcher({
            application_status: "Failed to Add Job"
          })
        );
      });
  };
};
