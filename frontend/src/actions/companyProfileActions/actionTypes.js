//Profile Pic Actions
export const GETPROFILE = "GETPROFILE";
export const UPDATEPROFILE = "UPDATEPROFILE";
export const UPLOADPROFILEPIC = "UPLOADPROFILEPIC";

//Company Jobs
export const GETJOBS = "GETJOBS";
export const ADDJOB = "ADDJOB";
