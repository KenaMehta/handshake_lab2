//importing reduc action types
import { GETMESSAGES, GETSELECTEDMESSAGE, SENDMESSAGE } from "./actionTypes";

import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for sending message
const sendMessageDispatcher = payload => {
  console.log(
    "Inside sendMessageDispatcher action, " + JSON.stringify(payload)
  );
  return {
    type: SENDMESSAGE,
    payload
  };
};

//dispatcher for getting messages
const getMessagesDispatcher = payload => {
  console.log(
    "Inside getMessagesDispatcher action, " + JSON.stringify(payload)
  );
  return {
    type: GETMESSAGES,
    payload
  };
};

//dispatcher for updating profile pic
const getSelectedMessageDispatcher = payload => {
  console.log(
    "Inside getSelectedMessageDispatcher action, " + JSON.stringify(payload)
  );
  return {
    type: GETSELECTEDMESSAGE,
    payload
  };
};

//dispatcher for adding job
export const refreshStatus = payload => {
  console.log("Inside refreshStatus action");
  return {
    type: "REFRESHSTATUS",
    payload
  };
};

//thunk to send message from company to student
export const sendMessage = payload => {
  return dispatch => {
    console.log("Inside sendMessage thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .post(
        configPath.api_host +
          "/company/message/sendMessage/" +
          payload.sid +
          "/" +
          localStorage.getItem("SID"),
        payload,
        config
      )
      .then(response => {
        if (response.status === 200)
          dispatch(
            sendMessageDispatcher({
              ...response.data,
              message_status: "Message Sent!"
            })
          );
        else
          dispatch(
            sendMessageDispatcher({ message_status: "Sending Failed!" })
          );
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk for getting messages
export const getMessages = payload => {
  console.log("Inside getMessages", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host + "/company/message/" + localStorage.getItem("SID"),
        config
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          dispatch(getMessagesDispatcher(res.data));
        }
      });
  };
};

//thunk for getting selected message thread
export const getSelectedMessage = payload => {
  console.log("Inside getSelectedMessage", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };

    try {
      console.log("In try block");
      axios
        .get(
          configPath.api_host +
            `/company/message/selectedMessage/${payload.sid}/${localStorage.getItem(
              "SID"
            )}`,
          config
        )
        .then(res => {
          if (res.status == 200)
            dispatch(getSelectedMessageDispatcher(res.data));
        })
        .catch(err => {
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  };
};
