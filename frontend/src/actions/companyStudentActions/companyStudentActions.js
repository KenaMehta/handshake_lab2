//importing redux action types
import { GETSTUDENTS, UPDATESTATUS, GETEVENTSTUDENT } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher for getting profile details
const getStudentsDispatcher = payload => {
  console.log("Inside getStudentsDispatcher action, " + payload);
  return {
    type: GETSTUDENTS,
    payload
  };
};

//dispatcher for updating status
const updateStatusDispatcher = payload => {
  console.log("Inside updateStatusDispatcher action, " + payload);
  return {
    type: UPDATESTATUS,
    payload
  };
};

//dispatcher for refreshing status
export const refreshStatus = payload => {
  console.log("Inside updateStatusDispatcher action");
  return {
    type: "REFRESHSTATUS",
    payload
  };
};

//dispatcher for getting profile details
const getEventStudentsDispatcher = payload => {
  console.log("Inside getEventStudentsDispatcher action, " + payload);
  return {
    type: GETEVENTSTUDENT,
    payload
  };
};

//thunk for getting students of the job
export const getStudents = payload => {
  return dispatch => {
    console.log("Inside getProfile thunk");
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/company/job/students/${localStorage.getItem("Selected_Job")}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          dispatch(getStudentsDispatcher({ studentArr: response.data }));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};

//thunk for updating student's status
export const updateStatus = payload => {
  console.log("Inside updateStatus", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .put(configPath.api_host + `/company/job/students`, payload, config)
      .then(response => {
        if (response.status === 200) {
          console.log("status_line updated");
          dispatch(updateStatusDispatcher({ status_line: "Updated" }));
        } else
          dispatch(updateStatusDispatcher({ status_line: "Failed to update" }));
      })
      .catch(error =>
        dispatch(updateStatusDispatcher({ status_line: "Failed to update" }))
      );
  };
};

//thunk for getting event's students
export const getEventStudents = payload => {
  console.log("Inside getEventStudents", payload);

  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/company/event/students/${localStorage.getItem("Selected_Event")}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          dispatch(getEventStudentsDispatcher({ studentArr: response.data }));
        }
      })
      .catch(err => console.log(err + " : Error in getting Profile details"));
  };
};
