import { GETAPPLICATIONS } from "./actionTypes";
import configPath from "./../../configApp";
import axios from "axios";

//dispatcher to get student's applications
const getApplicationsDispatcher = payload => {
  console.log("Inside getApplicationsDispatcher action, " + payload);
  return {
    type: GETAPPLICATIONS,
    payload
  };
};

//thunk to get student's applications
export const getApplications = payload => {
  console.log("Inside getApplications thunk", payload);
  return dispatch => {
    //set the with credentials to true
    axios.defaults.withCredentials = true;
    console.log(localStorage.getItem("SID"));
    let config = {
      headers: {
        Authorization: `${window.localStorage.getItem("SIDToken")}`
      }
    };
    axios
      .get(
        configPath.api_host +
          `/student/applications/${localStorage.getItem(
            "SID"
          )}/${payload.pending_filter}/${payload.reviewed_filter}/${payload.declined_filter}?page=${payload.page}&limit=${payload.limit}`,
        config
      )
      .then(response => {
        if (response.status === 200) {
          dispatch(getApplicationsDispatcher(response.data));
        }
      })
      .catch(err => console.log(err + " : Error in getting Job details"));
  };
};
