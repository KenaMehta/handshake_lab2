var connection = new require("./kafka/Connection");

//student services
const accountService = require("./services/account");
const studentService = require("./services/student");
const studentActivityService = require("./services/studentActivities");

//company services
const companyService = require("./services/company");
const companyActivityService = require("./services/companyActivities");

const Mongoose = require("mongoose");
var options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  reconnectInterval: 500,
  poolSize: 50,
  bufferMaxEntries: 0
};

Mongoose.connect(
  "mongodb+srv://kenamehta:kenamehta@handshake-kena-8xufz.mongodb.net/handshake?retryWrites=true&w=majority",
  options
  // {
  //   useNewUrlParser: true,
  //   useUnifiedTopology: true
  // }
)
  .then(() => console.log("Connected to MongoDB"))
  .catch(err => {
    console.log("Failed to connect to MongoDB");
    console.log(err);
  });

function handleTopicRequest(topic_name, fname) {
  //var topic_name = 'root_topic';
  var consumer = connection.getConsumer(topic_name);
  var producer = connection.getProducer();
  console.log("server is running ");
  consumer.on("message", function(message) {
    console.log("message received for " + topic_name + " ", fname);
    console.log(JSON.stringify(message.value));
    var data = JSON.parse(message.value);

    fname.handle_request(data.data, function(err, res) {
      console.log("after handle" + err);
      let response = "";
      if (err) {
        response = err;
      } else {
        response = res;
      }
      var payloads = [
        {
          topic: data.replyTo,
          messages: JSON.stringify({
            correlationId: data.correlationId,
            data: response
          }),
          partition: 0
        }
      ];
      producer.send(payloads, function(err, data) {
        console.log(data);
      });
      return;
    });
  });
}

const response = (data, res, err, producer) => {
  var payloads = [
    {
      topic: data.replyTo,
      messages: JSON.stringify({
        correlationId: data.correlationId,
        data: res,
        err: err
      }),
      partition: 0
    }
  ];
  producer.send(payloads, function(err, data) {
    if (err) {
      console.log("Error when producer sending data", err);
    } else {
      console.log(data);
    }
  });
  return;
};

// Add your TOPICs here
//first argument is topic name
//second argument is a function that will handle this topic request
handleTopicRequest("account", accountService);
handleTopicRequest("student", studentService);
handleTopicRequest("studentActivities", studentActivityService);
handleTopicRequest("company", companyService);
handleTopicRequest("companyActivities", companyActivityService);
