const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const uniqueValidator = require("mongoose-unique-validator");
const { Student } = require("./studentModels");
const { Company } = require("./companyModels");
const mongoosePaginate = require("mongoose-paginate");

const messageSchema = new Mongoose.Schema(
  {
    Body: { type: String, required: true },
    From: {
      type: Mongoose.Schema.Types.ObjectId,
      required: true,
      refPath: "FromModel"
    },
    To: {
      type: Mongoose.Schema.Types.ObjectId,
      required: true,
      refPath: "ToModel"
    },
    ThreadName: String, //cid-sid
    FromModel: {
      type: String,
      required: true,
      enum: ["Student", "Company"]
    },
    ToModel: {
      type: String,
      required: true,
      enum: ["Student", "Company"]
    }
  },
  { timestamps: true }
);
const Messages = Mongoose.model("Messages", messageSchema);

//exporting Company's models
module.exports = {
  Messages
};
