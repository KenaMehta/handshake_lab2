"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let updateStudentBasicDetails = async (msg, callback) => {
  console.log(msg);
  Student.findOne({
    _id: msg.params.sid
  }).then(async result => {
    console.log(result + "res found");
    let DOB = msg.DOB ? msg.DOB : result.DOB;
    let CITY = msg.CITY ? msg.CITY : result.CITY;
    let STATE = msg.STATE ? msg.STATE : result.STATE;
    let COUNTRY = msg.COUNTRY ? msg.COUNTRY : result.COUNTRY;
    let PHONE = msg.PHONE ? msg.PHONE : result.PHONE;

    try {
      console.log(DOB + " major found");
      console.log(CITY + " yop found");
      console.log(STATE + " gpa found");

      let query = {
        _id: msg.params.sid
      };
      let update = {
        DOB: DOB,
        CITY: CITY,
        STATE: STATE,
        COUNTRY: COUNTRY,
        PHONE: PHONE
      };
      let options = { new: true, useFindAndModify: false };
      let basicDetailsUpdate = await Student.findOneAndUpdate(
        query,
        update,
        options
      );
      if (basicDetailsUpdate)
        return callback(null, {
          status: 200,
          dob: basicDetailsUpdate.DOB || "",
          city: basicDetailsUpdate.CITY || "",
          state: basicDetailsUpdate.STATE || "",
          country: basicDetailsUpdate.COUNTRY || "",
          phone: basicDetailsUpdate.PHONE || "",
          email: basicDetailsUpdate.EMAIL
        });
      else return callback({ status: 500 }, null);
    } catch (err) {
      return callback({ status: 500 }, null);
    }
  });
};

exports.updateStudentBasicDetails = updateStudentBasicDetails;
