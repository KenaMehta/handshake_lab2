"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let deleteStudentSkills = async (msg, callback) => {
  const Stu = await Student.findOne({ _id: msg.params.sid });
  var SKILL = Stu.SKILL;
  console.log("SKILL: " + SKILL[0].SKILL);
  SKILL = SKILL.filter(sk => {
    console.log(sk.SKILL);
    return sk.SKILL !== msg.data.deleteSkill;
  });
  let query = {
    _id: msg.params.sid
  };
  let update = {
    SKILL: SKILL
  };
  let options = { new: true, useFindAndModify: false };
  let skillDelete = await Student.findOneAndUpdate(query, update, options);

  const afterDelete = await Student.findOne({
    _id: msg.params.sid
  });
  if (afterDelete)
    return callback(null, { status: 200, skill: afterDelete.SKILL });
  else return callback({ status: 500 }, null);
};

exports.deleteStudentSkills = deleteStudentSkills;
