"use strict";
const { uploadStudentPic } = require("./uploadStudentPic");
const { getStudentProfile } = require("./getStudentProfile");
const { updateStudentProfile } = require("./updateStudentProfile");
const { getStudentJourney } = require("./updateStudentJourney");
const { updateStudentJourney } = require("./updateStudentJourney");
const { deleteStudentEducation } = require("./deleteStudentEducation");
const { updateStudentEducation } = require("./updateStudentEducation");
const { deleteStudentExperience } = require("./deleteStudentExperience");
const { updateStudentExperience } = require("./updateStudentExperience");
const { updateStudentBasicDetails } = require("./updateStudentBasicDetails");
const { addStudentSkills } = require("./addStudentSkills");
const { deleteStudentSkills } = require("./deleteStudentSkills");

let handle_request = (msg, callback) => {
  console.log("in student switch");
  switch (msg.route) {
    case "uploadStudentPic":
      uploadStudentPic(msg, callback);
      break;
    case "getStudentProfile":
      getStudentProfile(msg, callback);
      break;
    case "updateStudentProfile":
      updateStudentProfile(msg, callback);
      break;
    case "getStudentJourney":
      getStudentJourney(msg, callback);
      break;
    case "updateStudentJourney":
      updateStudentJourney(msg, callback);
      break;
    case "deleteStudentEducation":
      deleteStudentEducation(msg, callback);
      break;
    case "updateStudentEducation":
      updateStudentEducation(msg, callback);
      break;
    case "deleteStudentExperience":
      deleteStudentExperience(msg, callback);
      break;
    case "updateStudentExperience":
      updateStudentExperience(msg, callback);
      break;
    case "updateStudentBasicDetails":
      updateStudentBasicDetails(msg, callback);
      break;
    case "addStudentSkills":
      addStudentSkills(msg, callback);
      break;
    case "deleteStudentSkills":
      deleteStudentSkills(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;
