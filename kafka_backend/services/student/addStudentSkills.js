"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let addStudentSkills = async (msg, callback) => {
  try {
    const Stu = await Student.findOne({ _id: msg.params.sid });
    const SKILL = Stu ? Stu.SKILL : [];
    SKILL.push({
      SKILL: msg.SKILL
    });
    console.log(SKILL);
    const updated = await Stu.save();
    const afterAdd = await Student.findOne({
      _id: msg.params.sid
    });
    if (afterAdd) return callback(null, { status: 200, skill: afterAdd.SKILL });
    else return callback({ status: 500 }, null);
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err }, null);
  }
};

exports.addStudentSkills = addStudentSkills;
