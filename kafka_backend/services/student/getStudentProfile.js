"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let getStudentProfile = async (msg, callback) => {
  try {
    console.log("in student profile");
    Student.findOne({ _id: msg.params.sid }).then(async result => {
      if (result) {
        console.log(result);
        var COLLEGE_NAME = result.COLLEGE_NAME;
        var EducationArr = result.EDUCATION;
        var ExperienceArr = result.EXPERIENCE;
        var SkillArr = result.SKILL;
        var Edu = EducationArr.filter(ed => ed.COLLEGE_NAME == COLLEGE_NAME);
        if (Edu.length != 0) {
          console.log(Edu);
          var MAJOR = Edu[0].MAJOR;

          return callback(null, {
            status: 200,
            id: msg.params.sid,
            name: result.NAME,
            school: result.COLLEGE_NAME,
            DEGREE: Edu[0].DEGREE,
            MAJOR: Edu[0].MAJOR,
            GPA: Edu[0].CURRENT_GPA,
            dob: result.DOB || "",
            city: result.CITY || "",
            state: result.STATE || "",
            country: result.COUNTRY || "",
            phone: result.PHONE || "",
            email: result.EMAIL,
            Education: EducationArr,
            Experience: ExperienceArr,
            Skill: SkillArr,
            PHOTO: result.PHOTO ? result.PHOTO : ""
          });
        } else {
          return callback(null, {
            status: 200,
            id: msg.params.sid,
            name: result.NAME,
            school: result.COLLEGE_NAME,
            DEGREE: "",
            MAJOR: "",
            GPA: "",
            dob: result.DOB || "",
            city: result.CITY || "",
            state: result.STATE || "",
            country: result.COUNTRY || "",
            phone: result.PHONE || "",
            email: result.EMAIL,
            Education: EducationArr,
            Experience: ExperienceArr,
            Skill: SkillArr,
            PHOTO: result.PHOTO ? result.PHOTO : "null"
          });
        }
      } else {
        return callback(
          {
            status: 403,
            res: "Unauthenticated Student"
          },
          null
        );
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    return callback(
      {
        status: 500,
        res: "Unable to fetch profile from Student"
      },
      null
    );
  }
};

exports.getStudentProfile = getStudentProfile;
