"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let deleteStudentEducation = async (msg, callback) => {
  console.log(msg.data.deleteSchool);

  const Stu = await Student.findOne({ _id: msg.params.sid });
  var EDUCATION = Stu.EDUCATION;
  console.log("EDUCATION: " + EDUCATION[0].COLLEGE_NAME);
  EDUCATION = EDUCATION.filter(edu => {
    console.log(edu.COLLEGE_NAME);
    return edu.COLLEGE_NAME !== msg.data.deleteSchool;
  });
  let query = {
    _id: msg.params.sid
  };
  let update = {
    EDUCATION: EDUCATION
  };
  let options = { new: true, useFindAndModify: false };
  let educationDelete = await Student.findOneAndUpdate(query, update, options);

  const afterDelete = await Student.findOne({
    _id: msg.params.sid
  });
  if (afterDelete)
    return callback(null, { status: 200, education: afterDelete.EDUCATION });
  else return callback({ status: 500 }, null);
};

exports.deleteStudentEducation = deleteStudentEducation;
