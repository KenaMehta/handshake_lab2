"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let uploadStudentPic = async (msg, callback) => {
  if (msg.file) {
    let query = { _id: msg.params.sid };
    let update = { PHOTO: `${msg.params.sid}-${msg.file.originalname}` };
    let options = { new: true, useFindAndModify: false };
    let student_pic = await Student.findOneAndUpdate(query, update, options);
    student_pic
      .save()
      .then(response => {
        return callback(null, {
          status: 200,
          data: `${msg.params.sid}-${msg.file.originalname}`,
          writeHead: {
            "Content-Type": "application/json"
          }
        });
      })
      .catch(err => {
        return callback({ status: 403 }, null);
      });
  }
};

exports.uploadStudentPic = uploadStudentPic;
