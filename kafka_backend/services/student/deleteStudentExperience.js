"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let deleteStudentExperience = async (msg, callback) => {
  console.log(msg.data.deleteExperience);

  const Stu = await Student.findOne({ _id: msg.params.sid });
  var EXPERIENCE = Stu.EXPERIENCE;
  console.log("EXPERIENCE: " + EXPERIENCE[0].COMPANY_NAME);
  EXPERIENCE = EXPERIENCE.filter(ex => {
    console.log(ex.COMPANY_NAME);
    return ex.COMPANY_NAME !== msg.data.deleteExperience;
  });
  let query = {
    _id: msg.params.sid
  };
  let update = {
    EXPERIENCE: EXPERIENCE
  };
  let options = { new: true, useFindAndModify: false };
  let experienceDelete = await Student.findOneAndUpdate(query, update, options);

  const afterDelete = await Student.findOne({
    _id: msg.params.sid
  });
  if (afterDelete)
    return callback(null, { status: 200, experience: afterDelete.EXPERIENCE });
  else return callback({ status: 500 }, null);
};

exports.deleteStudentExperience = deleteStudentExperience;
