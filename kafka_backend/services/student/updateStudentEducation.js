"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let updateStudentEducation = async (msg, callback) => {
  console.log(msg);
  Student.findOne({
    _id: msg.params.sid,
    "EDUCATION.COLLEGE_NAME": msg.COLLEGE_NAME
  }).then(async result => {
    console.log(result + "res found");
    if (result) {
      const Stu = await Student.findOne({ _id: msg.params.sid });
      var EDUCATION = Stu.EDUCATION;
      console.log("EDUCATION: " + EDUCATION[0].COLLEGE_NAME);
      var EDUCATION_other = EDUCATION.filter(edu => {
        console.log(edu.COLLEGE_NAME);
        return edu.COLLEGE_NAME != msg.COLLEGE_NAME;
      });
      var EDUCATION_update = EDUCATION.filter(edu => {
        console.log(edu.COLLEGE_NAME);
        return edu.COLLEGE_NAME == msg.COLLEGE_NAME;
      });
      let degree = msg.DEGREE ? msg.DEGREE : EDUCATION_update[0].DEGREE;
      let major = msg.MAJOR ? msg.MAJOR : EDUCATION_update[0].MAJOR;
      let yop = msg.YEAR_OF_PASSING
        ? msg.YEAR_OF_PASSING
        : EDUCATION_update[0].YEAR_OF_PASSING;
      let gpa = msg.CURRENT_GPA
        ? msg.CURRENT_GPA
        : EDUCATION_update[0].CURRENT_GPA;
      try {
        console.log(degree + " major found");
        console.log(yop + " yop found");
        console.log(gpa + " gpa found");
        EDUCATION_other.push({
          COLLEGE_NAME: msg.COLLEGE_NAME,
          DEGREE: degree,
          MAJOR: major,
          YEAR_OF_PASSING: yop,
          CURRENT_GPA: gpa
        });
        console.log(EDUCATION_other);
        let query = {
          _id: msg.params.sid
        };
        let update = {
          EDUCATION: EDUCATION_other
        };
        let options = { new: true, useFindAndModify: false };
        let educationAdd = await Student.findOneAndUpdate(
          query,
          update,
          options
        );
        if (educationAdd)
          return callback(null, {
            status: 200,
            education: educationAdd.EDUCATION
          });
        else return callback({ status: 403 }, null);
      } catch (err) {
        console.log(err);
        return callback({ status: 500 }, null);
      }
    } else {
      console.log("in else");
      const Stu = await Student.findOne({ _id: msg.params.sid });
      const EDUCATION = Stu.EDUCATION;

      EDUCATION.push({
        COLLEGE_NAME: msg.COLLEGE_NAME,
        DEGREE: msg.DEGREE,
        MAJOR: msg.MAJOR,
        YEAR_OF_PASSING: msg.YEAR_OF_PASSING,
        CURRENT_GPA: msg.CURRENT_GPA
      });
      console.log(EDUCATION);
      const updated = await Stu.save();
      const afterAdd = await Student.findOne({
        _id: msg.params.sid
      });
      if (afterAdd)
        return callback(null, {
          status: 200,
          education: afterAdd.EDUCATION
        });
      else return callback({ status: 403 }, null);
    }
  });
};

exports.updateStudentEducation = updateStudentEducation;
