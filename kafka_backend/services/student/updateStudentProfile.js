"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let updateStudentProfile = async (msg, callback) => {
  try {
    let query = { _id: msg.params.sid };
    let update = { NAME: msg.updateTextValue };
    let options = { new: true, useFindAndModify: false };
    let student_name = await Student.findOneAndUpdate(
      query,
      update,
      options
    ).then(result => {
      return callback(null, { status: 200, result });
    });
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err }, null);
  }
};

exports.updateStudentProfile = updateStudentProfile;
