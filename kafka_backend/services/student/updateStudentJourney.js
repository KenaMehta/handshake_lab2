"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let getStudentJourney = async (msg, callback) => {
  try {
    Student.findOne({
      _id: msg.params.sid
    })
      .then(result => {
        if (result) {
          return callback(null, { status: 200, journey: result.CAREER_OBJ });
        } else {
          return callback({ status: 403, msg: "Unauthenticated User" }, null);
        }
      })
      .catch(err => {
        console.log("Error caught when hitting Student_Profile");
        return callback(
          {
            status: 500,
            res: "Unable to fetch data from Student_Profile"
          },
          null
        );
      });
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err }, null);
  }
};

let updateStudentJourney = async (msg, callback) => {
  try {
    console.log("msg: " + JSON.stringify(msg));
    let query = { _id: msg.params.sid };
    let update = { CAREER_OBJ: msg.journey };
    let options = { new: true, useFindAndModify: false };
    let student_pic = await Student.findOneAndUpdate(
      query,
      update,
      options
    ).then(result => {
      if (result) {
        return callback(null, { status: 200, result });
      } else {
        return callback(
          {
            status: 500,
            res: "Unable to fetch data from Student_Profile"
          },
          null
        );
      }
    });
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err }, null);
  }
};

exports.getStudentJourney = getStudentJourney;
exports.updateStudentJourney = updateStudentJourney;
