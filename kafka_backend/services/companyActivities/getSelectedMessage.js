"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let getSelectedMessage = async (msg, callback) => {
  try {
    var { sid, cid } = msg.params;
    const MessageThreadArr = await Messages.find({
      ThreadName: cid + "-" + sid
    });
    const student = await Student.findById(sid);
    const studentData = {
      NAME: student.NAME,
      INFO: student.COLLEGE_NAME,
      PHOTO: student.PHOTO ? student.PHOTO : "null"
    };
    return callback(null, { status: 200, MessageThreadArr, studentData });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.getSelectedMessage = getSelectedMessage;
