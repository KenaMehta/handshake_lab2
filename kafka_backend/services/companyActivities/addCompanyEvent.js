"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../../model/companyModels");

let addCompanyEvent = async (msg, callback) => {
  try {
    var today = new Date();
    console.log("add event  " + JSON.stringify(msg));
    const addEvent = await Company_Events({
      CID: msg.params.cid,
      E_NAME: msg.E_NAME,
      E_DESC: msg.E_DESC,
      TIME: msg.TIME,
      DATE: msg.DATE,
      LOCATION: msg.LOCATION,
      ELIGIBILITY: msg.ELIGIBILITY,
      POST_FLAG: "Y",
      Created_At:
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate()
    });
    addEvent
      .save()
      .then(async result => {
        var { limit, page } = msg.query;
        let options = {
          limit: parseInt(limit, 10) || 10,
          page: parseInt(page, 10) || 1,
          sort: { DATE: 1 }
        };
        const eventPaginateObject = await Company_Events.paginate(
          { CID: msg.params.cid },
          options
        );
        console.log(eventPaginateObject);
        return callback(null, {
          status: 200,
          eventArr: eventPaginateObject.docs,
          total: eventPaginateObject.total,
          pages: eventPaginateObject.pages
        });
      })
      .catch(err => {
        console.log(err);
        return callback({ status: 500 }, null);
      });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.addCompanyEvent = addCompanyEvent;
