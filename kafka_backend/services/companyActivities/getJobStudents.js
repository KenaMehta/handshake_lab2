"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const { Student_Job } = require("../../model/companyModels");

let getJobStudents = async (msg, callback) => {
  console.log("...." + msg.params.jid + "....");
  try {
    const studentList = await Student_Job.find({
      JID: msg.params.jid
    }).populate("SID");
    return callback(null, { status: 200, studentList });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.getJobStudents = getJobStudents;
