"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const { Student_Event } = require("../../model/companyModels");

let getEventStudents = async (msg, callback) => {
  console.log("...." + msg.params.event_code + "....");
  try {
    const studentList = await Student_Event.find({
      EID: msg.params.eid
    }).populate("SID");
    return callback(null, { status: 200, studentList });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.getEventStudents = getEventStudents;
