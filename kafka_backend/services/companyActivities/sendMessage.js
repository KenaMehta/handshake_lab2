"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let sendMessage = async (msg, callback) => {
  try {
    var { cid, sid } = msg.params;
    const messageNew = await Messages.create({
      Body: msg.BODY,
      From: cid,
      To: sid,
      ThreadName: cid + "-" + sid,
      FromModel: "Company",
      ToModel: "Student"
    });
    if (messageNew) {
      const MessageThreadArr = await Messages.find({
        ThreadName: cid + "-" + sid
      });
      return callback(null, {
        status: 200,
        MessageThreadArr,
        message_status: "Message Sent"
      });
    } else
      return callback(
        { status: 403, message_status: "Failure to send Message" },
        null
      );
  } catch (err) {
    console.log(err);
    return callback(
      { status: 500, message_status: "Failure to send Message" },
      null
    );
  }
};

exports.sendMessage = sendMessage;
