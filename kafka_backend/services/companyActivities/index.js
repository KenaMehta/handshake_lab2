"use strict";
//events
const { getCompanyEvents } = require("./getCompanyEvents");
const { addCompanyEvent } = require("./addCompanyEvent");
//job's students
const { getJobStudents } = require("./getJobStudents");
const { updateStudentStatus } = require("./updateStudentStatus");
//event's students
const { getEventStudents } = require("./getEventStudents");
//messages
const { getMessages } = require("./getMessages");
const { getSelectedMessage } = require("./getSelectedMessage");
const { sendMessage } = require("./sendMessage");

let handle_request = (msg, callback) => {
  console.log("in message switch");
  switch (msg.route) {
    //events
    case "getCompanyEvents":
      getCompanyEvents(msg, callback);
      break;
    case "addCompanyEvent":
      addCompanyEvent(msg, callback);
      break;
    //job's students
    case "getJobStudents":
      getJobStudents(msg, callback);
      break;
    case "updateStudentStatus":
      updateStudentStatus(msg, callback);
      break;
    //event's students
    case "getEventStudents":
      getEventStudents(msg, callback);
      break;
    //messages
    case "getMessages":
      getMessages(msg, callback);
      break;
    case "getSelectedMessage":
      getSelectedMessage(msg, callback);
      break;
    case "sendMessage":
      sendMessage(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;
