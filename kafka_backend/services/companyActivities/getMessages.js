"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let getMessages = async (msg, callback) => {
  try {
    const StudentArr = await Messages.aggregate([
      {
        $match: {
          ThreadName: { $regex: new RegExp(msg.params.cid) }
        }
      },
      {
        $group: {
          _id: { ThreadName: "$ThreadName" },
          message: { $last: "$Body" },
          maxDate: { $max: "$createdAt" }
        }
      },
      {
        $sort: {
          maxDate: -1
        }
      }
    ]);
    var StudentMessagesArr = [];
    const request = await StudentArr.map(async stu => {
      var student = await Student.findById(stu._id.ThreadName.split("-")[1]);
      var data = {
        ID: student._id,
        NAME: student.NAME,
        INFO: student.COLLEGE_NAME,
        PHOTO: student.PHOTO ? student.PHOTO : "null",
        LAST_MESSAGE: stu.message,
        LAST_DATE: stu.maxDate
      };
      StudentMessagesArr.push(data);
    });
    function compare(a, b) {
      let comparison = 0;
      if (a.LAST_DATE > b.LAST_DATE) {
        comparison = 1;
      } else if (a.LAST_DATE < b.LAST_DATE) {
        comparison = -1;
      }
      return comparison;
    }
    Promise.all(request).then(() => {
      return callback(null, {
        status: 200,
        StudentMessagesArr: StudentMessagesArr.sort(compare)
      });
    });
  } catch (err) {
    return callback({ status: 500, err }, null);
  }
};
exports.getMessages = getMessages;
