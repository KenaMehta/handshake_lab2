"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const { Student_Job } = require("../../model/companyModels");

let updateStudentStatus = async (msg, callback) => {
  let query = { SID: msg.sid, JID: msg.job_id };
  let update = { STATUS: msg.status };
  let options = { new: true, useFindAndModify: false };
  let company_pic = await Student_Job.findOneAndUpdate(query, update, options);
  company_pic
    .save()
    .then(response => {
      return callback(null, { status: 200 });
    })
    .catch(err => {
      console.log(error);
      return callback({ status: 500 }, null);
    });
};

exports.updateStudentStatus = updateStudentStatus;
