"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const { Company_Events } = require("../../model/companyModels");

let getCompanyEvents = async (msg, callback) => {
  console.log("Inside company events list");
  console.log(JSON.stringify(msg.params));
  var whereCondition = { POST_FLAG: "Y", CID: msg.params.cid };
  if (msg.params.event_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      E_NAME: { $regex: new RegExp(msg.params.event_filter, "i") }
    };
  }
  console.log(whereCondition);
  var { limit, page } = msg.query;
  let options = {
    limit: parseInt(limit, 10) || 10,
    page: parseInt(page, 10) || 1,
    sort: { DATE: 1 }
  };
  const eventPaginateObject = await Company_Events.paginate(
    whereCondition,
    options
  );
  console.log(eventPaginateObject);
  return callback(null, {
    status: 200,
    eventArr: eventPaginateObject.docs,
    total: eventPaginateObject.total,
    pages: eventPaginateObject.pages
  });
};

exports.getCompanyEvents = getCompanyEvents;
