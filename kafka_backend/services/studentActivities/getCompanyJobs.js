"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Jobs, Student_Job } = require("../../model/companyModels");

let getCompanyJobs = async (msg, callback) => {
  console.log("Inside company jobs list");
  console.log(JSON.stringify(msg.params));
  var whereCondition = { POST_FLAG: "Y" };
  if (msg.params.category_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      JOB_CATEGORY: msg.params.category_filter
    };
  }
  if (msg.params.company_filter !== "none") {
    console.log("inside if clause for company_fiiilter");
    whereCondition = {
      ...whereCondition,
      C_NAME: { $regex: new RegExp(msg.params.company_filter, "i") }
    };
  }
  if (msg.params.job_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      JOB_TITLE: { $regex: new RegExp(msg.params.job_filter, "i") }
    };
  }
  if (msg.params.location_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      CITY: { $regex: new RegExp(msg.params.location_filter, "i") }
    };
  }
  var sortOn = msg.query.sortOn;
  var sortOrder = msg.query.sortOrder;
  console.log("sortOrder:" + sortOrder);
  console.log(whereCondition);
  try {
    var { limit, page } = msg.query;
    let options = {
      populate: "CID",
      limit: parseInt(limit, 10) || 10,
      page: parseInt(page, 10) || 1,
      sort:
        sortOn == "LOCATION"
          ? { CITY: sortOrder == "1" ? 1 : -1 }
          : sortOn == "CreatedAt"
            ? { CreatedAt: sortOrder == "1" ? 1 : -1 }
            : { APP_DEADLINE: sortOrder == "1" ? 1 : -1 }
    };
    console.log("options:  " + JSON.stringify(options));
    const fullData = await Company_Jobs.paginate(whereCondition, options);
    return callback(null, {
      status: 200,
      jobArr: fullData.docs,
      jobObj: fullData.docs[0],
      total: fullData.total,
      pages: fullData.pages
    });
  } catch (err) {
    console.log("Error in fetching job list" + err);
    return callback({ status: 500 }, null);
  }
};

exports.getCompanyJobs = getCompanyJobs;
