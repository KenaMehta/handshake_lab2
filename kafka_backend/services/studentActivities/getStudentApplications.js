"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Jobs, Student_Job } = require("../../model/companyModels");
const { Student } = require("../../model/studentModels");

let getStudentApplications = async (msg, callback) => {
  try {
    var whereOrCondition = {};
    if (msg.params.pending_filter == "true") {
      console.log("in pending");
      whereOrCondition = { STATUS: "Pending" };
      console.log("pending where " + JSON.stringify(whereOrCondition));
    }
    if (msg.params.reviewed_filter == "true") {
      console.log("in reviewed");
      whereOrCondition = { STATUS: "Reviewed" };
      console.log("reviewed where " + JSON.stringify(whereOrCondition));
    }
    if (msg.params.declined_filter == "true") {
      console.log("in declined");
      whereOrCondition = { STATUS: "Declined" };
      console.log("declined where " + JSON.stringify(whereOrCondition));
    }
    console.log(Object.keys(whereOrCondition).length);
    var { limit, page } = msg.query;
    let options = {
      populate: "JID",
      limit: parseInt(limit, 10) || 10,
      page: parseInt(page, 10) || 1
    };
    if (Object.keys(whereOrCondition).length === 0) {
      console.log("in if");

      const jobPaginateObject = await Student_Job.paginate(
        {
          SID: msg.params.sid
        },
        options
      );
      return callback(null, {
        status: 200,
        jobListArr: jobPaginateObject.docs,
        total: jobPaginateObject.total,
        pages: jobPaginateObject.pages
      });
    } else {
      console.log("in else");
      const jobPaginateObject = await Student_Job.paginate(
        {
          SID: msg.params.sid,
          ...whereOrCondition
        },
        options
      );
      return callback(null, {
        status: 200,
        jobListArr: jobPaginateObject.docs,
        total: jobPaginateObject.total,
        pages: jobPaginateObject.pages
      });
    }
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.getStudentApplications = getStudentApplications;
