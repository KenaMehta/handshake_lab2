"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Jobs } = require("../../model/companyModels");

let getSelectedJob = async (msg, callback) => {
  try {
    const selectedCompany = await Company_Jobs.findOne({
      _id: msg.job_id
    }).populate("CID");
    return callback(null, { status: 200, company: selectedCompany });
  } catch (err) {
    return callback({ status: 500 }, null);
  }
};

exports.getSelectedJob = getSelectedJob;
