"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Events, Student_Event } = require("../../model/companyModels");
const { Student } = require("../../model/studentModels");

let registerEvent = async (msg, callback) => {
  console.log(msg);
  try {
    Student.findOne({ _id: msg.SID })
      .then(async result => {
        console.log(result);
        var COLLEGE_NAME = result.COLLEGE_NAME;
        var EducationArr = result.EDUCATION;
        var Edu = EducationArr.filter(ed => ed.COLLEGE_NAME == COLLEGE_NAME);
        console.log(Edu);
        var MAJOR = Edu[0].MAJOR;
        console.log(msg.ELIGIBILITY + "  " + MAJOR);
        if (
          msg.ELIGIBILITY.toLowerCase().includes("all") ||
          msg.ELIGIBILITY.toLowerCase().includes(MAJOR.toLowerCase())
        ) {
          console.log("in if");
          const addedStudent = await Student_Event.create({
            EID: msg.E_CODE,
            SID: msg.SID
          });

          if (addedStudent) {
            try {
              const eventListArr = await Student_Event.find({
                SID: msg.SID
              }).populate("EID");
              return callback(null, {
                status: 200,
                eventListArr,
                application_status: "Applied successfully"
              });
            } catch (err) {
              console.log("between:" + err);
              return callback({ status: 500 }, null);
            }
          }
        } else {
          console.log("in else");
          return callback(
            {
              status: 403,
              application_status: "  Not Eligible"
            },
            null
          );
        }
      })
      .catch(err => {
        console.log("in catch " + err);
        return callback(
          { status: 500, application_status: "  Already applied" },
          null
        );
      });
  } catch (err) {
    console.log("last err:" + err);
    return callback({ status: 500 }, null);
  }
};

exports.registerEvent = registerEvent;
