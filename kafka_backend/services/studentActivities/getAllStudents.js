"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Events, Student_Event } = require("../../model/companyModels");
const { Student } = require("../../model/studentModels");

let getAllStudents = async (msg, callback) => {
  try {
    let { page, limit } = msg.query;
    let options = {
      limit: parseInt(limit, 10) || 10,
      page: parseInt(page, 10) || 1,
      sort: {
        NAME: 1 //Sort by Name
      }
    };
    if (
      msg.params.student_filter == "none" &&
      msg.params.major_filter == "none" &&
      msg.params.skill_filter == "none"
    ) {
      const studentPaginateObject = await Student.paginate({}, options);
      var newStudentArr = [];
      studentPaginateObject.docs.map(student => {
        var EducationArr = student.EDUCATION;
        var Edu = EducationArr.filter(
          ed => ed.COLLEGE_NAME == student.COLLEGE_NAME
        );
        var SkillArr = student.SKILL;
        var skillList = "";
        SkillArr.map(skill => {
          skillList
            ? (skillList = skillList + "," + skill.SKILL)
            : (skillList = skill.SKILL);
        });
        console.log(Edu);
        newStudentArr.push({
          SID: student._id,
          name: student.NAME,
          profilePic: student.PHOTO,
          school: student.COLLEGE_NAME,
          MAJOR: Edu.length != 0 ? Edu[0].MAJOR : "",
          DEGREE: Edu.length != 0 ? Edu[0].DEGREE : "",
          YEAR_OF_PASSING: Edu.length != 0 ? Edu[0].YEAR_OF_PASSING : "",
          SkillList: skillList
        });
      });
      return callback(null, {
        status: 200,
        studentArr: newStudentArr,
        total: studentPaginateObject.total,
        pages: studentPaginateObject.pages
      });
    } else {
      const studentPaginateObject = await Student.paginate(
        {
          $or: [
            {
              NAME: {
                $regex: new RegExp(msg.params.student_filter, "i")
              }
            },
            {
              COLLEGE_NAME: {
                $regex: new RegExp(msg.params.student_filter, "i")
              }
            },
            {
              "EDUCATION.MAJOR": {
                $regex: new RegExp(msg.params.major_filter, "i")
              }
            },
            {
              "SKILL.SKILL": {
                $regex: new RegExp(msg.params.skill_filter, "i")
              }
            }
          ]
        },
        options
      );
      var newStudentArr = [];
      studentPaginateObject.docs.map(student => {
        var EducationArr = student.EDUCATION;
        var Edu = EducationArr.filter(
          ed => ed.COLLEGE_NAME == student.COLLEGE_NAME
        );
        var SkillArr = student.SKILL;
        var skillList = "";
        SkillArr.map(skill => {
          skillList
            ? (skillList = skillList + "," + skill.SKILL)
            : (skillList = skill.SKILL);
        });
        console.log(Edu);
        newStudentArr.push({
          SID: student._id,
          name: student.NAME,
          profilePic: student.PHOTO,
          school: student.COLLEGE_NAME,
          MAJOR: Edu.length != 0 ? Edu[0].MAJOR : "",
          DEGREE: Edu.length != 0 ? Edu[0].DEGREE : "",
          YEAR_OF_PASSING: Edu.length != 0 ? Edu[0].YEAR_OF_PASSING : "",
          SkillList: skillList
        });
      });
      return callback(null, {
        status: 200,
        studentArr: newStudentArr,
        total: studentPaginateObject.total,
        pages: studentPaginateObject.pages
      });
    }
  } catch (err) {
    console.log(err);
    return callback({ status: 500 });
  }
};

exports.getAllStudents = getAllStudents;
