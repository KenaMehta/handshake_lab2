"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Events, Student_Event } = require("../../model/companyModels");

let getCompanyEvents = async (msg, callback) => {
  console.log("Inside company events list");
  console.log(JSON.stringify(msg.params));
  var whereCondition = { POST_FLAG: "Y" };
  if (msg.params.event_filter !== "none") {
    whereCondition = {
      ...whereCondition,
      E_NAME: { $regex: new RegExp(msg.params.event_filter, "i") }
    };
  }
  console.log(whereCondition);
  try {
    var { limit, page } = msg.query;
    let options = {
      populate: "CID",
      limit: parseInt(limit, 10) || 10,
      page: parseInt(page, 10) || 1,
      sort: {
        DATE: 1 //Sort by Name
      }
    };
    const eventPaginateObject = await Company_Events.paginate(
      whereCondition,
      options
    );
    return callback(null, {
      status: 200,
      eventArr: eventPaginateObject.docs,
      eventObj: eventPaginateObject.docs[0],
      total: eventPaginateObject.total,
      pages: eventPaginateObject.pages
    });
  } catch (err) {
    console.log("Error in fetching event list" + err);
    return callback({ status: 500 }, null);
  }
};

exports.getCompanyEvents = getCompanyEvents;
