"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Events, Student_Event } = require("../../model/companyModels");

let getRegisteredEvents = async (msg, callback) => {
  console.log(msg);
  try {
    const eventListArr = await Student_Event.find({
      SID: msg.SID
    }).populate("EID");
    return callback(null, { status: 200, eventListArr });
  } catch (err) {
    return callback({ status: 500 }, null);
  }
};

exports.getRegisteredEvents = getRegisteredEvents;
