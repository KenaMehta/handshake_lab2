"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let getSelectedMessage = async (msg, callback) => {
  try {
    var { sid, id } = msg.params;
    var reciever = await Student.findById(id);
    var toModel = reciever ? "Student" : "Company";
    var threadName =
      toModel == "Student"
        ? id > sid ? id + "-" + sid : sid + "-" + id
        : id + "-" + sid;
    var UserData = {};
    const MessageArr = await Messages.find({
      ThreadName: threadName
    }).then(async res => {
      var student = await Student.findById(id);
      var company = await Company.findById(id);
      if (student) {
        var UserData = {
          NAME: student.NAME,
          INFO: student.COLLEGE_NAME,
          PHOTO: student.PHOTO ? student.PHOTO : "null"
        };
      } else {
        var UserData = {
          NAME: company.NAME,
          INFO: company.LOCATION,
          PHOTO: company.PROFILE_PIC ? company.PROFILE_PIC : "null"
        };
      }
      Promise.all([student, company]).then(() => {
        return callback(null, { status: 200, MessageThreadArr: res, UserData });
      });
    });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 }, null);
  }
};

exports.getSelectedMessage = getSelectedMessage;
