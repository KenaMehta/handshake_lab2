"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let getMessages = async (msg, callback) => {
  var sid = msg.params.sid;
  try {
    const UserArr = await Messages.aggregate([
      {
        $match: {
          ThreadName: { $regex: new RegExp(sid) }
        }
      },
      {
        $group: {
          _id: { ThreadName: "$ThreadName" },
          message: { $last: "$Body" },
          maxDate: { $max: "$createdAt" }
        }
      },
      {
        $sort: {
          maxDate: -1
        }
      }
    ]);
    var UserMessagesArr = [];
    const request = await UserArr.map(async user => {
      var idTemp = user._id.ThreadName.replace(sid, "");
      var id = idTemp.replace("-", "");
      var student = await Student.findById(id);
      var company = await Company.findById(id);
      if (student) {
        var data = {
          ID: student._id,
          NAME: student.NAME,
          INFO: student.COLLEGE_NAME,
          PHOTO: student.PHOTO ? student.PHOTO : "null",
          LAST_MESSAGE: user.message,
          LAST_DATE: user.maxDate
        };
        UserMessagesArr.push(data);
      } else {
        var data = {
          ID: company._id,
          NAME: company.NAME,
          INFO: company.LOCATION,
          PHOTO: company.PROFILE_PIC ? company.PROFILE_PIC : "null",
          LAST_MESSAGE: user.message,
          LAST_DATE: user.maxDate
        };
        UserMessagesArr.push(data);
      }
    });
    function compare(a, b) {
      let comparison = 0;
      if (a.LAST_DATE > b.LAST_DATE) {
        comparison = -1;
      } else if (a.LAST_DATE < b.LAST_DATE) {
        comparison = 1;
      }
      return comparison;
    }

    Promise.all(request).then(() => {
      return callback(null, {
        status: 200,
        UserMessagesArr: UserMessagesArr.sort(compare)
      });
    });
  } catch (err) {
    return callback({ status: 500, err }, null);
  }
};
exports.getMessages = getMessages;
