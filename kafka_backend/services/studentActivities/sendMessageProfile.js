//delete this file
"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const { Messages } = require("../../model/messages");
const Mongoose = require("mongoose");

let sendMessageProfile = async (msg, callback) => {
  var { sid1, sid2 } = msg.params;
  try {
    var Thread = sid1 > sid2 ? sid1 + "-" + sid2 : sid2 + "-" + sid1;
    const messageNew = await Messages.create({
      Body: msg.BODY,
      From: sid1,
      To: sid2,
      ThreadName: Thread,
      FromModel: "Student",
      ToModel: "Student"
    });
    if (messageNew)
      return callback(null, { status: 200, message_status: "Message Sent" });
    else
      return callback(
        { status: 403, message_status: "Failure to send Message" },
        null
      );
  } catch (err) {
    console.log(err);
    return callback(
      { status: 500, message_status: "Failure to send Message" },
      null
    );
  }
};

exports.sendMessageProfile = sendMessageProfile;
