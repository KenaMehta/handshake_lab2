"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Student_Job } = require("../../model/companyModels");

let applyJob = async (msg, callback) => {
  try {
    console.log(msg);
    console.log(msg.file);
    var today = new Date();
    const addedStudent = await Student_Job({
      JID: msg.JOB_CODE,
      SID: msg.SID,
      STATUS: "Pending",
      RESUME: msg.file ? msg.file.originalname : "",
      CreatedAt:
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate()
    });
    addedStudent
      .save()
      .then(result => {
        console.log(result);

        return callback(null, {
          status: 200,
          application_status: "  Applied successfully",
          resume: msg.file ? msg.file.originalname : ""
        });
      })
      .catch(err => {
        console.log(err);
        return callback(
          {
            status: 403,
            application_status: "  Already applied"
          },
          null
        );
      });
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err: err }, null);
  }
};

exports.applyJob = applyJob;
