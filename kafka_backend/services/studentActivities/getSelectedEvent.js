"use strict";
var express = require("express");
const Mongoose = require("mongoose");
const { Company_Events, Student_Event } = require("../../model/companyModels");

let getSelectedEvent = async (msg, callback) => {
  Company_Events.findOne({
    _id: msg.event_id
  })
    .populate("CID")
    .then(result => {
      if (result) {
        console.log(result);
        return callback(null, { status: 200, result });
      } else {
        return callback({ status: 403 }, null);
      }
    })
    .catch(err => {
      console.log(err);
      return callback({ status: 500 }, null);
    });
};

exports.getSelectedEvent = getSelectedEvent;
