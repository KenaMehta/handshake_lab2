"use strict";
//jobs
const { getCompanyJobs } = require("./getCompanyJobs");
const { getSelectedJob } = require("./getSelectedJob");
const { applyJob } = require("./applyJob");
//events
const { getCompanyEvents } = require("./getCompanyEvents");
const { getSelectedEvent } = require("./getSelectedEvent");
const { registerEvent } = require("./registerEvent");
const { getRegisteredEvents } = require("./getRegisteredEvents");
//application
const { getStudentApplications } = require("./getStudentApplications");
//students
const { getAllStudents } = require("./getAllStudents");
//messages
const { getMessages } = require("./getMessages");
const { getSelectedMessage } = require("./getSelectedMessage");
const { sendMessage } = require("./sendMessage");
// const { sendMessageProfile } = require("./sendMessageProfile");

let handle_request = (msg, callback) => {
  console.log("in student switch");
  switch (msg.route) {
    //jobs
    case "getCompanyJobs":
      getCompanyJobs(msg, callback);
      break;
    case "getSelectedJob":
      getSelectedJob(msg, callback);
      break;
    case "applyJob":
      applyJob(msg, callback);
      break;
    //events
    case "getCompanyEvents":
      getCompanyEvents(msg, callback);
      break;
    case "getSelectedEvent":
      getSelectedEvent(msg, callback);
      break;
    case "registerEvent":
      registerEvent(msg, callback);
      break;
    case "getRegisteredEvents":
      getRegisteredEvents(msg, callback);
      break;
    //application
    case "getStudentApplications":
      getStudentApplications(msg, callback);
      break;
    //Students
    case "getAllStudents":
      getAllStudents(msg, callback);
      break;
    //Messages
    case "getMessages":
      getMessages(msg, callback);
      break;
    case "getSelectedMessage":
      getSelectedMessage(msg, callback);
      break;
    case "sendMessage":
      sendMessage(msg, callback);
      break;
    // case "sendMessageProfile":
    //   sendMessageProfile(msg, callback);
    //   break;
  }
};

exports.handle_request = handle_request;
