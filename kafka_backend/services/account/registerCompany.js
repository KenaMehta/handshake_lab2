"use strict";
var express = require("express");
const bcrypt = require("bcrypt");
const { Company } = require("../../model/companyModels");
const Mongoose = require("mongoose");

let registerCompany = async (msg, callback) => {
  try {
    const company_register = Company({
      NAME: msg.name,
      EMAIL: msg.email,
      PASSWORD: bcrypt.hashSync(msg.password1, 10),
      LOCATION: msg.location
    });
    company_register
      .save()
      .then(savedRecord => {
        console.log(JSON.stringify(savedRecord) + ": saved record");
        return callback(null, {
          status: 200,
          passed: true,
          res: "Company Record Inserted",
          msg: savedRecord
        });
      })
      .catch(err => {
        return callback(
          {
            status: 403,
            passed: false,
            res: "Error caught",
            msg: err
          },
          null
        );
      });
  } catch (err) {
    console.log(err);
  }
};

exports.registerCompany = registerCompany;
