"use strict";
var express = require("express");
const bcrypt = require("bcrypt");
const { Student } = require("../../model/studentModels");
const { Company } = require("../../model/companyModels");
const Mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const { secret } = require("../../utils/config");
const { auth } = require("../../utils/passport");
auth();

let login = async (msg, callback) => {
  let response = {};
  let err = {};
  let activeUser;
  let model = "";
  console.log(JSON.stringify(msg));
  model = msg.category === "student" ? Student : Company;
  let email = msg.email;
  model
    .findOne({
      EMAIL: email
    })
    .then(result => {
      if (result) {
        console.log(JSON.stringify(result) + "here");
        console.log("msg.password", msg.password);
        console.log("result.PASSWORD", result.PASSWORD);

        bcrypt.compare(msg.password, result.PASSWORD, async function(
          err,
          matchFlag
        ) {
          if (err) {
            console.log("Error caught in password comparison");
            return callback(
              {
                status: 403,
                id: "",
                category: msg.category,
                passed: false,
                res: "Error caught in password comparison",
                msg: err,
                name: ""
              },
              null
            );
          } else if (!matchFlag) {
            console.log("Password Incorrect");

            return callback(
              {
                status: 403,
                id: "",
                category: msg.category,
                passed: false,
                res: "Password Incorrect",
                msg: null,
                name: ""
              },
              null
            );
          } else {
            console.log("Logged in successfully");
            // const logintoken = await generateToken(result._id);
            const token = jwt.sign(
              { _id: result._id, category: msg.category },
              secret,
              {
                expiresIn: 1008000
              }
            );
            var jwtToken = "JWT " + token;
            return callback(null, {
              status: 200,
              id: result._id,
              category: msg.category,
              passed: true,
              res: "Logged in successfully",
              name: result.NAME,
              idToken: jwtToken
            });
          }
        });
      } else {
        console.log("User not found");
        return callback(
          {
            status: 403,
            id: "",
            category: msg.category,
            passed: false,
            res: "User not found",
            msg: null,
            name: ""
          },
          null
        );
      }
    })
    .catch(err => {
      console.log("Error caught");
      return callback(
        {
          status: 500,
          id: "",
          category: msg.category,
          passed: false,
          res: "Error caught",
          msg: err,
          name: ""
        },
        null
      );
    });
};

exports.login = login;
