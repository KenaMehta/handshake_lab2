"use strict";
var express = require("express");
const bcrypt = require("bcrypt");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");

let registerStudent = async (msg, callback) => {
  const student_register = await Student({
    NAME: msg.name,
    EMAIL: msg.email,
    PASSWORD: bcrypt.hashSync(msg.password1, 10),
    COLLEGE_NAME: msg.college_name
  });
  student_register
    .save()
    .then(savedRecord => {
      return callback(null, {
        status: 200,
        passed: true,
        res: "Student Record Inserted",
        msg: savedRecord
      });
    })
    .catch(err => {
      return callback(
        {
          status: 403,
          passed: false,
          res: "Error caught while inserting record",
          msg: err
        },
        null
      );
    });
};

exports.registerStudent = registerStudent;
