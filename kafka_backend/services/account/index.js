"use strict";
const { registerStudent } = require("./registerStudent");
const { registerCompany } = require("./registerCompany");
const { login } = require("./login");
const { apiTest } = require("./apiTest");

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "register_student":
      registerStudent(msg, callback);
      break;
    case "login":
      login(msg, callback);
      break;
    case "register_company":
      registerCompany(msg, callback);
      break;
    case "api_test":
      apiTest(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;
