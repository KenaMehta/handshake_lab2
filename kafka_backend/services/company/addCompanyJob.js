"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../../model/companyModels");

let addCompanyJob = async (msg, callback) => {
  console.log("Req body : ", msg.body);
  try {
    var today = new Date();
    const addJob = await Company_Jobs({
      CID: msg.params.cid,
      C_NAME: msg.C_NAME,
      JOB_TITLE: msg.JOB_TITLE,
      APP_DEADLINE: msg.APP_DEADLINE,
      CITY: msg.CITY,
      STATE: msg.STATE,
      COUNTRY: msg.COUNTRY,
      SALARY: msg.SALARY,
      JOB_DESC: msg.JOB_DESC,
      JOB_CATEGORY: msg.JOB_CATEGORY,
      POST_FLAG: "Y",
      Created_At:
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate()
    });
    addJob
      .save()
      .then(async result => {
        var { limit, page } = msg.query;
        let options = {
          populate: "CID",
          limit: parseInt(limit, 10) || 10,
          page: parseInt(page, 10) || 1
        };
        const addedJob = await Company_Jobs.paginate(
          { CID: msg.params.cid },
          options
        );
        return callback(null, {
          status: 200,
          jobArr: addedJob.docs,
          jobObj: addedJob.docs[0],
          total: addedJob.total,
          pages: addedJob.pages
        });
      })
      .catch(err => {
        console.log(err);
        return callback({ status: 500 }, null);
      });
  } catch (err) {
    return callback({ status: 500 }, null);
  }
};

exports.addCompanyJob = addCompanyJob;
