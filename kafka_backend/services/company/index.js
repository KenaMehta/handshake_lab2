"use strict";
//profile
const { uploadCompanyPic } = require("./uploadCompanyPic");
const { getCompanyProfile } = require("./getCompanyProfile");
const { updateCompanyProfile } = require("./updateCompanyProfile");
//company jobs
const { addCompanyJob } = require("./addCompanyJob");

let handle_request = (msg, callback) => {
  console.log("in company switch");
  switch (msg.route) {
    //company profile
    case "uploadCompanyPic":
      uploadCompanyPic(msg, callback);
      break;
    case "getCompanyProfile":
      getCompanyProfile(msg, callback);
      break;
    case "updateCompanyProfile":
      updateCompanyProfile(msg, callback);
      break;
    //company jobs
    case "addCompanyJob":
      addCompanyJob(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;
