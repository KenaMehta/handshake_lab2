"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../../model/companyModels");

let updateCompanyProfile = async (msg, callback) => {
  console.log("Req body : ", msg.body);
  try {
    Company.findOne({ _id: msg.params.cid }).then(async result => {
      console.log(result);
      let NAME = msg.NAME ? msg.NAME : result.NAME;
      let LOCATION = msg.LOCATION ? msg.LOCATION : result.LOCATION;
      let DESCRIPTION = msg.DESCRIPTION ? msg.DESCRIPTION : result.DESCRIPTION;
      let PHONE = msg.PHONE ? msg.PHONE : result.PHONE;

      let query = { _id: msg.params.cid };
      let update = {
        NAME: NAME,
        DESCRIPTION: DESCRIPTION,
        PHONE: PHONE,
        LOCATION: LOCATION
      };
      let options = { new: true, useFindAndModify: false };
      let company_basic_details = await Company.findOneAndUpdate(
        query,
        update,
        options
      );
      return callback(null, { status: 200, data: company_basic_details });
    });
  } catch (err) {
    console.log(err);
    return callback({ status: 500 });
  }
};

exports.updateCompanyProfile = updateCompanyProfile;
