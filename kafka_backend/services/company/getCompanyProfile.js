"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../../model/companyModels");

let getCompanyProfile = async (msg, callback) => {
  console.log("Req body : ", msg);
  Company.findOne({
    _id: msg.params.cid
  })
    .populate("CID")
    .then(result => {
      return callback(null, {
        status: 200,
        PROFILE_PIC: result
          ? result.PROFILE_PIC ? result.PROFILE_PIC : "null"
          : "null",
        NAME: result ? result.NAME : "",
        LOCATION: result ? result.LOCATION : "",
        PHONE: result ? result.PHONE : "",
        DESCRIPTION: result ? result.DESCRIPTION : ""
      });
      console.log(result + "   --->result");
    })
    .catch(err => {
      console.log(err);
      return callback({ status: 500 }, null);
    });
};

exports.getCompanyProfile = getCompanyProfile;
