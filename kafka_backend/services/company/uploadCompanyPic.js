"use strict";
var express = require("express");
const { Student } = require("../../model/studentModels");
const Mongoose = require("mongoose");
const {
  Company,
  Company_Jobs,
  Company_Events,
  Student_Job,
  Student_Event,
  Company_Profile
} = require("../../model/companyModels");

let uploadCompanyPic = async (msg, callback) => {
  console.log("Req body : ", msg.file);
  try {
    let query = { _id: msg.params.cid };
    let update = {
      PROFILE_PIC: `${msg.params.cid}-${msg.file.originalname}`
    };
    let options = { new: true, useFindAndModify: false };
    let company_pic = await Company.findOneAndUpdate(query, update, options);
    company_pic
      .save()
      .then(response => {
        return callback(null, {
          status: 200,
          photo: `${msg.params.cid}-${msg.file.originalname}`
        });
      })
      .catch(err => {
        console.log(err);
        return callback({ status: 500, err }, null);
      });
  } catch (err) {
    console.log(err);
    return callback({ status: 500, err }, null);
  }
};

exports.uploadCompanyPic = uploadCompanyPic;
