var chai = require("chai"),
  chaiHttp = require("chai-http");
chai.use(chaiHttp);
const api_host = "http://localhost";
const api_port = "3001";
const api_url = api_host + ":" + api_port;

var expect = chai.expect;

it("Check if API server is up and running", function(done) {
  chai
    .request(api_url)
    .get("/api")
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.text).to.equal("matched");
      done();
    });
});

it("Check if login is valid", function(done) {
  chai
    .request(api_url)
    .post("/login")
    .send({
      email: "dd@gmail.com",
      password: "!123Qqwe",
      category: "student"
    })
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body.passed).to.equal(true);
      done();
    });
});

it("Validate profile details", function(done) {
  chai
    .request(api_url)
    .get("/student/profile/5e7bcc516d5360219b6fc05d")
    .set(
      "Authorization",
      "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTdiY2M1MTZkNTM2MDIxOWI2ZmMwNWQiLCJjYXRlZ29yeSI6InN0dWRlbnQiLCJpYXQiOjE1ODYzOTMwODUsImV4cCI6MTU4NzQwMTA4NX0.dim9BHh0Ji5rt2er-ZaNVoFtG1JZRi9m9JqGrEcEvDU"
    )
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body).to.be.a("Object");
      expect(res.body.id).to.equal("5e7bcc516d5360219b6fc05d");
      done();
    });
});

it("Checking if job search returns atleast one job", function(done) {
  chai
    .request(api_url)
    .get("/student/job_list/none/none/none/none")
    .set(
      "Authorization",
      "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTdiY2M1MTZkNTM2MDIxOWI2ZmMwNWQiLCJjYXRlZ29yeSI6InN0dWRlbnQiLCJpYXQiOjE1ODYzOTMwODUsImV4cCI6MTU4NzQwMTA4NX0.dim9BHh0Ji5rt2er-ZaNVoFtG1JZRi9m9JqGrEcEvDU"
    )
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body.jobArr).to.be.a("Array");
      expect(res.body.jobArr).to.have.length.greaterThan(1);
      done();
    });
});

it("Checking if event search returns atleast one event", function(done) {
  chai
    .request(api_url)
    .get("/student/event_list/none")
    .set(
      "Authorization",
      "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTdiY2M1MTZkNTM2MDIxOWI2ZmMwNWQiLCJjYXRlZ29yeSI6InN0dWRlbnQiLCJpYXQiOjE1ODYzOTMwODUsImV4cCI6MTU4NzQwMTA4NX0.dim9BHh0Ji5rt2er-ZaNVoFtG1JZRi9m9JqGrEcEvDU"
    )
    .send()
    .end(function(err, res) {
      expect(res).to.have.status(200);
      expect(res.body.eventArr).to.be.a("Array");
      expect(res.body.eventArr).to.have.length.greaterThan(1);
      done();
    });
});
