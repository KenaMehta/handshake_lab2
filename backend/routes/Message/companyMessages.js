var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//Send message from Company to Student
router.post("/sendMessage/:sid/:cid", async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "sendMessage";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Messages Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch messages from company"
    });
  }
});

//get company's message threads
router.get("/:cid", async function(req, res) {
  try {
    console.log(req.params.cid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getMessages";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting messages Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch messages from company"
    });
  }
});

//get company's selected message thread
router.get("/selectedMessage/:sid/:cid", async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getSelectedMessage";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting messages Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch messages from company"
    });
  }
});

module.exports = router;
