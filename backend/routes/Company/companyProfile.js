var express = require("express");
var app = express();
var router = express.Router();
var multer = require("multer");
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

storage = multer.diskStorage({
  destination: "./public/images",
  filename: function(req, file, cb) {
    cb(null, req.params.cid + "-" + file.originalname);
  }
});

upload = multer({ storage });

//upload profile picture
router.post(
  "/picture/:cid",
  upload.single("myimage"),
  checkAuth,
  async function(req, res) {
    console.log("Req body : ", req.file);
    try {
      let msg = req.body;
      msg.params = req.params;
      msg.file = req.file;
      msg.route = "uploadCompanyPic";
      kafka.make_request("company", msg, function(err, results) {
        if (err) {
          res.status(err.status).send();
        } else {
          res.writeHead(200, {
            "Content-Type": "application/json"
          });
          res.status(results.status).end(results.photo);
        }
      });
    } catch (err) {
      res.status(500).send();
      console.log(err);
    }
  }
);

//Add or update company's basic details
router.post("/:cid", checkAuth, async function(req, res) {
  console.log("Req body : ", req.file);
  try {
    let msg = req.body;
    msg.params = req.params;
    msg.file = req.file;
    msg.route = "updateCompanyProfile";
    kafka.make_request("company", msg, function(err, results) {
      if (err) {
        res.status(err.status).send();
      } else {
        res.status(results.status).send(results.data);
      }
    });
  } catch (err) {
    res.status(500).send();
    console.log(err);
  }
});

//Get company's detail
router.get("/:cid", checkAuth, function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "getCompanyProfile";
    kafka.make_request("company", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch profile from company"
    });
  }
});

//Add new job posting
router.post("/add_job/:cid", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "addCompanyJob";
    kafka.make_request("company", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company Connection: " + err);
    res.status(500).send({
      res: "Unable to add job for company"
    });
  }
});

module.exports = router;
