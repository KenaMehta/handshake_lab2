var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//Get list of students applied for particular event
router.get("/:eid", async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getEventStudents";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.studentList);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting student_event Connection: " + err);
    res.status(500).send({
      res: "Unable to get student's events who registered for the event"
    });
  }
});

module.exports = router;
