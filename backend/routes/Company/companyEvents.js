var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//Add new event posting
router.post("/add_event/:cid", async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "addCompanyEvent";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch events from company"
    });
  }
});

//get company's events
router.get("/:cid/:event_filter", async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getCompanyEvents";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch events from company"
    });
  }
});

module.exports = router;
