var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//update student's status
router.put("/", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "updateStudentStatus";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send();
      } else {
        res.status(results.status).send();
      }
    });
  } catch (err) {
    console.log("Error caught when hitting student_job Connection: " + err);
    res.status(500).send({
      res: "Unable to update student's status who applied for the job"
    });
  }
});

//Get list of students applied for particular job
router.get("/:jid", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getJobStudents";
    kafka.make_request("companyActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.studentList);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting student_job Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch students who applied for the job"
    });
  }
});

module.exports = router;
