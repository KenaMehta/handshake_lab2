var express = require("express");
var app = express();
const bcrypt = require("bcrypt");
var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//Get Company Event List
router.get("/:event_filter", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getCompanyEvents";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company_event Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch jobs from company"
    });
  }
});

//Get selected event
router.post("/", checkAuth, function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getSelectedEvent";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.result);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting company_event Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch selected event from company"
    });
  }
});

//apply for event
router.post("/event_apply", checkAuth, async function(req, res) {
  try {
    console.log(req.body);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "registerEvent";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting student_event Connection: " + err);
    res.status(500).send({
      res: "Unable to apply for event"
    });
  }
});

//get registered events
router.post("/regEvents", checkAuth, async function(req, res) {
  try {
    console.log(req.body);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getRegisteredEvents";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.eventListArr);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting student_event Connection: " + err);
    res.status(500).send({
      res: "Unable to apply for event"
    });
  }
});

module.exports = router;
