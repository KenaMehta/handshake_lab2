var express = require("express");
var app = express();
var router = express.Router();
var multer = require("multer");
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

storage = multer.diskStorage({
  destination: "./public/images",
  filename: function(req, file, cb) {
    cb(null, req.params.sid + "-" + file.originalname);
  }
});

upload = multer({ storage });

//upload profile picture
router.post(
  "/picture/:sid",
  upload.single("myimage"),
  checkAuth,
  async function(req, res) {
    console.log("Req body : ", req.file);
    try {
      let msg = req.body;
      msg.params = req.params;
      msg.file = req.file;
      msg.route = "uploadStudentPic";
      kafka.make_request("student", msg, function(err, results) {
        if (err) {
          res.status(err.status).send();
        } else {
          res.writeHead(200, results.writeHead);
          res.status(results.status).end(results.data);
        }
      });
    } catch (err) {
      res.status(500).send();
      console.log(err);
    }
  }
);

//Get Student's Profile
router.get("/:sid", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "getStudentProfile";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch profile from Student"
    });
  }
});

//Update Student's Profile
router.put("/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "updateStudentProfile";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.result);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to update profile from Student"
    });
  }
});

//get Student's journey
router.get("/journey/:sid", function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "getStudentJourney";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send({ journey: results.journey });
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to get journey from Student"
    });
  }
});

//Update Student's Journey
router.put("/journey/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "updateStudentJourney";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to update journey from Student"
    });
  }
});

//Delete Student Education
router.delete("/education/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "deleteStudentEducation";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.education);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to delete education from Student"
    });
  }
});

//Add or Update student education
router.post("/education/:sid", checkAuth, function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "updateStudentEducation";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.education);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to update education from Student"
    });
  }
});

//Delete Student Experience
router.delete("/experience/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "deleteStudentExperience";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.experience);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to delete experience from Student"
    });
  }
});

//Add or Update student experience
router.post("/experience/:sid", checkAuth, function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "updateStudentExperience";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.experience);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to update experience from Student"
    });
  }
});

//Delete skills
router.delete("/skills/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "deleteStudentSkills";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.skill);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to delete skills from Student"
    });
  }
});

//Add skills
router.post("/skills/:sid", checkAuth, async function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "addStudentSkills";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.skill);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to add skills from Student"
    });
  }
});

//Add or update student's basic details
router.post("/basic_details/:sid", checkAuth, function(req, res) {
  console.log(req.body);
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.route = "updateStudentBasicDetails";
    kafka.make_request("student", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to update basic details from Student"
    });
  }
});

module.exports = router;
