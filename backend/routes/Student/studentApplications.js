var express = require("express");
var app = express();
const bcrypt = require("bcrypt");
var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

router.get(
  "/:sid/:pending_filter/:reviewed_filter/:declined_filter",
  checkAuth,
  async function(req, res) {
    try {
      console.log(req.params.sid);
      let msg = req.body;
      msg.params = req.params;
      msg.query = req.query;
      msg.route = "getStudentApplications";
      kafka.make_request("studentActivities", msg, function(err, results) {
        if (err) {
          res.status(err.status).send(err);
        } else {
          res.status(results.status).send(results);
        }
      });
    } catch (err) {
      console.log("Error caught when hitting student_job Connection: " + err);
      res.status(500).send({
        res: "Unable to fetch student's jobs"
      });
    }
  }
);

module.exports = router;
