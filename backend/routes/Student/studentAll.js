var express = require("express");
var app = express();
const bcrypt = require("bcrypt");
var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");

//Get list of students
router.get(
  "/:student_filter/:major_filter/:skill_filter",
  checkAuth,
  async function(req, res) {
    try {
      console.log(req.params.sid);
      let msg = req.body;
      msg.params = req.params;
      msg.query = req.query;
      msg.route = "getAllStudents";
      kafka.make_request("studentActivities", msg, function(err, results) {
        if (err) {
          res.status(err.status).send(err);
        } else {
          res.status(results.status).send(results);
        }
      });
    } catch (err) {
      console.log("Error caught when hitting student Connection: " + err);
      res.status(500).send({
        res: "Unable to fetch student's profiles"
      });
    }
  }
);

//Get selected students profile

module.exports = router;
