var express = require("express");
var app = express();
var router = express.Router();
const { checkAuth } = require("../../utils/passport");
const kafka = require("../../kafka/client");
var multer = require("multer");

storage = multer.diskStorage({
  destination: "./public/images",
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
upload = multer({ storage });

//Get Company Job's List
router.get(
  "/:category_filter/:company_filter/:job_filter/:location_filter",
  checkAuth,
  async function(req, res) {
    try {
      console.log(req.params.sid);
      let msg = req.body;
      msg.params = req.params;
      msg.query = req.query;
      msg.route = "getCompanyJobs";
      kafka.make_request("studentActivities", msg, function(err, results) {
        if (err) {
          res.status(err.status).send(err);
        } else {
          res.status(results.status).send(results);
        }
      });
    } catch (err) {
      console.log("Error caught when hitting Student Connection: " + err);
      res.status(500).send({
        res: "Unable to fetch profile from Student"
      });
    }
  }
);

//Get selected job
router.post("/", checkAuth, async function(req, res) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.route = "getSelectedJob";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results.company);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch profile from Student"
    });
  }
});

//Apply job by student in student_job table
router.post("/job_apply", upload.single("resume"), checkAuth, async function(
  req,
  res
) {
  try {
    console.log(req.params.sid);
    let msg = req.body;
    msg.params = req.params;
    msg.query = req.query;
    msg.file = req.file;
    msg.route = "applyJob";
    kafka.make_request("studentActivities", msg, function(err, results) {
      if (err) {
        res.status(err.status).send(err);
      } else {
        res.status(results.status).send(results);
      }
    });
  } catch (err) {
    console.log("Error caught when hitting Student Connection: " + err);
    res.status(500).send({
      res: "Unable to fetch profile from Student"
    });
  }
});

module.exports = router;
