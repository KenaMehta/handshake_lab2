var express = require("express");
const bcrypt = require("bcrypt");
var router = express.Router();
var cookieParser = require("cookie-parser");
var connection = require("../db_connections");
const { Student } = require("../model/studentModels");
const { Company } = require("../model/companyModels");
const Mongoose = require("mongoose");
const { generateToken, decryptToken } = require("./../service/token");
const jwt = require("jsonwebtoken");
const { secret } = require("../utils/config");
const { auth } = require("../utils/passport");
const kafka = require("../kafka/client");
auth();

router.get("/api", function(req, res, next) {
  let msg = {};
  msg.route = "api_test";
  kafka.make_request("account", msg, function(err, results) {
    if (err) {
      res.status(err.status).send(err);
    } else {
      res.status(results.status).send(results.data);
    }
  });
});

//student and company login
router.post("/login", function(req, res) {
  let msg = req.body;
  msg.route = "login";
  kafka.make_request("account", msg, function(err, results) {
    if (err) {
      res.status(err.status).send(err);
    } else {
      res.status(results.status).send(results);
    }
  });
});

//Register a student. Adds new student record in model student_register
router.post("/registerStudent", async function(req, res) {
  let msg = req.body;
  msg.route = "register_student";
  kafka.make_request("account", msg, function(err, results) {
    if (err) {
      res.status(err.status).send(err);
    } else {
      res.status(results.status).send(results);
    }
  });
});

//Register a company. Adds new company record in model company_register
router.post("/registerCompany", async function(req, res) {
  console.log(JSON.stringify(req.body));
  let msg = req.body;
  msg.route = "register_company";
  kafka.make_request("account", msg, function(err, results) {
    if (err) {
      res.status(err.status).send(err);
    } else {
      res.status(results.status).send(results);
    }
  });
});

module.exports = router;
