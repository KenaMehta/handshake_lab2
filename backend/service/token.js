const jwt = require("jsonwebtoken");
const key = require("./key");

function generateToken(username) {
  return jwt.sign({ payload: username }, key.secret);
}
function decryptToken(token) {
  console.log("inside decrypt token" + token);
  let id;
  try {
    token = token.replace(/^"|"$/g, "");
    console.log("inside decrypt token" + token);

    id = jwt.verify(token, key.secret).payload;
  } catch (err) {
    return {
      id: null,
      error: err.message
    };
  }
  return {
    id,
    error: null
  };
}

module.exports = {
  generateToken,
  decryptToken
};
